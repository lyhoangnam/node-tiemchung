const SessionID = "yKDc76sxU3xkOeo3VOq6abegV5A6zcKI";
const cafe = {
    media: {
        image: [".ico", ".jpg", ".jpeg", ".png", ".gif", ".svg", ".jfif", ".webp"],
        script: [".js", ".ts", ".map"],
        doc: [".txt", ".pdf", ".docx", ".xlsx", ".xml", ".html", ".htm"],
        audio: [".mp3"],
        video: [".mp4", ".flv"],
        font: [".ttf", ".otf", ".woff", ".woff2"],
        zip: [".zip", ".rar"],
        app: [".exe", ".dll", ".json"],
    },
    cus: ["sa", "VIP", "Customer"],
};
const cfm = cafe.media;
let loginRequired = {
    list: [],
    detail: [
        {
            href: "/order/create",
            required: cafe.cus,
            packages: [],
        },
    ]
};
loginRequired.list = [];
for (let item of loginRequired.detail) loginRequired.list.push(item.href);

module.exports = {
    SessionID,
    loginRequired,
    transpileDependencies: ["vuetify"],
    file: {
        media: {
            images: cfm.image,
            videos: cfm.video,
            zips: cfm.zip,
            apps: cfm.app,
            extension: [...cfm.image, ...cfm.script, ...cfm.doc, ...cfm.audio, ...cfm.video, ...cfm.font],
            doc: cfm.doc,
        },
    },
};

var Excel = require("exceljs");
const moment = require("moment");
const path = require("path");
const fs = require("fs-extra");
const numberStyle = { alignment: { horizontal: "right" }, numFmt: "#,##0" };
const cafe = {
    default: {
        sheetName: "Sheet1",
        colors: {
            header: {
                fill: "70AD47",
                font: "FFFFFF",
            },
            row: {
                fill: "E2EFDA",
            },
            list: {
                hex1: ["92CD00", "6599FF"],
                hex2: ["E47297", "FFC629"],
                hex3: ["8C489F", "56BAEC"],
                rgb: [
                    [228, 114, 151],
                    [146, 205, 0],
                    [255, 198, 41],
                    [101, 153, 255],
                    [192, 80, 77],
                    [140, 72, 159],
                    [170, 1, 20],
                    [131, 131, 31],
                    [202, 39, 140],
                    [86, 186, 236],
                ],
            },
        },
    },
    format: {
        time: "x",
    },
};
const df = cafe.default;
const fm = cafe.format;

const ExcelExport = (filter) => {
    let p = new Promise((resolve) => {
        let workbook = new Excel.Workbook();
        workbook.creator = "Bot Isaura";
        workbook.lastModifiedBy = "Bot Isaura System";
        workbook.created = new Date();
        workbook.modified = new Date();
        workbook.lastPrinted = new Date();
        workbook.properties.date1904 = true;

        for (let [s, sheet] of filter.data.entries()) {
            let sheetName = `Sheet ${s + 1}`;
            let wsv = [];
            let columns = sheet.columns || [];
            let rows = sheet.rows || [];

            let cf = sheet.config || {};
            let cfws = cf.worksheet;
            // if (cfws && cfws.name) sheetName = cfws.name;
            if (sheet.sheetName) sheetName = sheet.sheetName;
            if (cfws && cfws.views) wsv = [...wsv, ...cfws.views];

            let worksheet = workbook.addWorksheet(sheetName);
            worksheet.views = wsv;
            worksheet.columns = columns;

            let wsr1 = worksheet.getRow(1);
            let list = {
                IDCard: [],
                Address01: [],
                Address02: [],
                SubAddress: [],
            };
            for (let c = 0; c < columns.length; c++) {
                let wsc = wsr1.getCell(c + 1);
                wsc.fill = {
                    type: "pattern",
                    pattern: "solid",
                    fgColor: { argb: df.colors.header.fill },
                };
                wsc.font = {
                    bold: true,
                    color: { argb: df.colors.header.font },
                };
            };
            for (let [r, row] of rows.entries()) {
                let wsr = worksheet.getRow(r + 2);
                let FullName = row.FullName.toLowerCase();
                let IDCard = `${row.IDCard}`;
                let Address01 = `${FullName}-${row.Address01}`;
                let Address02 = `${FullName}-${row.Address02}`;
                let SubAddress = `${FullName}-${row.SubAddress}`;
                let isDup = {
                    IDCard: row.Case === "Trùng CMND",
                    Address01: row.Case === "Trùng địa chỉ",
                    Address02: row.Case === "Trùng địa chỉ",
                    SubAddress: row.Case === "Trùng tạm trú",
                };
                if (!list.IDCard.includes(IDCard)) list.IDCard.push(IDCard);
                if (!list.Address01.includes(Address01)) list.Address01.push(Address01);
                if (!list.Address02.includes(Address02)) list.Address02.push(Address02);
                if (!list.SubAddress.includes(SubAddress)) list.SubAddress.push(SubAddress);
                for (let [c, col] of columns.entries()) {
                    let obj = row[col.key];
                    let wsc = wsr.getCell(c + 1);
                    wsc.border = {
                        bottom: { style: "thin", color: { argb: df.colors.header.fill } },
                    };
                    let k1 = list.IDCard.length % df.colors.list.hex1.length;
                    let k21 = list.Address01.length % df.colors.list.hex2.length;
                    let k22 = list.Address02.length % df.colors.list.hex2.length;
                    let k3 = list.SubAddress.length % df.colors.list.hex3.length;
                    let color1 = df.colors.list.hex1[k1];
                    let color21 = df.colors.list.hex2[k21];
                    let color22 = df.colors.list.hex2[k22];
                    let color3 = df.colors.list.hex3[k3];
                    if (isDup.IDCard && col.key === "IDCard") wsc.fill = {
                        type: "pattern",
                        pattern: "solid",
                        fgColor: { argb: color1 },
                    };
                    if (isDup.Address01 && col.key === "Address01") wsc.fill = {
                        type: "pattern",
                        pattern: "solid",
                        fgColor: { argb: color21 },
                    };
                    if (isDup.Address02 && col.key === "Address02") wsc.fill = {
                        type: "pattern",
                        pattern: "solid",
                        fgColor: { argb: color22 },
                    };
                    if (isDup.SubAddress && col.key === "SubAddress") wsc.fill = {
                        type: "pattern",
                        pattern: "solid",
                        fgColor: { argb: color3 },
                    };
                    if (c === columns.length - 1) wsc.border.right = { style: "thin", color: { argb: df.colors.header.fill } };
                    if (typeof (obj) === "object") {
                        //customize format
                        try {
                            wsc.value = obj.value;
                            for (let key of Object.keys(obj.style)) wsc[key] = obj.style[key];
                        } catch (error) {
                            wsc.value = obj;
                        };
                    } else {
                        //default format
                        wsc.value = obj;
                    };
                };
            };
        };
        let filePath = filter.filePath || "./data/export";
        fs.ensureDirSync(filePath);
        let today = moment(new Date());
        let x = today.format(fm.time);
        let fileName = filter.fileName || "book1";
        fileName = fileName.replace(".xlsx", "");
        fileName = fileName.replace(".xls", "");
        fileName = fileName.replace(/ /g, "");
        fileName = encodeURI(fileName);
        fileName = `${fileName}-${x}.xlsx`;
        let fullPath = path.join(filePath, fileName);
        let data = { fullPath, filePath, fileName, x };
        let res = { is_success: true, data };
        workbook.xlsx.writeFile(fullPath).then(() => resolve(res)).catch((error) => resolve({ error }));
    });

    return p;
};

const ExcelRead = async (filter) => {
    console.log("excel.read", filter);
    let wb = new Excel.Workbook();
    try {
        let { fileName } = filter;
        let sheetName = filter.sheetName || df.sheetName;
        await wb.xlsx.readFile(fileName);
        let wsh = wb.getWorksheet(sheetName);
        let data = [];
        let props = [];
        wsh.eachRow({ includeEmpty: true }, (row) => {
            if (data.length <= 0) {
                row.eachCell({ includeEmpty: true }, (cell, colNumber) => {
                    props.push(cell.value);
                });
                data.push(props);
            } else {
                let dr = {};
                row.eachCell({ includeEmpty: true }, (cell, colNumber) => {
                    dr[props[colNumber - 1]] = cell.value;
                });
                data.push(dr);
            };
        });
        data.shift();

        return data;
    } catch (error) {
        console.log("error", error);
    };
};

module.exports = {
    numberStyle,
    export: ExcelExport,
    read: ExcelRead,
};
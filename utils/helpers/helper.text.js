const moment = require("moment");
const numeral = require("numeral");
const cheerio = require("cheerio");
const hFile = require("./helper.file");
const cafe = {
    default: {
        ascii_length: 50,
        validateGenderType: "1",
    },
    format: {
        number: "0,0",
        date: "YYYY-MM-DD",
    },
};
const df = cafe.default;

const regE = () => {
    return {
        ebay: new RegExp(/(http:\/\/www.ebay.com\/([^\s]+)|https:\/\/www.ebay.com\/)([^\s]+)/, "g"),
        nike: new RegExp(/(https:\/\/www.nike.com\/t\/([^\s]+)|https:\/\/www.nike.com\/u\/([^\s]+))/, "g"),
        adidas: new RegExp(/https:\/\/www.adidas.com\/us\/([^\s]+)/, "g"),
        jomashop: new RegExp(/https:\/\/www.jomashop.com\/([^\s]+)/, "g"),
        sephora: new RegExp(/https:\/\/www.sephora.com\/([^\s]+)/, "g"),
        amazon: new RegExp(/(https:\/\/www.amazon.com\/(.*?)\/dp\/([^\s]+)|https:\/\/www.amazon.com\/dp\/([^\s]+)|https:\/\/www.amazon.com\/(.*?)\/d\/([^\s]+)|https:\/\/www.amazon.com\/(.*?)\/product\/([^\s]+))/, "g"),
    };
};

const formatNumber = (obj) => {
    let number = 0;
    let format = cafe.format;
    if (typeof (obj) === "object") {
        number = obj.number;
        format = obj.format || cafe.format;
    };
    if (!isNaN(obj)) number = obj;
    if (isNaN(number)) return "isNaN";
    let text = numeral(number).format(format);
    if (format === "short") {
        let milion = 1000000;
        let thousand = 1000;
        let n, n1, n2, fm, suffix;
        if (number >= milion) {
            n = number / milion;
            n1 = ~~n;
            text = `${numeral(n1).format("0")}M`;
            if (n !== n1) {
                n2 = (number - n1 * milion) / thousand;
                fm = n2 === ~~n2 ? "0" : "0.0";
                text += `${numeral(n2).format(fm)}k`;
            };
        } else if (number >= thousand) {
            n = number / thousand;
            suffix = "k";
            fm = n === ~~n ? "0" : "0.0";
            text = `${numeral(n).format(fm)}${suffix}`;
        } else {
            text = number;
        };
    };

    return text;
};

const dateDiff = (date1, date2) => {
    if (date1 instanceof Date) date1 = moment(date1);
    if (date2 instanceof Date) date2 = moment(date2);
    let text = "";
    let dd = date2.diff(date1, "days") % 365;
    let hh = date2.diff(date1, "hours") % 24;
    let mm = date2.diff(date1, "minutes") % 60;
    let ss = date2.diff(date1, "seconds") % 60;
    dd = formatNumber({ number: dd, format: "00" });
    hh = formatNumber({ number: hh, format: "00" });
    mm = formatNumber({ number: mm, format: "00" });
    ss = formatNumber({ number: ss, format: "00" });
    if (dd !== "00") text += `${dd} ngày `;
    if (hh !== "00") text += `${hh} giờ `;
    if (mm !== "00") text += `${mm} phút `;
    if (ss !== "00") text += `${ss} giây`;

    return text.trim();
};

const toAscii = (text) => {
    if (typeof (text) === "undefined") text = "";
    if (typeof (text) === "object") text = JSON.stringify(text);
    let arr = [];
    let temp = [];
    for (let char of text.split("")) {
        if (temp.length >= df.ascii_length) {
            arr.push(temp.join("-"));
            temp = [];
        };
        temp.push(char.charCodeAt(0));
    };
    if (temp.length > 0) arr.push(temp.join("-"));
    let value = arr.join("/");

    return value;
};

const toUnicode = (str) => {
    return str.split('').map(function (value, index, array) {
        var temp = value.charCodeAt(0).toString(16).toUpperCase();
        if (temp.length > 2) {
            return '\\u' + temp;
        }
        return value;
    }).join('');
};

const toExif = (text) => {
    if (typeof (text) === "undefined") text = "";
    if (typeof (text) === "object") text = JSON.stringify(text);
    let arr = [];
    for (let char of text.split("")) {
        let x = char.charCodeAt(0);
        arr.push(x);
        // arr.push(0);
    };
    // arr = [...arr, 0, 0];

    return arr;
};

const toBuffer = (dataURL) => {
    if (!dataURL) return null;
    let x = dataURL.indexOf(",") + 1;
    let dataBase64 = dataURL.substring(x, dataURL.length);
    let dataBuffer = Buffer.from(dataBase64, "base64");

    return dataBuffer;
};

const sumAscii = (text) => {
    if (typeof (text) === "undefined") text = "";
    if (typeof (text) === "object") text = JSON.stringify(text);
    let sum = 0;
    for (let i = 0; i < text.length; i++) sum += text.charCodeAt(i);

    return sum;
};

const toUpperFirstLetter = (text) => {
    text = (text || "hello world!").toLowerCase();
    return `${text[0].toUpperCase()}${text.slice(1)}`;
};

const toLowerFirstLetter = (text) => {
    text = text || "hello world!";
    return `${text[0].toLowerCase()}${text.slice(1)}`;
};

const toUpperFirstLetterOfWord = (text) => {
    text = text || "hello world!";
    let arr = text.split(" ");
    let words = [];
    for (let letter of arr) {
        if (!letter.trim()) continue;
        words.push(`${letter[0].toUpperCase()}${letter.slice(1).toLowerCase()}`);
    };
    return words.join(" ");
};

const toInsert = (text) => {
    if (!text) return "";
    text = text.replace(/N''/g, "null")
        .replace(/''/g, "null")
        .replace(/N'null'/g, "null")
        .replace(/'null'/g, "null")
        .replace(/N'undefined'/g, "null")
        .replace(/'undefined'/g, "null");
    return text;
};

const minifyHtml = (text) => {
    text = text || "";
    let $ = cheerio.load(text);
    $("head").append(`<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">`);
    $("script").remove();
    text = $.html();

    let reg01 = new RegExp(/>\r\n/, "g");
    let reg02 = new RegExp(/>\s+</, "g");
    let reg03 = new RegExp(/\n\n/, "g");
    let reg04 = new RegExp(/a-text-center/, "g");
    // let reg05 = new RegExp(/<script(.|\n)*<\/script>/, "g");

    while (reg01.test(text)) text = text.replace(reg01, ">");
    while (reg02.test(text)) text = text.replace(reg02, "><");
    while (reg03.test(text)) text = text.replace(reg03, "\n");
    while (reg04.test(text)) text = text.replace(reg04, "text-center");

    return text;
};

const minifyText = (text) => {
    text = text || "";
    let $ = cheerio.load(text);
    text = $.text();
    let reg = new RegExp(/\n\n/, "g");
    while (reg.test(text)) text = text.replace(reg, "\n");
    text = text.replace(/\n/g, "\n+ ");

    return text;
};

const randomNumber = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const randomString = (max) => {
    let possible = {
        text: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        number: "0123456789",
    };
    let possible_string = `${possible.text}${possible.number}`;
    let x = randomNumber(0, possible.text.length - 1);
    let text = possible.text.charAt(x);
    for (let i = 1; i < max; i++) {
        x = randomNumber(0, possible_string.length - 1);
        text += possible_string.charAt(x);
    };

    return text;
};

const removeDiacritics = (text) => {
    if (typeof (text) !== "string") return null;
    let tResult = text.trim().replace(/\s\s/g, "").toLowerCase()
        .normalize("NFD").replace(/[\u0300-\u036f]/g, "")
        .replace(/đ/g, "d");

    return tResult;
};

const escapeRegExp = (text) => {
    if (!text) return null;
    return text.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
};

const validateFullName = (FullName) => {
    if (!FullName) return null;
    FullName = FullName.trim();
    FullName = FullName.replace(/\n/gi, " ");
    FullName = FullName.replace(/'/gi, "''");
    while ((new RegExp("\r", "gi")).test(FullName)) FullName = FullName.replace(/\r/gi, "");
    while ((new RegExp("\n", "gi")).test(FullName)) FullName = FullName.replace(/\n/gi, "");
    FullName = FullName.replace(/\s\s/g, "");
    FullName = toUpperFirstLetterOfWord(FullName);
    return FullName;
};

const validateAddress = (Address) => {
    if (!Address) return { isInValid: true };
    if (typeof (Address) !== "string") Address = JSON.stringify(Address);
    let Stadtteile = null;
    let Ward = null;
    let isInValid = false;
    Address = Address.trim();
    while ((new RegExp("\r", "gi")).test(Address)) Address = Address.replace(/\r/gi, "");
    while ((new RegExp("\n", "gi")).test(Address)) Address = Address.replace(/\n/gi, "");
    Address = Address.replace(/\s\s/g, " ");

    Address = Address.replace(/, Phường 16, Quận 4, Thành phố Hồ Chí Minh/gi, "");
    Address = Address.replace(/TTT/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thắt Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thất Thuyến/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thất Thuyểt/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thát Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tốn Thất Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tồn Thất Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thât Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/Tôn Thất Thuyêt/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/ Ôn Thất Thuyết/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/ TÔN THẤT THYẾT/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/ TÔN THẤT TUYẾT/gi, "Tôn Thất Thuyết");
    Address = Address.replace(/\/Tôn Thất Thuyết/gi, " Tôn Thất Thuyết");

    Address = Address.replace(/ĐVB/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/BVĐ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/đbv/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/DVB/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đvc/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đoàn Vă Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đoan Văn Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đoàn Van Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đooàn Văn Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Boàn Văn Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đoàn Văn Vơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/Đoán Văn Bơ/gi, "Đoàn Văn Bơ");
    Address = Address.replace(/\/Đoàn Văn Bơ/gi, " Đoàn Văn Bơ");

    Address = Address.replace(/XC/gi, "Xóm Chiếu");
    Address = Address.replace(/Xóm Chiêu/gi, "Xóm Chiếu");
    Address = Address.replace(/XÓM CHIÉU/gi, "Xóm Chiếu");
    Address = Address.replace(/\/Xóm Chiếu/gi, " Xóm Chiếu");
    Address = Address.replace(/NTT/gi, "Nguyễn Tất Thành");
    Address = Address.replace(/TĐ/gi, "Tôn Đản");
    Address = Address.replace(/Nguyễn Đình Triễu/gi, "Nguyễn Đình Chiểu");

    let reg = {
        to: new RegExp(/tổ \d+/, "gi"),
        ward: {
            p2: new RegExp(/ Phường 2/, "gi"),
            p3: new RegExp(/ Phường 3/, "gi"),
            p4: new RegExp(/ Phường 4/, "gi"),
            p6: new RegExp(/ Phường 6/, "gi"),
            p8: new RegExp(/ Phường 8/, "gi"),
            p9: new RegExp(/ Phường 9/, "gi"),
            p10: new RegExp(/ Phường 10/, "gi"),
            p13: new RegExp(/ Phường 13/, "gi"),
            p14: new RegExp(/ Phường 14/, "gi"),
            p15: new RegExp(/ Phường 15/, "gi"),
            p16: new RegExp(/ Phường 16/, "gi"),
            p18: new RegExp(/ Phường 18/, "gi"),
        },
        valid: {
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),
        },
        stick: {
            dvb: new RegExp(/\d+Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/\d+Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/\d+Xóm Chiếu/, "gi"),
        },
    };
    let testStadtteile = Address.match(reg.to);
    // hFile.log("Address.01", Address);
    if (Array.isArray(testStadtteile)) {
        Address = Address.replace(testStadtteile[0], " ");
        Stadtteile = testStadtteile[0].replace(/tổ/gi, "").trim();
    };

    Address = Address.replace(/khu phố 1/gi, "");
    Address = Address.replace(/khu phố 2/gi, "");
    Address = Address.replace(/khu phố 3/gi, "");
    Address = Address.replace(/khu phố 4/gi, "");
    Address = Address.replace(/ \(.*?\)/gi, "");
    Address = Address.replace(/ ở trọ/gi, "");
    Address = Address.replace(/ nhà trọ/gi, "");
    Address = Address.replace(/ tạm trú/gi, "");
    Address = Address.replace(/ đường/gi, "");

    Address = Address.replace(/ p16q4/gi, "");
    // Address = Address.replace(/ p16\/q4/gi, "");
    // Address = Address.replace(/ p16\/q5/gi, "");
    Address = Address.replace(/ q4/gi, "");
    // Address = Address.replace(/ p16/gi, "");
    // Address = Address.replace(/ -p16/gi, "");
    Address = Address.replace(/ quận 4/gi, "");
    // Address = Address.replace(/ phường 16/gi, "");
    Address = Address.replace(/'/gi, "''");
    Address = Address.replace(/,/gi, " ");
    Address = Address.replace(/\s\s+/g, " ");
    // hFile.log("Address.04", Address);

    Address = Address.replace(/ p2/gi, " Phường 2");
    Address = Address.replace(/ p3/gi, " Phường 3");
    Address = Address.replace(/ p4/gi, " Phường 4");
    Address = Address.replace(/ p6/gi, " Phường 6");
    Address = Address.replace(/ p8/gi, " Phường 8");
    Address = Address.replace(/ p9/gi, " Phường 9");
    Address = Address.replace(/ p10/gi, " Phường 10");
    Address = Address.replace(/ p13/gi, " Phường 13");
    Address = Address.replace(/ p14/gi, " Phường 14");
    Address = Address.replace(/ p15/gi, " Phường 15");
    Address = Address.replace(/ p16/gi, " Phường 16");
    Address = Address.replace(/ p18/gi, " Phường 18");

    Address = toUpperFirstLetterOfWord(Address).trim();
    if (reg.stick.ttt.test(Address)) {
        let from = Address.match(reg.stick.ttt);
        let to = `${(new RegExp(/\d+/gi)).exec(from)[0]} Tôn Thất Thuyết`;
        // hFile.log("stick.ttt", Address);
        Address = Address.replace(from, to);
    };
    if (reg.stick.dvb.test(Address)) {
        let from = Address.match(reg.stick.dvb);
        let to = `${(new RegExp(/\d+/gi)).exec(from)[0]} Đoàn Văn Bơ`;
        // hFile.log("stick.dvb", Address);
        Address = Address.replace(from, to);
    };
    if (reg.stick.xc.test(Address)) {
        let from = Address.match(reg.stick.xc);
        let to = `${(new RegExp(/\d+/gi)).exec(from)[0]} Xóm Chiếu`;
        // hFile.log("stick.xc", Address);
        Address = Address.replace(from, to);
    };
    if (
        !reg.valid.dvb.test(Address) &&
        !reg.valid.ttt.test(Address) &&
        !reg.valid.xc.test(Address)
    ) isInValid = true;
    if (reg.ward.p2.test(Address)) Ward = "Phường 2";
    if (reg.ward.p3.test(Address)) Ward = "Phường 3";
    if (reg.ward.p4.test(Address)) Ward = "Phường 4";
    if (reg.ward.p6.test(Address)) Ward = "Phường 6";
    if (reg.ward.p8.test(Address)) Ward = "Phường 8";
    if (reg.ward.p9.test(Address)) Ward = "Phường 9";
    if (reg.ward.p10.test(Address)) Ward = "Phường 10";
    if (reg.ward.p13.test(Address)) Ward = "Phường 13";
    if (reg.ward.p14.test(Address)) Ward = "Phường 14";
    if (reg.ward.p15.test(Address)) Ward = "Phường 15";
    if (reg.ward.p16.test(Address)) Ward = "Phường 16";
    if (reg.ward.p18.test(Address)) Ward = "Phường 18";

    return { Address, Stadtteile, Ward, isInValid };
};

const validateGender = ({ Male, FeMale, ValidateType = df.validateGenderType }) => {
    let obj = { DateOfBirth: null, Gender: 1 };

    switch (ValidateType) {
        case "1":
            // Male - FeMale
            if (typeof (Male) === "string")
                if (Male) Male = Male.trim();
            if (FeMale) FeMale = FeMale.trim();// trim trước xem có khoảng trắng không
            if (FeMale) {
                obj.Gender = 2;
                obj.DateOfBirth = FeMale;
            } else {
                obj.DateOfBirth = Male;
            };
            obj.DateOfBirth = validateDateOfBirth({ DateOfBirth: obj.DateOfBirth });
            break;
        default:
            break;
    };

    return obj;
};

const validateDateOfBirth = ({ STT, DateOfBirth }) => {
    /*********|validate input|*********/
    if (!DateOfBirth) {
        // hFile.log("validate.DateOfBirth.error", STT, DateOfBirth);
        return null;
    };
    DateOfBirth = `${DateOfBirth}`.trim();
    if (!DateOfBirth) return null;
    if (DateOfBirth.length === 4) DateOfBirth = `${DateOfBirth}-01-01`;
    let arr = [];
    let newDate = new Date();
    if (DateOfBirth.includes("/")) arr = DateOfBirth.split("/");
    if (DateOfBirth.includes(".")) arr = DateOfBirth.split(".");
    if (DateOfBirth.includes("-")) arr = DateOfBirth.split("-");
    if (arr.length != 3) return null;

    /*********|input is valid|*********/
    let isInValid = false;
    let mDate = null;
    let year = "2021";
    let month = parseInt(arr[1], 10) - 1;
    let day = "1";
    if (arr[0].length === 4) {
        year = arr[0];
        day = arr[2];
    } else {
        year = arr[2];
        day = arr[0];
    };
    if (year.length === 2) year = `20${year}`;
    year = parseInt(year, 10);
    day = parseInt(day, 10);
    try {
        newDate = new Date(year, month, day, 0, 0, 0, 0);
        mDate = moment(newDate);
        if (!mDate.isValid()) {
            isInValid = true;
            mDate = null;
        };
    } catch (_) {
        isInValid = true;
    };
    if (isInValid) {
        hFile.log("validate.DateOfBirth.error", STT, DateOfBirth);
        hFile.log("DateOfBirth.validate", arr, `${year}-${month}-${day}`);
    };

    return mDate;
};

const copyObject = (obj) => {
    return JSON.parse(JSON.stringify(obj));
};

const getHost = (href) => {
    let url = new URL(href);
    let hn = url.hostname.replace("www.", "");

    return hn;
};

const getPath = (href) => {
    let url = new URL(href);
    let hn = url.hostname.replace("www.", "");
    let text = `${hn}${url.pathname}`;

    return text;
};

module.exports = {
    format: {
        number: formatNumber,
    },
    date: {
        diff: dateDiff,
    },
    minify: {
        html: minifyHtml,
        text: minifyText,
    },
    random: {
        number: randomNumber,
        string: randomString,
    },
    to: {
        ascii: toAscii,
        exif: toExif,
        unicode: toUnicode,
        buffer: toBuffer,
        insert: toInsert,
        upper: {
            firstLetter: toUpperFirstLetter,
            firstLetterOfWord: toUpperFirstLetterOfWord,
        },
        lower: {
            firstLetter: toLowerFirstLetter,
        },
    },
    sum: {
        ascii: sumAscii,
    },
    remove: {
        diacritics: removeDiacritics,
    },
    escape: {
        regExp: escapeRegExp,
    },
    validate: {
        FullName: validateFullName,
        Address: validateAddress,
        DateOfBirth: validateDateOfBirth,
        Gender: validateGender,
    },
    regEcom: regE,
    cpo: copyObject,
    getHost,
    getPath,
};
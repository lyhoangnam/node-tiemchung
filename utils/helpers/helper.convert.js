
function ounceToKilogram(ounce) {
    return ounce * 0.0283495;
};

function poundToKilogram(pound) {
    return pound * 0.453592;
};

module.exports = {
    ounce: {
        to: {
            kg: ounceToKilogram,
        },
    },
    pound: {
        to: {
            kg: poundToKilogram,
        },
    },
};
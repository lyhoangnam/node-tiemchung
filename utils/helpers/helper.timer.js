const waitPromise = (x) => {
    let p = new Promise((resolve) => {
        if (isNaN(x)) return resolve();
        setTimeout(resolve, x);
    });

    return p;
};

module.exports = {
    wait: {
        promise: waitPromise,
    },
};
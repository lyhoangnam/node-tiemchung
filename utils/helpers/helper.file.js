const { Console } = require("console");
const fs = require("fs-extra");
const archiver = require("archiver");
const moment = require("moment");
const path = require("path");
const config = require("./config");
const vc = require("../../vue.config");
const cafe = {
    default: {
        folderPath: "D:\\download",
        zip: {
            dir: "./data/images",
            options: {
                zlib: { level: 9 }
            },
        },
    },
    format: {
        time: "x",
    },
};
const df = cafe.default;
const isTest = 1;
let botLog;
if (!botLog) botLog = new Console({
    stdout: fs.createWriteStream(config.fileLog, { flags: "a" }),
    stderr: fs.createWriteStream(config.fileLog, { flags: "a" }),
});

const fileLog = (text = "", ...optionalParams) => {
    let today = moment().format(config.format.dateTime);
    botLog.log(text, ...optionalParams);
    if (isTest) console.log(text, ...optionalParams);
};

const clear = () => {
    let fileName = config.fileLog;
    fs.ensureFileSync(fileName);
    fs.writeFileSync(fileName, "");
};

const newReg = (text) => {
    let reg = {
        g: new RegExp(text, "g"),
        m: new RegExp(text, "m"),
    };

    return reg;
};

const fileReplace = ({ file = "", data = {} }) => {
    if (!file || !fs.existsSync(file)) return { text: "", errors: [{ message: `Cannot find file ${file}` }] };
    let sysErrors = [];
    let sysText = fs.readFileSync(file, "utf8");
    let sysMatchs = []
    let regAll = [...config.regText.inFile, ...config.regText.inEntity];
    for (let sysReg of regAll) {
        let sysMatch = sysText.match(newReg(sysReg).g) || [];
        sysMatchs = [...sysMatchs, ...sysMatch];
    };
    for (let sysMatch of sysMatchs) {
        try {
            let sys_value = eval(sysMatch.trim());
            sysText = sysText.replace(sysMatch, sys_value);
        } catch (err) {
            sysErrors.push(err);
        };
    };
    let obj = { text: sysText, errors: sysErrors };

    return obj;
};

const loadDir = ({ folderPath = df.folderPath }) => {
    let p = new Promise((resolve) => {
        console.log("file.load.dir", folderPath);
        let isExist = fs.existsSync(folderPath);
        if (!isExist) return resolve(null);
        fs.readdir(folderPath, async (err, files) => {
            if (err) return console.log("file-size-read-err", err);
            let data = [];
            for (let name of files) {
                try {
                    let filePath = path.join(folderPath, name);
                    let stat = fs.statSync(filePath);
                    let key = `${folderPath}/${name}`;
                    let id = key.replace("\./", "/");
                    let objType = "folder";
                    if (stat.isDirectory()) {
                        let children = await loadDir({ folderPath: key });
                        data.push({ type: objType, id, name, children });
                    } else {
                        objType = "document";
                        let extname = path.extname(name).toLowerCase();
                        if (vc.file.media.images.includes(extname)) {
                            fs.unlinkSync(id);
                            objType = "image";
                        };
                        if (vc.file.media.videos.includes(extname)) objType = "video";
                        if (vc.file.media.zips.includes(extname)) objType = "zip";
                        if (vc.file.media.apps.includes(extname)) objType = "app";
                        if (extname === ".psd") objType = "psd";
                        if (extname === ".json") objType = "json";
                        if ([".pdf"].includes(extname)) objType = "pdf";
                        if ([".xls", ".xlsx"].includes(extname)) objType = "excel";
                        if ([".htm", ".html"].includes(extname)) objType = "html";
                        data.push({ type: objType, id, name });
                    };
                } catch (error_stat) {
                    console.log("error_stat", error_stat);
                };
            };
            resolve(data);
        });
    });

    return p;
};

const zip = ({ fileList = [] }) => {
    let p = new Promise((resolve) => {
        let today = moment();
        let x = today.format(cafe.format.time);
        let fileSave = `${df.zip.dir}/files-${x}.zip`;
        const out = fs.createWriteStream(fileSave);
        const archive = archiver("zip", df.zip.options);
        out.on("close", () => {
            let data = {
                fileName: fileSave,
                size: archive.pointer(),
            };
            console.log(`${data.size} total bytes`);
            resolve({ is_success: true, data });
        });
        archive.on("error", (error) => {
            resolve({ error });
        });
        archive.pipe(out);
        for (let fileName of fileList) {
            let isExist = fs.existsSync(fileName);
            if (!isExist) continue;
            let basename = path.basename(fileName);
            archive.file(fileName, { name: basename });
        };
        archive.finalize();
    });

    return p;
};

module.exports = {
    log: fileLog,
    zip,
    clear,
    replace: fileReplace,
    loadDir,
};
const path = require("path");

module.exports = {
    fileLog: path.resolve(__dirname, "../../data/log.txt"),
    format: {
        dateTime: "YYYY-MM-DD hh-mm-ss A",
    },
    regText: {
        inFile: ["\{\{(.*?)\}\}", "(\{\{\r\n)((?!\}\}).?(\r\n)?)*(\}\})"],
        inEntity: ["\{\\[(.*?)\\]\}", "(\{\\[\r\n)((?!\\]\}).?(\r\n)?)*(\\]\})"],
    },
};

const convertCookiesExpiresUTCToUnix = (expires_utc) => {
    if (isNaN(expires_utc)) return 0;
    let m = 1000000;
    let n = 11644473600;
    let x = expires_utc / m - n;

    return x;
};

const mapCookies = (data) => {
    let list = [];
    let text = [];
    if (!Array.isArray(data)) return list;
    for (let ck of data) {
        text.push(`${ck.name}=${ck.value}`);
        let cookie = {
            name: ck.name,
            value: ck.value,
            domain: ck.host_key,
            path: ck.path,
            expires: convertCookiesExpiresUTCToUnix(ck.expires_utc),
            // size: 81,
            httpOnly: (ck.is_httponly === 1),
            secure: (ck.is_secure === 1),
            session: false
        };
        list.push(cookie);
    };
    text = text.join("; ");
    return { list, text };
};

module.exports = {
    map: mapCookies,
};
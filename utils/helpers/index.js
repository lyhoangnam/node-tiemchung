const helperFile = require("./helper.file");
const helperSearch = require("./helper.search");
const helperText = require("./helper.text");
const helperExcel = require("./helper.excel.02");
const helperTimer = require("./helper.timer");
const helperConvert = require("./helper.convert");
const helperCookies = require("./helper.cookies");

module.exports = {
   file: helperFile,
   search: helperSearch,
   text: helperText,
   excel: helperExcel,
   timer: helperTimer,
   convert: helperConvert,
   cookies: helperCookies,
};
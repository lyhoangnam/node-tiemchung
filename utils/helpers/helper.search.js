const helperText = require("./helper.text");

const randomNumber = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const randomTimeSpan = (min, max) => {
    let hh = helperText.format.number({ number: randomNumber(min, max), format: "00" });
    let mm = helperText.format.number({ number: randomNumber(0, 60), format: "00" });
    let text = `${hh}:${mm} AM`;

    return text;
};

const loopOptions = (options) => {
    let keys = Object.keys(options);
    let arr = [];
    let data = [];
    let item = {};
    let texts = [];
    let key, list, value;
    keys.map((key) => {
        list = options[key];
        value = list[0];
        item[key] = value;
        texts.push(value);
        arr.push(0);
    });
    let n = keys.length - 1;
    let loop = n;
    data.push(texts.join("-"));
    while (loop >= 0) {
        if (arr[loop] === options[keys[loop]].length - 1) {
            arr[loop] = 0;
            loop -= 1;
        } else {
            arr[loop] += 1;
            loop = n;
            item = {};
            texts = [];
            for (let [i, index] of arr.entries()) {
                key = keys[i];
                list = options[key];
                value = list[index];
                item[key] = value;
                texts.push(value);
            };
            data.push(texts.join("-"));
        };
    };

    return data;
};

const permute = (list) => {
    let arr = [];
    for (let i = 0; i < list.length; i = i + 1) {
        let rest = permute(list.slice(0, i).concat(list.slice(i + 1)));
        if (!rest.length) {
            arr.push([list[i]]);
        } else {
            for (let j = 0; j < rest.length; j = j + 1) {
                arr.push([list[i]].concat(rest[j]));
            };
        };
    };
    return arr;
};

module.exports = {
    random: {
        number: randomNumber,
        timespan: randomTimeSpan,
    },
    loop: {
        options: loopOptions,
    },
    permute,
};
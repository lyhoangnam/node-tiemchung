
module.exports = {
    kiot: {
        retailer_notfound: "Xin lỗi, retailer chưa đăng ký!",
    },
    zalo: {
        oa_notfound: "Xin lỗi, oa chưa đăng ký!",
    },
    tanca: {
        tanca_notfound: "Xin lỗi, bạn chưa đăng ký tanca!",
    },
    contact_admin: "Đã có lỗi xảy ra, bạn hãy trở lại lúc khác!",
    unauthen: "Phân quyền không cho phép!",
    invalid_inputs: "Invalid inputs!",
    session_error: "Session is not found",
};
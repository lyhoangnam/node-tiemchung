const axios = require("axios");
const config = require("./config");
const instance = axios.create({
    baseURL: config.baseURL,
    timeout: config.timeout,
});

const sendEmail = (filter) => {
    let resolve;
    let p = new Promise(r => resolve = r);
    const href = "/Email/Sender/Send";
    instance.post(href, filter).then((res) => {
        if (res.status === 200 && res.statusText.toLowerCase() === "ok") resolve(res.data);
        else resolve({ error: res });
    }).catch((error) => {
        console.log("net.email.error", error);
        resolve({ error });
    });

    return p;
};

module.exports = {
    sender: {
        send: sendEmail,
    },
};
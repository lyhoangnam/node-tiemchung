const baseURL = "https://core.api.es6.vn/net";
const baseURLTest = "http://api.v2.es6.vn/net";
const timeout = 1000 * 60 * 10;
const spName = "Forever@0608";
const SessionIDBot = "MoGp00eWDc3Yv0zn5cwe2tiRxO6FgfyC";
const SessionIDP16Q4 = "g0vN_r7zdt2rChiymKqQQ5uzlwsQsvKZ";
const isTest = 0;

module.exports = {
    baseURL: isTest ? baseURLTest : baseURL,
    timeout,
    spName,
    SessionID: {
        bot: SessionIDBot,
        p16q4: SessionIDP16Q4,
    },
    stored: {
        GetSocialSecurities: "S8RVjWVBWfCFPzKY115TrT3gnsI0HGixC1aVxCHCm0g=",

        link: {
            ahrefs: {
                GetKeywordsIgnore: "9xOyoyUX1y8xvUBdVqnYwfuwyCGcW5SeXYOuECZTwak=",
            },
        },
    },
};
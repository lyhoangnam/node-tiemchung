const axios = require("axios");
const config = require("./config");
const isLocal = 1;

const loadAll = ({ cnn = "Bot", href = "", filter = {}, baseURL = config.baseURL }) => {
    filter.cnn = cnn;
    const instance = axios.create({ baseURL, timeout: config.timeout });
    let p = new Promise((resolve) => {
        instance.post(href, filter).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ isSuccess: false, error });
        });
    });

    return p;
};

const loadES6 = (cnn) => {
    let baseURL = "http://api.v2.es6.vn/net";
    if (isLocal) baseURL = "http://api-v2-es6-vn.localhost/net";
    let obj = {
        list: ({ spName, filter }) => loadAll({ cnn, href: "/Data/Load/List", filter: { spName, filter }, baseURL }),
        multiList: ({ spName, filter }) => loadAll({ cnn, href: "/Data/Load/MultiList", filter: { spName, filter }, baseURL }),
        topOne: ({ spName, filter }) => loadAll({ cnn, href: "/Data/Load/TopOne", filter: { spName, filter } }, baseURL),
        cookies: (filter) => loadAll({ cnn, href: "/Data/Load/Cookies", filter: { spName: config.spName, filter }, baseURL }),
    };

    return obj;
};

const load = ({ cnn, projectName }) => {
    let obj = {
        list: ({ storeName, filter }) => loadAll({ cnn, href: "/Data/Load/List", filter: { storeName, filter, projectName } }),
        multiList: ({ storeName, filter }) => loadAll({ cnn, href: "/Data/Load/List", filter: { storeName, filter, projectName } }),
        topOne: ({ storeName, filter }) => loadAll({ cnn, href: "/Data/Load/List", filter: { storeName, filter, projectName } }),
    };

    return obj;
};

module.exports = {
    bot: {
        // load: loadES6("bot"),
        load: load({ cnn: "core.es6.vn", projectName: "core.es6.vn" }),
    },
    core: {
        load: load({ cnn: "core.es6.vn", projectName: "core.es6.vn" }),
    },
    filial: {
        load: load({ cnn: "filial.p16q4" }),
    },
    maccargovn: {
        load: load({ cnn: "maccargovn.com", projectName: "maccargovn" }),
    },
};
const axios = require("axios");
const config = require("./config");
const instance = axios.create({
    baseURL: config.baseURL,
    timeout: config.timeout,
});

const loadAll = ({ href = "#", filter = {} }) => {
    let resolve;
    let p = new Promise(r => resolve = r);
    instance.post(href, filter).then((res) => {
        if (res.status === 200 && res.statusText.toLowerCase() === "ok") resolve(res.data);
        else resolve({ error: res });
    }).catch((error) => {
        console.log(error);
        resolve(error);
    });

    return p;
};

module.exports = {
    text: {
        encrypt: (filter) => loadAll({ href: "/Converter/Text/Encrypt", filter }),
        remove: {
            unicode: (filter) => loadAll({ href: "/Converter/Text/RemoveUnicode", filter }),
        },
    },
};
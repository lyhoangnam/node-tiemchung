const config = require("./config");

module.exports = {
    data: require("./api.net.data"),
    email: require("./api.net.email"),
    converter: require("./api.net.converter"),
    config,
};
const baseURL = "https://public.kiotapi.com";
const timeout = 10 * 60 * 1000;
const headers = {
    xdgk: {
        Retailer: "xdgk",
        Authorization: "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6ImF0K2p3dCJ9.eyJuYmYiOjE2ODA5MzM0NjMsImV4cCI6MTY4MTAxOTg2MywiaXNzIjoiaHR0cDovL2lkLmtpb3R2aWV0LnZuIiwiY2xpZW50X2lkIjoiOWI3ZWE2YjUtMTY4Ny00NzM3LWJiZWItMWFlMDA4MTNhMzBmIiwiY2xpZW50X1JldGFpbGVyQ29kZSI6InhkZ2siLCJjbGllbnRfUmV0YWlsZXJJZCI6Ijg2NTE1NyIsImNsaWVudF9Vc2VySWQiOiI0Njk5NiIsImNsaWVudF9TZW5zaXRpdmVBcGkiOiJUcnVlIiwiaWF0IjoxNjgwOTMzNDYzLCJzY29wZSI6WyJQdWJsaWNBcGkuQWNjZXNzIl19.JPHkYZBD3dhU6YOEXEPS2guGMFHttSSTBka5hvSg-922_-2LdSNS2k2qpZMLMjnEBcc2sPKMnjk6nrv8eKvxSHJgeojG8ZBzpfuCWYXK2DSR9M7xiz-72T7TCskt-ElW-23IwUg2TEVQwx2kHywG53p_zj6JRKiZSthF3rU14ZNrGXR53vkUK7XSxxoxphDT1jN9ndyOwXzALAEh8eDpUIYWqsOhMmvswjKwplOdTE4JyFTrDrmKQDLOXFYpr00F64Jq5kA359EzFriqfLsj4UFdNONgIN7wyzzXBD4OYxvavL3dZxloZxwBU5WLMBB7aVaq244NSQA-FUMy1ndF1w",
    },
};
const productType = {
    1: "Hàng combo",
    2: "Hàng dịch vụ",
    3: "Hàng loại khác",
};

module.exports = {
    baseURL,
    timeout,
    headers,
    productType,
};
const axios = require("axios");
const qs = require("querystring");
const config = require("./config");
const smsKiot = require("../sms").kiot;
const instance = axios.create({ baseURL: config.baseURL, timeout: config.timeout });
const cafe = {
    sms: {
        retailer_notfound: smsKiot.retailer_notfound,
    },
};

const loadData = ({ href = "", retailer = "" }) => {
    let p = new Promise((resolve) => {
        let hd = config.headers[retailer];
        if (!hd) return resolve({ error: cafe.sms.retailer_notfound });
        const headers = Object.assign({}, hd);
        instance.get(href, { headers }).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ error });
        });
    });

    return p;
};

module.exports = {
    load: {
        categories: ({ retailer, filter }) => loadData({ href: `/categories?${qs.stringify(filter)}`, retailer }),
        products: ({ retailer, filter }) => loadData({ href: `/products?${qs.stringify(filter)}`, retailer }),
        stock: ({ retailer, filter }) => loadData({ href: `/productOnHands?${qs.stringify(filter)}`, retailer }),
        purchaseorders: ({ retailer, filter }) => loadData({ href: `/purchaseorders?${qs.stringify(filter)}`, retailer }),
        transfers: ({ retailer, filter }) => loadData({ href: `/transfers?${qs.stringify(filter)}`, retailer }),
        customers: {
            detail: ({ retailer, filter }) => loadData({ href: `/customers/${filter.id}`, retailer }),
        },
    },
};
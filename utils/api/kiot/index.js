const config = require("./config");

module.exports = {
    data: require("./kiot.data"),
    config,
};
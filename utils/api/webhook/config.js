const baseURL = "https://webhook.es6.vn/";
const baseURLTest = "http://localhost.es6.vn:8080/";
const timeout = 1000 * 60 * 5;
const isTest = 0;

module.exports = {
    baseURL: !isTest ? baseURL : baseURLTest,
    timeout,
};

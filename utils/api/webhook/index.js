const config = require("./config");

module.exports = {
    config,
    socket: require("./api.webhook.socket"),
};
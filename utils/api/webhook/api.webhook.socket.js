const axios = require("axios");
const config = require("./config");
const instance = axios.create({
    baseURL: config.baseURL,
    timeout: config.timeout,
});

const loadAll = ({ href = "", filter = {} }) => {
    let p = new Promise((resolve) => {
        instance.post(href, filter).then((res) => {
            if (res.status === 200 && res.statusText.toLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res });
        }).catch((error) => {
            console.log("socket.emit.error", error);
            resolve({ error });
        });
    });

    return p;
};

module.exports = {
    emit: (filter) => loadAll({ href: "/socket/emit", filter }),
    list: (filter) => loadAll({ href: "/socket/list", filter }),
};
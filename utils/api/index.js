const apiNet = require("./net");
const apiWebhook = require("./webhook");
const apiKiot = require("./kiot");
const apiZalo = require("./zalo");
const apiTanca = require("./tanca");
const sms = require("./sms");

module.exports = {
    sms,
    net: apiNet,
    webhook: apiWebhook,
    kiot: apiKiot,
    zalo: apiZalo,
    tanca: apiTanca,
};

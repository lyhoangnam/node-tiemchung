const config = require("./config");

module.exports = {
    data: require("./tanca.data"),
    config,
};
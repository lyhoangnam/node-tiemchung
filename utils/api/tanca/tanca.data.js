const axios = require("axios");
const moment = require("moment");
const fs = require("fs-extra");
const qs = require("querystring");
const config = require("./config");
const smsTanca = require("../sms").tanca;
const instance = axios.create({ baseURL: config.baseURL, timeout: config.timeout });
const cafe = {
    format: {
        dateTime: "YYYY-MM-DD hh:mm:ss A",
    },
    sms: {
        tanca_notfound: smsTanca.tanca_notfound,
    },
};

const loadData = ({ href = "", oa = "" }) => {
    let p = new Promise(async (resolve) => {
        let hd = config.headers[oa];
        if (!hd) return resolve({ error: cafe.sms.tanca_notfound });
        let headers = Object.assign({}, hd);
        instance.get(href, { headers }).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ error });
        });
    });

    return p;
};

module.exports = {
    load: {
        tasks: {
            projects: {
                list: ({ oa }) => loadData({ href: "/list-bundle", oa }),
            },
        },
    },
};
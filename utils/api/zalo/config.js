const baseURL = "https://openapi.zalo.me/v2.0";
const authenURL = "https://oauth.zaloapp.com/v4";
const timeout = 10 * 60 * 1000;
const headers = {
    bot: {
        app_id: "4491865177415156697",
        secret_key: "t14c550uJKRq9ENMAhbK",
    },
    xdgk: {
        id: "3463203908424236078",
    },
};

module.exports = {
    baseURL,
    authenURL,
    timeout,
    headers,
};
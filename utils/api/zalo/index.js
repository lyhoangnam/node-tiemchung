const config = require("./config");

module.exports = {
    data: require("./zalo.data"),
    config,
};
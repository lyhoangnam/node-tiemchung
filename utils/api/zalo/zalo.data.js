const axios = require("axios");
const moment = require("moment");
const fs = require("fs-extra");
const qs = require("querystring");
const config = require("./config");
const smsZalo = require("../sms").zalo;
const instance = axios.create({ baseURL: config.baseURL, timeout: config.timeout });
const cafe = {
    filePath: {
        jsonData: "/zalo.config.json",
    },
    format: {
        dateTime: "YYYY-MM-DD hh:mm:ss A",
    },
    sms: {
        oa_notfound: smsZalo.oa_notfound,
    },
};

const loadData = ({ href = "", oa = "" }) => {
    let p = new Promise(async (resolve) => {
        let access_token = await getToken({ oa });
        if (!access_token) return resolve({ error: cafe.sms.oa_notfound });
        let headers = { access_token };
        instance.get(href, { headers }).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ error });
        });
    });

    return p;
};

const postData = ({ href = "", oa = "", filter = {} }) => {
    let p = new Promise(async (resolve) => {
        let access_token = await getToken({ oa });
        if (!access_token) return resolve({ error: cafe.sms.oa_notfound });
        let headers = { access_token };
        instance.post(href, filter, { headers }).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") resolve(res.data);
            else resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ error });
        });
    });

    return p;
};

const loadToken = ({ oa = "", refresh_token = "" }) => {
    let p = new Promise(async (resolve) => {
        if (!oa || !refresh_token) return null;
        let href = `${config.authenURL}/oa/access_token`;
        let hd = config.headers.bot;
        let headers = { "Content-Type": "application/x-www-form-urlencoded", secret_key: hd.secret_key };
        let data = qs.stringify({
            app_id: hd.app_id,
            refresh_token,
            grant_type: "refresh_token",
        });
        console.log("zalo.token.reload");
        axios.post(href, data, { headers }).then((res) => {
            if (res.status === 200 && res.statusText.toLocaleLowerCase() === "ok") return resolve(res.data);
            resolve({ error: res.statusText });
        }).catch((error) => {
            resolve({ error });
        });
    });

    return p;
};

const getToken = ({ oa }) => {
    let p = new Promise(async (resolve) => {
        let fileName = `${__dirname}${cafe.filePath.jsonData}`;
        let data = fs.readJSONSync(fileName, { encoding: "utf-8" });
        let tk = data[oa];
        if (!tk) return resolve(null);
        let expireTime = moment(tk.expires_time, cafe.format.dateTime);
        let diff = expireTime.diff(moment(), "minutes");
        console.log("zalo.token.diff.minutes", diff);
        if (diff <= 0) {
            let res_token = await loadToken({ oa, refresh_token: tk.refresh_token });
            const { access_token } = res_token;
            if (!access_token) return resolve(console.log("zalo.load.token.error", res_token));
            tk = res_token;
            tk.expires_time = moment().add(24, "hours").format(cafe.format.dateTime);
            data[oa] = tk;
            fs.writeJSONSync(fileName, data, { encoding: "utf-8" });
        };

        resolve(tk.access_token);
    });

    return p;
};

module.exports = {
    load: {
        followers: ({ oa, filter }) => loadData({ href: `/oa/getfollowers?${qs.stringify(filter)}`, oa }),
        profile: ({ oa, filter }) => loadData({ href: `/oa/getprofile?${qs.stringify(filter)}`, oa }),
    },
    message: {
        text: ({ oa, filter }) => postData({ href: "/oa/message", oa, filter }),
    },
};
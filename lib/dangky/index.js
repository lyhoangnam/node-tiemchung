const puppeteer = require("puppeteer");
const axios = require("axios");
const open = require("open");
const path = require("path");
const moment = require("moment");
const cheerio = require("cheerio");
const fs = require("fs-extra");
const { api, helpers } = require("../../utils");
const cafe = {
    spName: "Forever@0608",
    file: {
        origin: "./data/tiemchung/tiemchung.origin.xlsx",
        data: "./data/tiemchung/tiemchung.xlsx",
        json: "./data/tiemchung/tiemchung.result.json",
        unlist: "./data/tiemchung/tiemchung.unlist.json"
    },
    default: {
        pagesize: 100,
        href: {
            input: "https://form.tphcm.gov.vn/dangkytiemchung",
            api: {
                get: {
                    list: "https://formapi.tphcm.gov.vn/api/NhanVien/danh-sach/19476/99/0",
                },
            },
        },
        phoneNumbers: [
            // "02839404432",
            // "0983220685",
            // "0778787083",
            // "0907232748",
            // "0903020432",
            // "02839409309",
            // "02839409308",

            "0901460802",
            "0949809587",
            "0785615557",
            "0779926991",
        ],
        export: {
            columns: [
                { header: "No", key: "key", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                { header: "idEmp", key: "idEmp" },
                { header: "idCom", key: "idCom" },
                { header: "FullName", key: "hoTen", width: 25 },
                { header: "Gender", key: "gioiTinh" },
                { header: "DateOfBirth", key: "ngaySinh", width: 25 },
                { header: "Street", key: "diaChi", width: 50 },
                { header: "PhoneNumber", key: "soDt", width: 25 },
                { header: "CatePriority", key: "maNhomUt" },
                { header: "District", key: "quanHuyen" },
                { header: "Ward", key: "xaPhuong" },

                // { header: "idCom", key: "idCom", width: 25, style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
            ],
            config: {
                worksheet: {
                    name: "sheetName",
                    views: [{
                        state: "frozen",
                        xSplit: 1,
                        ySplit: 1,
                        activeCell: "A1"
                    }],
                },
            },
            rows: [],
            maNhomUt: "42. Người trên 65 tuổi",
            quanHuyen: "Quận 4",
            xaPhuong: "Phường 16",
            tinhTp: "Thành phố Hồ Chí Minh",
        },
    },
    format: {
        dateTime01: "YYYY-MM-DD HH:mm:ss",
        dateTime02: "DD/MM/YYYY",
    },
    timeout: {
        delay: 150,
        wait01: 3 * 1000,
        wait02: 1 * 1000,
        wait03: 3 * 1000,
        random: {
            from: 500,
            to: 2000,
        },
    },
    sms: {
        contact_admin: api.sms.contact_admin,
        error: {
            notInList: "Họ tên không nằm trong file excel!",
        },
    },
};
const df = cafe.default;
const isTest = 0;

const updatedata = async () => {
    let list = fs.readJSONSync(cafe.file.unlist, { encoding: "utf8" });
    console.log("list", list.length);
    let data = [];
    for (let item of list) {
        // if (item.ID > 1900) continue;
        data.push(item);
    };
    console.log("data", data.length);
    // let size = 125;
    // let arrayOfArrays = [];
    // for (var i = 0; i < data.length; i += size) {
    //     arrayOfArrays.push(data.slice(i, i + size));
    // };
    // helpers.file.log(arrayOfArrays);
    // helpers.file.log(data);
    start(data);
};

const validateData = async () => {
    let data = {
        excel: await helpers.excel.read({ fileName: cafe.file.origin }),
        website: fs.readJSONSync(cafe.file.json, { encoding: "utf8" }).data,
    }
    let result = {
        errors: [],
        unlist: [],
    };
    console.log("data.excel", data.excel.length);
    console.log("data.website", data.website.length);
    for (let iWebsite of data.website) {
        let checkList = [];
        let
            fFullName, fHoTen,
            fStreet, fDiaChi,
            fGender, fGioiTinh,
            fNgaySinh, fDateOfBirth;
        for (let iExcel of data.excel) {
            fFullName = iExcel.FullName.trim().toLowerCase();
            fHoTen = iWebsite.hoTen.trim().toLowerCase();
            if (fFullName !== fHoTen) continue;
            checkList.push(iExcel);
        };
        let reCheckList = [];
        if (checkList.length <= 0) {
            result.errors.push({
                type: "not-in-list",
                err: { FullName: iWebsite.hoTen, message: cafe.sms.error.notInList },
            });
        } else {
            if (checkList.length > 1) {
                for (let chk of checkList) {
                    fStreet = chk.Street.trim().toLowerCase();
                    fDiaChi = iWebsite.diaChi.trim().toLowerCase();
                    if (fStreet !== fDiaChi) continue;
                    reCheckList.push(chk);
                };
                if (reCheckList.length >= 1) checkList = reCheckList;
                if (reCheckList.length <= 0 || reCheckList.length > 1) {
                    for (let chk of checkList) {
                        fNgaySinh = moment(iWebsite.ngaySinh).format(cafe.format.dateTime01);
                        fDateOfBirth = moment(chk.DateOfBirth, cafe.format.dateTime02).format(cafe.format.dateTime01);
                        if (fNgaySinh !== fDateOfBirth) continue;
                        checkList = [chk];
                        break;
                    };
                };
            };
            let isError = false;
            let err = {
                Gender: { excel: null, website: null },
                DateOfBirth: { excel: null, website: null },
                Street: { excel: null, website: null },
            };
            fNgaySinh = moment(iWebsite.ngaySinh).format(cafe.format.dateTime01);
            fDateOfBirth = moment(checkList[0].DateOfBirth, cafe.format.dateTime02).format(cafe.format.dateTime01);
            fGioiTinh = (iWebsite.gioiTinh === 0 ? "nam" : "nữ");
            fGender = checkList[0].Gender.trim().toLowerCase();
            if (fNgaySinh !== fDateOfBirth) {
                isError = true;
                err.message = "Sai ngày tháng năm sinh";
                err.DateOfBirth.excel = fDateOfBirth;
                err.DateOfBirth.website = fNgaySinh;
            };
            if (fGender !== fGioiTinh) {
                isError = true;
                err.message = "Sai giới tính";
                err.Gender.excel = fGender;
                err.Gender.website = fGioiTinh;
            };
            if (isError) result.errors.push({ type: "not-match", idEmp: iWebsite.idEmp, soDt: iWebsite.soDt, ID: checkList[0].ID, FullName: checkList[0].FullName, err });
        };
    };
    for (let iExcel of data.excel) {
        let isExist = false;
        let { FullName, Gender, DateOfBirth, Street } = iExcel;
        fDateOfBirth = moment(DateOfBirth, cafe.format.dateTime02).format(cafe.format.dateTime01);
        let jsonExcel = JSON.stringify({ FullName, DateOfBirth: fDateOfBirth });
        for (let iWebsite of data.website) {
            let { hoTen, ngaySinh } = iWebsite;
            let fNgaySinh = moment(ngaySinh).format(cafe.format.dateTime01);
            let jsonWebsite = JSON.stringify({ FullName: hoTen, DateOfBirth: fNgaySinh });
            if (jsonWebsite !== jsonExcel) continue;
            isExist = true;
            break;
        };
        if (!isExist) result.unlist.push(iExcel);
    };
    for (let item of result.errors) {
        if (!item.err.DateOfBirth) continue;
        if (!item.err.DateOfBirth.excel) continue;
        let count = 0;
        for (let obj of data.website) {
            let fFullName = item.FullName.trim().toLowerCase();
            let fHoTen = obj.hoTen.trim().toLowerCase();
            if (fFullName !== fHoTen) continue;
            count += 1;
        };
        if (count > 1) {
            item.type = "not-in-list";
            item.err = { message: cafe.sms.error.notInList };
        };
    };
    helpers.file.log(result);
};

const getData = async () => {
    let res_cookies = await api.net.data.bot.load.cookies({ spName: cafe.spName, filter: { host_key: "gov.vn" } });
    if (!res_cookies || !res_cookies.isSuccess) return { is_success: false, error: cafe.sms.contact_admin };
    let cookies = helpers.cookies.map(res_cookies.data);
    let browser, page;
    let { random } = cafe.timeout;

    /*********|Start browser|*********/
    helpers.file.log("browser start!");
    browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
        // executablePath: "C:/Users/Administrator/AppData/Local/Google/Chrome/Application/chrome.exe",
        userProfileDir: "C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/Default",
        // executablePath: "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe",
        args: ["--start-maximized"]
    });
    let href = df.href.input;
    helpers.file.log("dangkytiemchung", href);
    try {
        /*********|Use cookie|*********/
        let result = {
            data: [],
            pageCount: 0,
            totalRecords: 0,
        };
        page = await browser.newPage();
        await page.setCookie(...cookies.list);
        await page.setRequestInterception(true);
        page.on("request", async (req) => {
            if (req.url().indexOf(df.href.api.get.list) !== 0 || req.method() !== "GET") return req.continue();
            req.continue();

            /*********|handle data|*********/
            let url = new URL(req.url());
            url.searchParams.set("pagesize", df.pagesize);
            url.search = url.searchParams.toString();
            let change = url.toString();
            let headers = req.headers();
            let res_detail = await axios.get(change, { headers });
            if (!res_detail || !res_detail.data) return;
            let { pageCount, totalRecords } = res_detail.data;
            result.pageCount = pageCount;
            result.totalRecords = totalRecords;
            helpers.file.log("pageCount", pageCount);
            result.data = [...result.data, ...res_detail.data.data];
            for (let page = 1; page < pageCount; page++) {
                let x = helpers.text.random.number(random.from, random.to);
                await helpers.timer.wait.promise(x);
                change = `https://formapi.tphcm.gov.vn/api/NhanVien/danh-sach/19476/99/${page}?pagesize=${df.pagesize}&keyword=&noidia=0`;
                helpers.file.log("get", change);
                res_detail = await axios.get(change, { headers });
                if (!res_detail || !res_detail.data) continue;
                result.data = [...result.data, ...res_detail.data.data];
            };
            helpers.file.log("result", result);
            fs.writeFileSync(cafe.file.json, JSON.stringify(result), "utf8");
            await browser.close();
        });
        await page.goto(href, { timeout: 5 * 60 * 1000 });
        await page.content();
    } catch (error) {
        console.log("error", error);
        console.log("error", error);
        await browser.close();
        return { is_success: false, error };
    };
};

const start = async (data) => {
    let res_cookies = await api.net.data.bot.load.cookies({ spName: cafe.spName, filter: { host_key: "gov.vn" } });
    if (!res_cookies || !res_cookies.isSuccess) return { is_success: false, error: cafe.sms.contact_admin };
    let cookies = helpers.cookies.map(res_cookies.data);
    let browser, page;
    if (!data) data = await helpers.excel.read({ fileName: cafe.file.data });
    let { wait01, wait02, delay } = cafe.timeout;

    /*********|Start browser|*********/
    helpers.file.log("browser start!", data.length);
    browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
        // executablePath: "C:/Users/Administrator/AppData/Local/Google/Chrome/Application/chrome.exe",
        userProfileDir: "C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/Default",
        // executablePath: "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe",
        args: ["--start-maximized"]
    });
    for (let item of data) {
        let { ID, FullName, DateOfBirth, PhoneNumber, IDCard, SHI, CatePriority, Gender, City, District, Ward, Street } = item;
        let x = helpers.text.random.number(0, df.phoneNumbers.length - 1);
        if (!PhoneNumber) PhoneNumber = df.phoneNumbers[x];
        let href = `${df.href.input}?id=${ID}`;
        helpers.file.log("dangkytiemchung", href);
        try {
            // fs.emptyDirSync("./images");

            /*********|Use cookie|*********/
            page = await browser.newPage();
            await page.setCookie(...cookies.list);
            await page.goto(href, { timeout: 5 * 60 * 1000 });
            // await page.setViewport({ width: 1200, height: 800 });
            await page.content();
            let selector = {
                FullName: "#input-85",
                DateOfBirth: "#input-89",
                IDCard: "#input-108",
                SHI: "#input-111",
                CatePriority: {
                    input: "#input-93",
                    mask: ".v-list-item__mask",
                },
                Gender: {
                    input: "#input-99",
                    mask: ".v-list-item__mask",
                },
                PhoneNumber: "#input-105",
                City: {
                    input: "#input-115",
                    mask: ".v-list-item__mask",
                },
                District: {
                    input: "#input-121",
                    mask: ".v-list-item__mask",
                },
                Ward: {
                    input: "#input-127",
                    mask: ".v-list-item__mask",
                },
                Street: "#input-133",
                btnSubmit: `button[type="submit"]`,
            };
            await page.waitForSelector(selector.FullName);
            await page.focus(selector.FullName);
            await helpers.timer.wait.promise(wait01);
            await page.type(selector.FullName, FullName, { delay });

            await helpers.timer.wait.promise(wait02);
            await page.type(selector.DateOfBirth, DateOfBirth, { delay });

            if (IDCard) {
                await helpers.timer.wait.promise(wait02);
                await page.type(selector.IDCard, IDCard, { delay });
            };
            if (SHI) {
                await helpers.timer.wait.promise(wait02);
                await page.type(selector.SHI, SHI, { delay });
            };

            await page.type(selector.CatePriority.input, CatePriority, { delay });
            await helpers.timer.wait.promise(wait02);
            await page.click(selector.CatePriority.mask);
            await helpers.timer.wait.promise(wait02);

            await page.type(selector.Gender.input, Gender, { delay });
            await helpers.timer.wait.promise(wait02);
            await page.click(selector.Gender.mask);
            await helpers.timer.wait.promise(wait02);

            await page.focus(selector.PhoneNumber);
            await helpers.timer.wait.promise(wait01);
            await page.type(selector.PhoneNumber, PhoneNumber, { delay });

            if (City !== df.export.tinhTp) {
                await page.type(selector.City.input, City, { delay });
                await helpers.timer.wait.promise(wait02);
                await page.click(selector.City.mask);
                await helpers.timer.wait.promise(wait02);
            };

            await page.type(selector.District.input, District, { delay });
            await helpers.timer.wait.promise(wait02);
            await page.click(selector.District.mask);
            await helpers.timer.wait.promise(wait02);

            await page.type(selector.Ward.input, Ward, { delay });
            await helpers.timer.wait.promise(wait02);
            await page.click(selector.Ward.mask);
            await helpers.timer.wait.promise(wait02);

            await page.focus(selector.Street);
            await helpers.timer.wait.promise(wait01);
            await page.type(selector.Street, Street, { delay });

            if (isTest) {
                await helpers.timer.wait.promise(5 * 60 * 1000);
            } else {
                await helpers.timer.wait.promise(wait02);
                await page.click(selector.btnSubmit);
                await helpers.timer.wait.promise(wait02);
                await page.screenshot({ path: `./data/images/dangkytiemchung/${ID}.png`, fullPage: true });
                await page.close();
            };
        } catch (error) {
            console.log("error", error);
            await browser.close();
            return { is_success: false, error };
        };
    };
};

const exportData = async () => {
    let data = fs.readJSONSync(cafe.file.json, { encoding: "utf8" }).data;
    console.log("data", data.length);
    let sheet0 = {
        columns: df.export.columns,
        config: df.export.config,
        rows: [],
    };
    for (let [i, item] of data.entries()) {
        let {
            idEmp, idCom, hoTen, ngaySinh,
            gioiTinh, maNhomUt, soDt, diaChi,
        } = item;
        let quanHuyen = df.export.quanHuyen;
        let xaPhuong = df.export.xaPhuong;
        gioiTinh = gioiTinh === 0 ? "Nam" : "Nữ";
        maNhomUt = df.export.maNhomUt;
        ngaySinh = moment(ngaySinh).format(cafe.format.dateTime02);
        sheet0.rows.push({
            key: i + 1,
            idEmp, idCom, hoTen, ngaySinh, gioiTinh,
            maNhomUt, soDt, diaChi, quanHuyen, xaPhuong,
        });
    };
    let filter = {
        fileName: "tiemchung.export.xlsx",
        data: [sheet0],
    };
    let res = await helpers.excel.export(filter);
    console.log("res", res);
    if (res.is_success && res.data) {
        await open(path.resolve(res.data.fileName));
    };

    return res;
};

module.exports = {
    start,
    get: {
        data: getData,
    },
    validate: {
        data: validateData,
    },
    update: {
        data: updatedata,
    },
    export: {
        data: exportData,
    },
};
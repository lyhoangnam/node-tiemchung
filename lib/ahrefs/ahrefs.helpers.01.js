const fs = require("fs-extra");
const arraySort = require("array-sort");
const path = require("path");
const moment = require("moment");
const { api, helpers } = require("../../utils");
const { stored } = api.net.config;
const cafe = require("./config");
const { numberStyle } = helpers.excel;
const { cpo } = helpers.text;
const df = cafe.default;
const isTest = 1;

const groupKeywords = async ({ member, keywords, data, config, x }) => {
    if (!config || !config.ignore || !Array.isArray(config.ignore.reg)) return { error: cafe.sms.invalid_inputs };
    helpers.file.log("ahrefs.keywords.group.start", moment().format(cafe.format.dateTime));
    const checkIgnore = ({ Keyword }) => {
        if (!Keyword) return true;
        let isIgnore = config.ignore.list.includes(Keyword);
        if (isIgnore) return true;
        for (let regText of config.ignore.reg) {
            let reg = new RegExp(regText, "gi");
            isIgnore = reg.test(Keyword);
            if (isIgnore) break;
        };
        return isIgnore;
    };
    const handleReplaceText = ({ Keyword }) => {
        let data = { text: Keyword, dup: [] };
        if (!Keyword) return data;
        Keyword = Keyword.trim().replace(/\s\s/g, "");
        for (let txtFrom of Object.keys(config.txtReplace.text)) {
            let txtTo = config.txtReplace.text[txtFrom];
            let reg = new RegExp(txtFrom, "gi");
            if (reg.test(data.text)) {
                data.dup.push(data.text);
                data.text = data.text.replace(txtFrom, txtTo);
            };
        };

        return data;
    };
    const handleSaveDiacritics = ({ words, ignore }) => {
        let data = {
            words: {},
            diacritics: Object.assign({}, config.txtReplace.text),
        };
        if (!words || !Array.isArray(ignore)) return data;
        let keys = Object.keys(words).sort().sort((a, b) => a.length - b.length);
        for (let Keyword of keys) {
            let key = {
                main: Keyword,
                rd: helpers.text.remove.diacritics(Keyword),
            };
            if (key.main !== key.rd) {
                // trường hợp này có unicode
                let rd = data.diacritics[key.rd] || {};
                rd[key.main] = helpers.text.sum.ascii(key.main);
                data.diacritics[key.rd] = rd;
            };
            if (!ignore.includes(key.main)) data.words[key.main] = words[key.main];
        };

        return data;
    };
    const handlePermuteWords = ({ words = {}, Keyword }) => {
        if (!Keyword) return {};
        let word;
        let list = [];
        let arr = Keyword.split(" ");
        let n = arr.length;
        if (arr.length === 1) {
            word = words[Keyword];
            if (!word) word = { related: [] };
            list.push(Keyword);
            if (!word.related.includes(Keyword)) word.related.push(Keyword);
            words[Keyword] = word;
        } else {
            for (let w = 0; w < n - 1; w++) {
                let sub = [arr[w]];
                for (let i = w + 1; i < n; i++) {
                    sub.push(arr[i]);
                    let text = sub.join(" ");
                    if (list.includes(text)) continue;
                    let isIgnore = checkIgnore({ Keyword: text });
                    if (isIgnore) continue;
                    word = words[text];
                    if (!word) word = { related: [] };
                    list.push(text);
                    if (!word.related.includes(Keyword)) word.related.push(Keyword);
                    words[text] = word;
                };
            };
        };

        return { words, list };
    };
    const handleReverseDiacritics = ({ diacritics, list }) => {
        let data = {
            diacritics: Object.assign({}, diacritics),
            dup: [],
        };
        if (!diacritics || !Array.isArray(list)) return data;
        for (let txtRD of list) {
            if (data.diacritics[txtRD]) continue;// nếu dịch rồi thì thôi
            let text = txtRD;
            let obj = handlePermuteWords({ Keyword: txtRD });
            for (let word of obj.list) {
                let rd = data.diacritics[word];
                if (!rd) continue;
                text = text.replace(word, rd);
            };
            data.dup.push(txtRD);
            data.diacritics[txtRD] = text;
        };

        return data;
    };
    const handleSimilarWords01 = ({ keywords, merge, diacritics, words }) => {
        let data = {
            words: {},
            merge: Object.assign({}, merge),
            diacritics: Object.assign({}, diacritics),
            v1: {
                related: {},
                children: {},
                arr: {},
            },
            v2: {
                merge: {},
                group: {},
            },
            dup: {
                diacritics: [],
                meaning: [],
            },
            save: {
                diacritics: [],
            },
        };
        if (!keywords || !merge || !words) return data;
        let keys = {
            words: [],
            v1: {
                related: [],
                children: [],
                arr: [],
            },
            v2: {
                merge: [],
                group: [],
            },
            data: {
                words: [],
                merge: [],
            },
        };


        /*********|01. Lọc từ gần nghĩa|*********/
        let sml = handleSimilarWords({ keywords, words, merge });
        data.v1 = sml.v1;
        data.v2 = sml.v2;
        data.v1.arr = [];
        data.words = sml.words;
        data.merge = sml.merge;
        keys.v2.merge = Object.keys(data.v2.merge).sort().sort((a, b) => b.length - a.length);
        keys.v2.group = Object.keys(data.v2.group).sort().sort((a, b) => b.length - a.length);


        /*********|04. Cập nhật data diacritics|*********/
        keys.data.words = Object.keys(data.words).sort().sort((a, b) => b.length - a.length);
        for (let Keyword of keys.data.words) {
            let key = {
                main: Keyword,
                rd: helpers.text.remove.diacritics(Keyword),
            };
            let obj = {
                main: data.words[key.main],
                rd: data.words[key.rd],
            };
            let txtDiacritic = data.diacritics[key.rd];
            let isRemoveUnicode = key.main === key.rd && !txtDiacritic;
            if (isRemoveUnicode) continue;// từ khóa đã remove unicode thì bỏ qua
            if (!txtDiacritic) {
                data.dup.diacritics.push(key.rd);
                data.diacritics[key.rd] = key.main;
            };
            if (!obj.main || !obj.rd) continue;// không có một trong 2 có nghĩa là không bị dup
            let related = {
                main: obj.main.related,
                rd: [],
            };
            for (let rlt of related.main) {
                let rltRD = helpers.text.remove.diacritics(rlt);
                if (rlt !== rltRD) {
                    let txtRLT = data.diacritics[rltRD];
                    if (!txtRLT) {
                        data.dup.diacritics.push(rltRD);
                        data.diacritics[rltRD] = rlt;// cái này nhìu nắm :v
                    };
                };
                if (!related.rd.includes(rltRD)) related.rd.push(rltRD);
            };
            for (let rlt of obj.rd.related) {
                if (related.main.includes(rlt)) continue;
                if (related.rd.includes(rlt)) continue;
                if (rlt === key.main) continue;
                if (rlt === key.rd) continue;
                if (!data.save.diacritics.includes(rlt)) data.save.diacritics.push(rlt);
                related.main.push(rlt);
            };
            data.words[key.main].related = related.main;
            data.dup.diacritics.push(key.rd);
            delete data.words[key.rd];
        };


        /*********|05. Cập nhật lại data merge|*********/
        keys.data.merge = Object.keys(data.merge).sort().sort((a, b) => a.length - b.length);
        for (let keyMerge of keys.data.merge) {
            let key = {
                from: keyMerge,
                to: null,
                updated: null,
                delete: null,
            };
            let word = {
                from: data.words[key.from],
                to: null,
                updated: null,
                delete: null,
            };
            if (!word.from) continue;
            key.to = data.merge[key.from];
            word.to = data.words[key.to];
            if (!word.to) continue;
            key.updated = key.from;
            key.delete = key.to;
            if (word.to.Volume > word.from.Volume) {
                key.updated = key.to;
                key.delete = key.from;
            };
            word.updated = data.words[key.updated];
            word.delete = data.words[key.delete];
            let { related } = word.updated;
            if (!related.includes(key.delete)) related.push(key.delete);
            for (let rlt of word.delete.related) if (!related.includes(rlt) && rlt !== key.updated) related.push(rlt);
            data.words[key.updated].related = related;
            data.dup.meaning.push(key.delete);
            delete data.words[key.delete];
            // delete data.merge[key.updated];
            data.merge[key.delete] = key.updated;
        };

        let step = handleReverseDiacritics({
            diacritics: data.diacritics,
            list: data.save.diacritics,
        });
        data.diacritics = step.diacritics;
        data.dup.diacritics = [...data.dup.diacritics, ...step.dup];


        /*********|06. Cập nhật key diacritics|*********/
        for (let keyGroup of keys.v2.group) {
            let key = {
                main: keyGroup,
                diacritic: null,
                merge: null,
                rd: helpers.text.remove.diacritics(keyGroup),
                child: null,
            };
            let obj = {
                main: null,
                diacritic: null,
                merge: null,
                updated: {},
            };
            if (key.main === key.rd) {
                // trường hợp này là từ khóa remove unicode
                key.diacritic = data.diacritics[key.main] || key.main;
                if (key.diacritic !== key.main) {
                    // cập nhật key diacritic
                    obj.main = data.v2.group[key.main];
                    obj.diacritic = data.v2.group[key.diacritic];
                    obj.updated = Object.assign(obj.main, obj.diacritic);
                    data.v2.group[key.diacritic] = obj.updated;
                    data.dup.diacritics.push(key.main);
                    delete data.v2.group[key.main];
                    key.main = key.diacritic;
                };
            };
            key.merge = data.merge[key.main] || key.main;
            if (key.merge !== key.main) {
                // trường hợp này cần lấy theo key đã gom nhóm
                try {
                    obj.main = data.v2.group[key.main] || {};
                    obj.merge = data.v2.group[key.merge];
                    obj.updated = Object.assign(obj.main, obj.merge);
                    data.v2.group[key.merge] = obj.updated;
                    data.dup.meaning.push(key.main);
                    delete data.v2.group[key.main];
                    key.main = key.merge;
                } catch (error) {
                    helpers.file.log("error", key, obj, error);
                };
            };
            // start update child
            obj.main = data.v2.group[key.main] || {};//edit here
            obj.updated = {};
            for (let keyChild of Object.keys(obj.main)) {
                key.child = keyChild;
                key.rd = helpers.text.remove.diacritics(key.child);
                if (key.child === key.rd) {
                    // trường hợp này là từ khóa remove unicode
                    key.diacritic = data.diacritics[key.child] || key.child;
                    key.child = key.diacritic;
                };
                key.merge = data.merge[key.child] || key.child;
                if (key.merge !== key.child) key.child = key.merge;
                if (key.child !== key.main) obj.updated[key.child] = {};
            };
            data.v2.group[key.main] = obj.updated;
        };

        /*********|07. Xử lý cặp key 1-1 data.v2.group|*********/
        keys.v2.group = Object.keys(data.v2.group);
        for (let keyGroup of keys.v2.group) {
            let key = {
                v22: keyGroup,
                v23: null,
                child: null,
                v3: {
                    updated: null,
                    delete: null,
                },
            };
            let obj = {
                v22: data.v2.group[key.v22],
                v23: null,
                v3: {
                    from: data.words[key.v22],
                    to: null,
                    updated: null,
                    delete: null,
                },
            };
            let keys22 = Object.keys(obj.v22);
            if (!keys22.length) {
                delete data.v2.group[keyGroup];
                continue;
            };
            if (keys22.length === 1) {
                key.child = keys22.shift();
                delete data.v2.group[key.v22];
                obj.v3.to = data.words[key.child];
                if (!obj.v3.from && !obj.v3.to) continue;// related là nhóm con sau vẫn phải xét lại
                if (!obj.v3.from) {
                    data.dup.meaning.push(key.v22);
                    continue;
                };
                if (!obj.v3.to) {
                    data.dup.meaning.push(key.child);
                    continue;
                };
                // tời đây thì yêu cầu đã có from và to
                // đối với cặp 1-1 thì 1 trong 2 là parent hoặc child có ở words.v3 là được; thằng còn lại sẽ vào duplicate
                if (obj.v3.to.Volume > obj.v3.from.Volume) {
                    key.v3.updated = key.child;
                    key.v3.delete = key.v22;
                } else {
                    key.v3.updated = key.v22;
                    key.v3.delete = key.child;
                };
                obj.v3.updated = data.words[key.v3.updated];
                obj.v3.delete = data.words[key.v3.delete];
                let { related } = obj.v3.updated;
                if (!related.includes(key.v3.delete)) related.push(key.v3.delete);
                for (let rlt of obj.v3.delete.related) if (!related.includes(rlt) && rlt !== key.v3.updated) related.push(rlt);
                data.words[key.v3.updated].related = related;
                data.dup.meaning.push(key.v3.delete);
                delete data.words[key.v3.delete];
                // delete data.merge[key.v3.updated];
                data.merge[key.v3.delete] = key.v3.updated;
                continue;
            };
            let isExist = false;
            key.v23 = JSON.stringify(keys22);
            obj.v23 = data.v1.arr[key.v23] || [];
            if (obj.v3.from) isExist = true;
            obj.v23.push({ key: key.v22, isExist });
            data.v1.arr[key.v23] = obj.v23;
        };

        /*********|08. xóa các nhóm trùng ở data.v2.group|*********/
        keys.v1.arr = Object.keys(data.v1.arr);
        for (let keyArr of keys.v1.arr) {
            let arr = data.v1.arr[keyArr];
            if (arr.length < 2) continue;// không trùng thì bỏ qua
            let max = { length: 0, key: null };
            for (let item of arr) {
                let { key, isExist } = item;
                if (isExist) max = { length: key.length, key };
                if (key.length > max.length) max = { length: key.length, key };
            };
            for (let item of arr) {
                let { key } = item;
                if (key === max.key) continue;
                data.dup.meaning.push(key);
                delete data.v2.group[key];
            };
        };

        /*********|09. cập nhật reverse diacritics ở data.words|*********/
        keys.data.words = Object.keys(data.words).sort().sort((a, b) => a.length - b.length);
        for (let key of keys.data.words) {
            let word = data.words[key];
            let related = [];
            for (let rltV3 of word.related) {
                let rlt = rltV3;
                if (data.save.diacritics.includes(rltV3)) rlt = data.diacritics[rltV3] || rlt;
                if (!related.includes(rlt)) related.push(rlt);
            };
            data.words[key].related = related;
        };

        return data;
    };
    const handleSimilarWords05 = ({ keywords, merge, words }) => {
        let keys = {
            words: Object.keys(words).sort((a, b) => b.length - a.length),
            merge: [],
            group: [],
            delete: [],
        };
        let data = {
            words: {},
            v5: {},
            merge,
        };
        for (let Keyword of keys.words) {
            let obj = handlePermuteWords({ Keyword });
            let word = { related: obj.list };
            data.words[Keyword] = word;
        };
        let sml = handleSimilarWords({
            keywords: {},
            words: data.words,
            merge: {},
            isAssign: false,
            isIgnoreSubkey: false,
        });
        keys.merge = Object.keys(sml.v2.merge);
        keys.group = Object.keys(sml.v2.group).sort((a, b) => b.length - a.length);
        for (let Keyword of keys.group) {
            let item = sml.v2.group[Keyword];
            let keysUpdate = Object.keys(item);
            for (let keyUpdate of keysUpdate) if (!keys.delete.includes(keyUpdate)) keys.delete.push(keyUpdate);
        };
        for (let keyGroup of keys.group) {
            let kw = words[keyGroup];
            let item = {
                group: sml.v2.group[keyGroup],
                delete: null,
                delete: null,
            };
            let keysChild = Object.keys(item.group);
            if (!kw || !keysChild.length) continue;
            let key = {
                delete: null,
                update: keyGroup,
            };
            for (let keyDelete of keysChild) {
                key.delete = keyDelete;
                item.delete = cpo(data.v5[key.delete] || words[key.delete]);
                item.update = cpo(data.v5[key.update] || words[key.update]);
                item.delete.related = item.delete.related || [];
                item.update.related = item.update.related || [];
                if (!item.update.related.includes(key.delete)) item.update.related.push(key.delete);
                for (let rlt of item.delete.related) if (!item.update.related.includes(rlt)) item.update.related.push(rlt);
                data.merge[key.delete] = key.update;
                let isKeysChild = Array.isArray(item.update.keysChild) && item.update.keysChild.length;
                if (isKeysChild) {
                    item.update.keysChild.push(key.delete);
                    item.update.children.push(cpo(item.delete));
                    data.v5[key.update] = cpo(item.update);
                } else {
                    isKeysChild = Array.isArray(item.delete.keysChild) && item.delete.keysChild.length;
                    if (isKeysChild) {
                        for (let child of item.delete.children) {
                            if (!item.update.related.includes(child.Keyword)) item.update.related.push(child.Keyword);
                            let childRelated = child.related || [];
                            for (let rlt of childRelated) if (!item.update.related.includes(rlt)) item.update.related.push(rlt);
                        };
                    };
                    data.v5[key.update] = cpo(item.update);
                };
            };
        };
        for (let Keyword of keys.words) {
            let item = data.v5[Keyword];
            let kw = words[Keyword];
            if (item || keys.delete.includes(Keyword)) continue;
            data.v5[Keyword] = cpo(kw);
        };
        for (let keyDelete of keys.delete) delete data.v5[keyDelete];
        let stepAnalyze = handleAnalyzeVolume({ keywords, words: data.v5 });
        data.v5 = stepAnalyze.words;
        helpers.file.log("keys.group", JSON.stringify(keys.group));
        helpers.file.log("keys.delete", JSON.stringify(keys.delete));
        helpers.file.log("data.words", JSON.stringify(data.words));
        helpers.file.log("sml.group", JSON.stringify(sml.v2.group));

        return data;
    };

    /*********|first config|*********/
    helpers.file.log("config.ignore.reg.01", config.ignore.reg.length);
    const res_kwi = await api.net.data.bot.load.list({ storeName: stored.link.GetAhrefsKeywordsIgnore, filter: {} });
    if (res_kwi.isSuccess && Array.isArray(res_kwi.data)) {
        for (let item of res_kwi.data) {
            const { KeywordName } = item;
            config.ignore.reg.push(KeywordName);
        };
    };
    helpers.file.log("config.ignore.reg.02", config.ignore.reg.length);

    helpers.file.log("ahrefs.keywords.group.data", data.length);
    let list = { keywords: [], input: [], related: [], remove: [] };
    let temp = {
        keywords: {},
        words: {
            v1: {},// tách tất cả các từ
            v2: {},// từ đồng nghĩa
            v3: {},// từ gần nghĩa
            v4: {},// những từ còn lại chưa gom nhóm được
            v5: {},
            // sau khi gom nhóm vẫn còn các keywords chính có liên quan ngữ nghĩa
            // liên qua ngữ nghĩa ở v5 không phải liên quan từ phụ thuộc(related) mà là liên quan cách diễn giải
            // vd: kem trị tàn nhan, kem trị nám tàn nhan, kem trị nám: các từ này nên cùng 1 nhóm
        },
        related: {
            v1: {},// xử lý từ đồng nghĩa
            v22: {},// xử lý nhóm parent-child
        },
        merge: {},
        diacritics: {},
        heads: [],
        rare: [],
        details: [],
        chart: {},
    };
    let keys = {
        keywords: [],
        words: {
            v1: [],
            v2: [],
            v3: [],
            v4: [],
        },
        merge: {},
        diacritics: [],
    };
    let updated = {
        remove: [],
        rare: [],
        duplicate: {
            meaning: [],
            diacritic: [],
        },
        ignore: [],
        diacritics: [],
        branch: [],
        children: [],
    };

    /*********|Step 01: tách các từ khóa ra rồi nhóm lại để tìm sự liên quan|*********/
    for (let kw of keywords) {
        list.input.push(kw.text);
        if (!list.related.includes(kw.text)) list.related.push(kw.text);
        for (let kwrlt of kw.related) {
            if (!list.related.includes(kwrlt)) list.related.push(kwrlt);
        };
    };
    for (let Keyword of list.related) {
        data.push({
            Keyword,
            HostName: "google.com",
            Position: 11,
            Traffic: 0,
            Href: "google.com",
        });
    };
    for (let item of data) {
        let Keyword = item.Keyword;
        let isIgnore = checkIgnore({ Keyword });
        if (isIgnore) {
            updated.remove.push(Keyword);
            continue;
        };
        let { text, dup } = handleReplaceText({ Keyword });
        Keyword = text;
        updated.duplicate.meaning = [...updated.duplicate.meaning, ...dup];
        const { HostName, Position, Volume, KD, Traffic, Href } = item;
        let { words } = handlePermuteWords({ words: temp.words.v1, Keyword });
        temp.words.v1 = words;
        let obj = temp.keywords[Keyword];
        if (!obj) obj = { Volume, KD, items: [] };
        obj.items.push({ HostName, Position, Traffic, Href });
        temp.keywords[Keyword] = obj;
    };
    keys.keywords = Object.keys(temp.keywords);
    helpers.file.log("keys.keywords", keys.keywords.length);


    /*********|Step 02: xử lý sơ bộ words.v1 và tìm các từ khóa đồng nghĩa|*********/
    let step02 = handleRareKeywords({ keywords: temp.keywords, words: temp.words.v1 });
    temp.words.v1 = step02.words;
    temp.related.v1 = step02.related;
    updated.rare = step02.rare;// first time
    keys.words.v1 = Object.keys(temp.words.v1);
    helpers.file.log("keys.words.v1", keys.words.v1.length);


    /*********|Step 03: đối với các từ đồng nghĩa chỉ giữ lại một từ có length dài nhất|*********/
    let step03 = handleRemoveSynonym({ words: temp.words.v1, tRelated: temp.related.v1 });
    temp.words.v2 = step03.words;// first time
    temp.merge = step03.merge;// first time
    updated.duplicate.meaning = [...updated.duplicate.meaning, ...step03.dup];


    /*********|Step 04: xử lý diacritics và chuyển từ còn thiếu từ words.v1 sang words.v2|*********/
    updated.ignore = [...updated.rare, ...updated.duplicate.meaning];
    helpers.file.log("updated.ignore", updated.ignore.length);
    let step04 = handleSaveDiacritics({ words: temp.words.v1, ignore: updated.ignore });
    temp.words.v2 = Object.assign(temp.words.v2, step04.words);
    temp.diacritics = step04.diacritics;// first time
    updated.duplicate.diacritic = Object.keys(step04.diacritics);// first time
    keys.words.v2 = Object.keys(temp.words.v2);
    helpers.file.log("temp.diacritics.01", Object.keys(step04.diacritics).length);
    helpers.file.log("keys.words.v2", keys.words.v2.length);


    /*********|Step 05: cập nhật lại danh sách diacritics - chọn từ có unicode nhiều nhất|*********/
    let step05 = handleMaxDiacritics({ diacritics: temp.diacritics });
    temp.diacritics = Object.assign(temp.diacritics, step05.diacritics);
    helpers.file.log("temp.diacritics.02", Object.keys(temp.diacritics).length);


    /*********|Step 06: kiểm tra nhóm những từ gần nghĩa ở words.v2|*********/
    let step06 = handleSimilarWords01({
        keywords: temp.keywords,
        merge: temp.merge,
        diacritics: temp.diacritics,
        words: temp.words.v2,
        config,
    });
    temp.words.v3 = step06.words;// first time
    temp.merge = step06.merge;// đã gộp
    temp.related.v22 = step06.v2.group;
    temp.diacritics = step06.diacritics;// đã gộp
    updated.duplicate.diacritic = [...updated.duplicate.diacritic, ...step06.dup.diacritics];
    updated.duplicate.meaning = [...updated.duplicate.meaning, ...step06.dup.meaning];
    updated.diacritics = step06.save.diacritics;// first time
    helpers.file.log("temp.words.v3", Object.keys(temp.words.v3).length);


    /*********|Step 16: xử lý từ gần nghĩa ở words.v3|*********/
    updated.ignore = [...updated.rare, ...updated.duplicate.meaning, ...updated.duplicate.diacritic];
    let step16 = handleSimilarWords02({
        keywords: temp.keywords,
        merge: temp.merge,
        diacritics: temp.diacritics,
        words: temp.words.v3,
        group: temp.related.v22,
        v2: temp.words.v2,
        ignore: updated.ignore,
    });
    temp.words.v3 = step16.words;
    temp.merge = step16.merge;// đã gộp
    temp.diacritics = step16.diacritics;// đã gộp
    updated.duplicate.diacritic = [...updated.duplicate.diacritic, ...step16.dup.diacritics];
    updated.duplicate.meaning = [...updated.duplicate.meaning, ...step16.dup.meaning];
    updated.diacritics = [...updated.diacritics, ...step16.save.diacritics];
    updated.children = step16.children;// first time
    list.keywords = step16.keywords;
    keys.words.v3 = Object.keys(temp.words.v3);
    temp.words.v4 = step16.v4;
    updated.branch = step16.branch;// first time
    helpers.file.log("keys.words.v3", keys.words.v3.length);
    helpers.file.log("keys.words.v4.01", keys.words.v4.length);


    /*********|Step 21: xử lý từ gần nghĩa ở words.v4|*********/
    let step21 = handleSimilarWords03({
        keywords: temp.keywords,
        merge: temp.merge,
        diacritics: temp.diacritics,
        words: temp.words.v4,
        branch: updated.branch,
        config,
    });
    temp.words.v4 = step21.words;
    temp.merge = step21.merge;// đã gộp
    temp.diacritics = step21.diacritics;// đã gộp
    updated.duplicate.diacritic = [...updated.duplicate.diacritic, ...step21.dup.diacritics];
    updated.duplicate.meaning = [...updated.duplicate.meaning, ...step21.dup.meaning];
    keys.words.v4 = Object.keys(temp.words.v4);
    keys.merge = Object.keys(temp.merge);
    helpers.file.log("keys.words.v4.02", keys.words.v4.length);
    helpers.file.log("keys.merge", keys.merge.length);

    let step23 = handleSimilarWords05({
        keywords: temp.keywords,
        words: temp.words.v4,
        merge: temp.merge,
    });
    temp.words.v5 = step23.v5;
    temp.merge = step23.merge;


    /*********|Step 22: thiết kế outline content|*********/
    let step22 = handleOutLine({ keywords: temp.keywords, words: temp.words.v5 });
    temp.heads = step22.heads;
    keys.diacritics = Object.keys(temp.diacritics);
    temp.chart = {
        tool: "tool-keywords-planner",
        data: {
            Keyword: "Home",
            id: 0,
            root: true,
            expand: true,
            children: step22.items,
        },
    };

    let n = 0;
    for (let Keyword of keys.keywords) {
        let kw = temp.keywords[Keyword];
        let x = kw.items.length - 1;
        if (list.remove.includes(Keyword) || x <= 0) continue;
        let obj = { Keyword, dup: x, type: "duplicate-in-files" };
        list.remove.push(Keyword);
        temp.details.push(obj);
    };
    for (let Keyword of updated.remove) {
        if (list.remove.includes(Keyword)) continue;
        let obj = { Keyword, dup: 1, type: "bad-words" };
        list.remove.push(Keyword);
        temp.details.push(obj);
    };
    for (let Keyword of updated.ignore) {
        let isExist = list.remove.includes(Keyword);
        let isInListKW = keys.keywords.includes(Keyword);
        let isInListV5 = step22.keywords.includes(Keyword);
        if (isExist || isInListV5 || !isInListKW) continue;
        let kw = temp.keywords[Keyword];
        let Volume = kw.Volume || 0;
        let KD = kw.KD || 0;
        let obj = { Keyword, dup: 1, type: "rare", Volume, KD };
        list.remove.push(Keyword);
        temp.rare.push(obj);
    };
    temp.rare = arraySort(temp.rare, ["Volume", "KD"], { reverse: true });
    temp.details = [...temp.details, ...temp.rare];
    for (let keyFrom of keys.merge) {
        let keyTo = temp.merge[keyFrom];// bỏ keyFrom - dùng keyTo
        let isExist = list.remove.includes(keyFrom);
        let isInListKW = keys.keywords.includes(keyFrom);
        let isInListV5 = step22.keywords.includes(keyFrom);
        if (isExist || isInListV5 || !isInListKW) continue;
        let obj = { Keyword: keyFrom, dup: 1, type: "replace", name: keyTo };
        list.remove.push(keyFrom);
        temp.details.push(obj);
    };
    for (let keyFrom of keys.diacritics) {
        let keyTo = temp.diacritics[keyFrom];// bỏ keyFrom - dùng keyTo
        let isExist = list.remove.includes(keyFrom);
        let isInList = keys.keywords.includes(keyFrom);
        if (isExist || !isInList) continue;
        let kw = temp.keywords[keyTo];
        if (!kw) helpers.file.log("diacritics.wow", keyTo);
        let obj = { Keyword: keyFrom, dup: 1, type: "diacritics", name: keyTo };
        list.remove.push(keyFrom);
        temp.details.push(obj);
    };
    for (let Keyword of keys.keywords) {
        let isRemove = list.remove.includes(Keyword);
        let isExist = step22.keywords.includes(Keyword);
        if (isRemove || isExist) continue;
        helpers.file.log("whyyyyyyyyyyyyyyyyyyyyyyyyyyyy", Keyword);
    };
    for (let item of temp.details) n += item.dup;
    helpers.file.log("ahrefs.keywords.details", n);

    /*********|write file|*********/
    const dfe = df.excel;
    let fsConfig = { encoding: "utf-8" };
    fs.ensureDirSync(`./data/export/${x}`);
    let fileWrite = {
        keywords: `./data/export/${x}/${x}-ahrefs.keywords.json`,
        outline: `./data/export/${x}/${x}-ahrefs.outline.json`,
        v1: `./data/export/${x}/${x}-ahrefs.v1.json`,
        v2: `./data/export/${x}/${x}-ahrefs.v2.json`,
        v3: `./data/export/${x}/${x}-ahrefs.v3.json`,
        v4: `./data/export/${x}/${x}-ahrefs.v4.json`,
        v5: `./data/export/${x}/${x}-ahrefs.v5.json`,
        diacritics: `./data/export/${x}/${x}-ahrefs.diacritics.json`,
        details: `./data/export/${x}/${x}-ahrefs.details.json`,
        merge: `./data/export/${x}/${x}-ahrefs.merge.json`,
        chart: `./data/export/${x}/${x}-chart.json`,
    };
    fs.writeJSONSync(fileWrite.keywords, temp.keywords, fsConfig);
    fs.writeJSONSync(fileWrite.outline, temp.heads, fsConfig);
    fs.writeJSONSync(fileWrite.v1, temp.words.v1, fsConfig);
    fs.writeJSONSync(fileWrite.v2, temp.words.v2, fsConfig);
    fs.writeJSONSync(fileWrite.v3, temp.words.v3, fsConfig);
    fs.writeJSONSync(fileWrite.v4, temp.words.v4, fsConfig);
    fs.writeJSONSync(fileWrite.v5, temp.words.v5, fsConfig);
    fs.writeJSONSync(fileWrite.diacritics, temp.diacritics, fsConfig);
    fs.writeJSONSync(fileWrite.details, temp.details, fsConfig);
    fs.writeJSONSync(fileWrite.merge, temp.merge, fsConfig);
    fs.writeJSONSync(fileWrite.chart, temp.chart, fsConfig);


    /*********|send email|*********/
    let obj = slitChildren({ item: temp.chart.data, key: "Keyword" });
    let sheet01 = {
        sheetName: "keywords.outline",
        columns: [
            { header: "STT", key: "STT", style: numberStyle },
        ],
        config: { worksheet: dfe.worksheet },
        rows: obj.data,
    };
    helpers.file.log("sheet01", sheet01.rows.length);
    helpers.file.log("maxLevel", obj.maxLevel);
    for (let i = 1; i <= obj.maxLevel; i++) {
        sheet01.columns.push({
            header: `Level${i}`,
            key: `h${i}`,
            width: 25,
        });
    };
    sheet01.columns.push({ header: "Volume Avg", key: "Volume", style: numberStyle });
    sheet01.columns.push({ header: "Volume Main", key: "VolumeMain", style: numberStyle });
    sheet01.columns.push({ header: "Difficulty", key: "KD", style: numberStyle });
    sheet01.columns.push({ header: "Từ khóa liên quan", key: "related", style: numberStyle });

    let filter = {
        htmlText: {
            file: "./data/templates/email.ahrefs.keywords.export.html",
            data: { member, keywords: list.input },
        },
        email: {
            Profile: df.email.profile,
            MailFromAlias: "Bot Isaura - Ahrefs",
            MailCC: df.email.cc,
            MailSubject: "Kết quả gom nhóm từ khóa",
            MailTo: member.Email,
            MailBody: null,
            AttachFiles: [
                path.resolve(fileWrite.keywords),
                path.resolve(fileWrite.v5),
            ],
        },
        outline: {
            fileName: "keywords.outline",
            x,
            data: [sheet01],
        },
        socket: {
            SessionID: api.net.config.SessionID,
            key: "res.ahrefs.keywords.export",
            to: [member.SocketID],
            data: {
                from: { id: member.SocketID },
                payload: { is_success: true, data: { id: x } },
            },
        },
    };
    helpers.excel.export(filter.outline);
    if (isTest) return { is_success: true, data: "Ahazon seo" };
    for (let keyFile of Object.keys(fileWrite)) {
        let filePath = fileWrite[keyFile];
        let fileFrom = path.resolve(filePath);
        let fileTo = `${df.copy.dirTo}/${x}/${path.basename(filePath)}`;
        fs.copy(fileFrom, fileTo);
    };
    let objFileReplace = helpers.file.replace(filter.htmlText);
    if (Array.isArray(objFileReplace.errors) && objFileReplace.errors.length) {
        helpers.file.log("ahrefs.email.replace.error", objFileReplace.errors);
        return { errors: objFileReplace.errors };
    };
    filter.email.MailBody = objFileReplace.text;
    fs.writeFile("./data/output/email.ahrefs.keywords.export.html", objFileReplace.text, fsConfig);
    const res_email = await api.net.email.sender.send(filter.email);
    if (!res_email.isSuccess) helpers.file.log("ahrefs.keywords.send.mail.error", res_email);
    api.webhook.socket.emit(filter.socket);
    helpers.file.log("ahrefs.keywords.group.stop", moment().format(cafe.format.dateTime));
    let res = { is_success: true, data: filter.socket.data };

    return res;
};

const handleAnalyzeVolume = ({ keywords, words }) => {
    let data = { words: {} };
    if (!keywords || !words) return data;
    let keys = Object.keys(words);
    let list = { words: [] };
    for (let key of keys) {
        let item = words[key];
        let itemSum = { Volume: 0, KD: 0, count: 0 };
        let itemAvg = { Volume: 0, KD: 0 };
        let itemRelated = item.related || [];
        let itemChildren = item.children || [];
        itemSum.Volume += item.Volume || 0;
        itemSum.KD += item.KD || 0;
        itemSum.count += 1;
        for (let [r, rlt] of itemRelated.entries()) if (!keywords[rlt]) itemRelated.splice(r, 1);
        for (let child01 of itemChildren) {
            let child01Sum = { Volume: 0, KD: 0, count: 0 };
            let child01Avg = { Volume: 0, KD: 0 };
            child01Sum.Volume += child01.Volume || 0;
            child01Sum.KD += child01.KD || 0;
            child01Sum.count += 1;

            let child01Related = child01.related || [];
            let child01Children = child01.children || [];
            itemSum.Volume += child01.Volume || 0;
            itemSum.KD += child01.KD || 0;
            itemSum.count += 1;

            for (let [r, rlt] of child01Related.entries()) if (!keywords[rlt]) child01Related.splice(r, 1);
            for (let child02 of child01Children) {
                let child02Sum = { Volume: 0, KD: 0, count: 0 };
                let child02Avg = { Volume: 0, KD: 0 };
                child02Sum.Volume += child02.Volume || 0;
                child02Sum.KD += child02.KD || 0;
                child02Sum.count += 1;

                child01Sum.Volume += child02.Volume || 0;
                child01Sum.KD += child02.KD || 0;
                child01Sum.count += 1;

                let child02Related = child02.related || [];
                let child02Children = child02.children || [];
                itemSum.Volume += child02.Volume || 0;
                itemSum.KD += child02.KD || 0;
                itemSum.count += 1;

                for (let [r, rlt] of child02Related.entries()) if (!keywords[rlt]) child02Related.splice(r, 1);
                for (let child03 of child02Children) {
                    let child03Sum = { Volume: 0, KD: 0, count: 0 };
                    let child03Avg = { Volume: 0, KD: 0 };
                    child03Sum.Volume += child03.Volume || 0;
                    child03Sum.KD += child03.KD || 0;
                    child03Sum.count += 1;

                    child02Sum.Volume += child03.Volume || 0;
                    child02Sum.KD += child03.KD || 0;
                    child02Sum.count += 1;

                    let child03Related = child03.related || [];
                    itemSum.Volume += child03.Volume || 0;
                    itemSum.KD += child03.KD || 0;
                    itemSum.count += 1;
                    for (let [r, rlt] of child03Related.entries()) if (!keywords[rlt]) child03Related.splice(r, 1);

                    child03Avg.Volume = parseFloat((child03Sum.Volume / child03Sum.count).toFixed(2));
                    child03Avg.KD = parseFloat((child03Sum.KD / child03Sum.count).toFixed(2));
                    let child03From = child03;
                    let child03To = { sum: child03Sum, avg: child03Avg };
                    child03 = Object.assign(child03From, child03To);
                    if (Array.isArray(child03.children)) child03.children = arraySort(child03.children, ["avg.Volume", "avg.KD"], { reverse: true });
                };

                child02Avg.Volume = parseFloat((child02Sum.Volume / child02Sum.count).toFixed(2));
                child02Avg.KD = parseFloat((child02Sum.KD / child02Sum.count).toFixed(2));
                let child02From = child02;
                let child02To = { sum: child02Sum, avg: child02Avg };
                child02 = Object.assign(child02From, child02To);
                if (Array.isArray(child02.children)) child02.children = arraySort(child02.children, ["avg.Volume", "avg.KD"], { reverse: true });
            };
            child01Avg.Volume = parseFloat((child01Sum.Volume / child01Sum.count).toFixed(2));
            child01Avg.KD = parseFloat((child01Sum.KD / child01Sum.count).toFixed(2));
            let child01From = child01;
            let child01To = { sum: child01Sum, avg: child01Avg };
            child01 = Object.assign(child01From, child01To);
            if (Array.isArray(child01.children)) child01.children = arraySort(child01.children, ["avg.Volume", "avg.KD"], { reverse: true });
        };
        itemAvg.Volume = parseFloat((itemSum.Volume / itemSum.count).toFixed(2));
        itemAvg.KD = parseFloat((itemSum.KD / itemSum.count).toFixed(2));
        let itemFrom = item;
        let itemTo = { Keyword: key, sum: itemSum, avg: itemAvg };
        list.words.push(Object.assign(itemFrom, itemTo));
        if (Array.isArray(item.children)) item.children = arraySort(item.children, ["avg.Volume", "avg.KD"], { reverse: true });
    };
    arraySort(list.words, ["avg.Volume", "avg.KD"], { reverse: true });
    for (let item of list.words) {
        const { Keyword } = item;
        data.words[Keyword] = item;
    };

    return data;
};

const handleRareKeywords = ({ keywords, words }) => {
    let data = { words: {}, related: {}, rare: [] };
    if (!keywords || !words) return data;
    let keys = Object.keys(words);
    for (let key of keys) {
        let word = words[key];
        let { related } = word;
        let k = related.indexOf(key);
        if (k >= 0) related.splice(k, 1);
        let check01 = JSON.stringify([key]);
        let check02 = JSON.stringify(related);
        if (check01 === check02 || related.length <= 1) {
            data.rare.push(key);
            // delete words[key];
            continue;
        };
        related.sort();
        let kw = keywords[key] || {};
        let { Volume, KD } = kw;
        data.words[key] = { Volume, KD, related };
        let arr = data.related[check02] || [];// lưu để xử lý từ đồng nghĩa
        if (!arr.includes(key)) arr.push(key);
        data.related[check02] = arr;
    };

    return data;
};

const handleRemoveSynonym = ({ words, tRelated }) => {
    let data = { words: {}, merge: {}, dup: [] };
    if (!words || !tRelated) return data;
    let keys = Object.keys(tRelated);
    for (let rlt of keys) {
        let arr = tRelated[rlt];
        if (arr.length < 2) continue;// không trùng thì bỏ qua
        arr.sort((a, b) => b.length - a.length);
        let key = arr.shift();// lấy key có length dài nhất
        let { Volume, KD, related } = words[key];
        for (let keyDup of arr) {
            let objDup = words[keyDup];
            if (!related.includes(keyDup)) related.push(keyDup);
            for (let rltDup of objDup.related) {
                if (!related.includes(rltDup) && rltDup !== key) related.push(rltDup);
            };
            data.merge[keyDup] = key;
            data.dup.push(keyDup);
        };
        let k = related.indexOf(key);
        if (k >= 0) related.splice(k, 1);
        related = related.sort();
        data.words[key] = { Volume, KD, related };
    };

    return data;
};

const handleMaxDiacritics = ({ diacritics }) => {
    let data = { diacritics: {} };
    if (!diacritics) return data;
    let keys = Object.keys(diacritics);
    for (let key of keys) {
        let rd = diacritics[key];
        let max = { sum: 0, diacritic: null };
        let keysDiacritics = Object.keys(rd);
        for (let diacritic of keysDiacritics) {
            if (rd[diacritic] > max.sum) max = { sum: rd[diacritic], diacritic };
        };
        if (!max.diacritic) continue;
        // updated.duplicate.diacritic.push(key);
        data.diacritics[key] = max.diacritic;
    };

    return data;
};

const handleSimilarWords = ({ keywords, words, merge, isAssign = false, isIgnoreSubkey = true }) => {
    // isIgnoreSubkey là bỏ qua các trường hợp:
    //      + từ khóa con không thuộc tệp ban đầu
    //      + từ mang 1 nghĩa thì khỏi phân nhóm
    helpers.file.log("handle.similar.words", `ignore.subjey=${isIgnoreSubkey}`);
    let data = {
        words: {},
        merge: Object.assign({}, merge),
        v1: {
            related: [],
            children: [],
        },
        v2: {
            merge: {},
            group: {},
        },
    };
    if (isAssign) data.words = Object.assign({}, words);
    if (!keywords || !words) return data;

    /*********|01. Lọc từ gần nghĩa|*********/
    let keys = {
        words: Object.keys(words).sort((a, b) => a.length - b.length),
        temp: {
            group: [],
        },
        v1: {
            related: [],
            children: [],
        },
        v2: {
            merge: [],
            group: [],
        },
    };
    for (let i = 0; i < keys.words.length - 1; i++) {
        let iKey = keys.words[i];
        let iObj = words[iKey];
        let iRelated = iObj.related;

        for (let j = i + 1; j < keys.words.length; j++) {
            let jKey = keys.words[j];
            let jObj = words[jKey];
            let jRelated = jObj.related;
            let ijRelated = [];
            let compareFrom = [];
            let compareTo = [];
            let key = null
            let subkey = null;
            let obj = null;
            let iCheck = JSON.stringify(iRelated);
            let jCheck = JSON.stringify(jRelated);

            // trường hợp này đồng nghĩa 100%
            if (iCheck === jCheck) {
                key = iKey;
                subkey = jKey;
                if (keywords[subkey]) {
                    obj = data.v1.related[key] || {};
                    obj[subkey] = {};
                    data.v1.related[key] = obj;
                };
                continue;
            };
            if (iRelated.length > jRelated.length || !isIgnoreSubkey) {
                // chữ ngắn mang nhiều nghĩa hơn chữ dài - lấy chữ ngắn làm gốc so với chữ dài
                key = iKey;
                subkey = jKey;
                compareFrom = iRelated;
                compareTo = jRelated;
            } else {
                // chữ dài mang nhiều nghĩa hơn chữ ngắn - lấy chữ dài làm gốc so với chữ ngắn
                key = jKey;
                subkey = iKey;
                compareFrom = jRelated;
                compareTo = iRelated;
            };
            if (isIgnoreSubkey) {
                if (!keywords[subkey]) continue;// từ khóa con không thuộc tệp ban đầu
                if (compareFrom.length <= 1) continue;// từ mang 1 nghĩa thì khỏi phân nhóm
            };
            let expectedRelatedLength = ~~compareFrom.length * df.duplicate.percent;
            for (let rlt of compareFrom) if (compareTo.includes(rlt)) ijRelated.push(rlt);
            let change = compareFrom.length - ijRelated.length;
            let isInExpectedPercent = ijRelated.length >= expectedRelatedLength;
            let isInExpectedChange = change <= df.duplicate.change;
            let isChild = ijRelated.length === compareTo.length;
            let isLongKeyword = subkey.split(" ").length > 1;
            if (!ijRelated.length) continue;
            if (ijRelated.length <= 1 && compareTo.length > 1 && isLongKeyword && isIgnoreSubkey) continue;// chúng ta không thuộc về nhau
            if (key === subkey) continue;// parent và child là 1 thì bỏ qua
            if (!isInExpectedPercent && !isInExpectedChange) {
                let isAlreadyInRelated = compareFrom.includes(subkey);
                // !isIgnoreSubkey = isCare
                if (isChild && (!isIgnoreSubkey || !isAlreadyInRelated)) {
                    obj = data.v1.children[key] || {};
                    obj[subkey] = {};
                    data.v1.children[key] = obj;
                };
                continue;
            };
            obj = data.v1.related[key] || {};
            obj[subkey] = {};
            data.v1.related[key] = obj;
        };
    };


    /*********|02. Kiểm tra cặp 1-1 hay 1-n|*********/
    keys.v1.related = Object.keys(data.v1.related).sort().sort((a, b) => b.length - a.length);
    keys.v1.children = Object.keys(data.v1.children);
    for (let key of keys.v1.children) {
        let item = data.v1.children[key];
        let keysChild = Object.keys(item);
        if (keysChild.length === 0) {
            delete data.v1.children[key];
            continue;
        };
        if (keysChild.length === 1) {
            data.v2.merge[key] = item;
            continue;
        };
        data.v2.group[key] = item;
    };
    for (let key of keys.v1.related) {
        let item = data.v1.related[key];
        let keysChild = Object.keys(item);
        if (keysChild.length === 0) {
            delete data.v1.related[key];
            continue;
        };
        if (keysChild.length === 1) {
            data.v2.merge[key] = item;
            continue;
        };
        data.v2.group[key] = item;
    };
    keys.v2.merge = Object.keys(data.v2.merge).sort().sort((a, b) => b.length - a.length);


    /*********|03. Cập nhật data 1-1|*********/
    let temp = { group: {} };
    for (let keyMerge of keys.v2.merge) {
        let key = {
            merge: keyMerge,
            updated: null,
            delete: null,
        };
        let a = data.v1.related[key.merge];
        let b = data.v1.children[key.merge];
        let obj = {
            merge: a || b,
        };
        if (!obj.merge) continue;
        if (Object.keys(obj.merge).length > 1) continue;
        let find = Object.keys(obj.merge).shift();
        let iCheck01 = keywords[key.merge];
        let iCheck02 = keywords[find];

        delete data.v1.related[keyMerge];
        if (!iCheck01) {
            // sau khi gom nhóm mà parent ko có Volume thì lấy Volume của child
            key.updated = find;
            key.delete = key.merge;
        } else {
            // sau khi gom nhóm mà parent và child đều có key thì ưu tiên Volume
            let Volume01 = iCheck01.Volume || 0;
            let Volume02 = iCheck02.Volume || 0;
            if (Volume01 < Volume02) {
                key.updated = find;
                key.delete = key.merge;
            } else {
                key.updated = key.merge;
                key.delete = find;
            };
        };
        data.merge[key.delete] = key.updated;// bên trái là remove - bên phải là replace
        let tempGroup = temp.group[key.updated];
        if (!tempGroup) tempGroup = { children: [] };
        if (!tempGroup.children.includes(key.delete)) tempGroup.children.push(key.delete);
        temp.group[key.updated] = tempGroup;
        let related = Object.assign([], words[key.updated].related);
        if (!related.includes(key.delete)) related.push(key.delete);
        for (let rlt of words[key.delete].related) if (!related.includes(rlt) && rlt !== key.updated) related.push(rlt);
        let kum = data.merge[key.updated];
        if (kum) key.updated = kum;
        let k = related.indexOf(key.updated);
        if (k >= 0) related.splice(k, 1);
        let item = words[key.updated];
        if (!item) {
            helpers.file.log("error.words", `${key.merge} / ${key.updated}`);
            continue;
        };
        let { Volume, KD } = item;
        data.words[key.updated] = { Keyword: key.updated, Volume, KD, related };
        if (isAssign) delete data.words[key.delete];
    };


    /*********|04. Cập nhật từ khóa merge 1-n |*********/
    keys.temp.group = Object.keys(temp.group);
    for (let keyUpdate of keys.temp.group) {
        let tempGroup = temp.group[keyUpdate];
        let arr = [];
        for (let keyDelete of tempGroup.children) {
            let kw = keywords[keyDelete];
            if (!kw && isIgnoreSubkey) continue;
            arr.push(keyDelete)
        };
        if (arr.length <= 1 && isIgnoreSubkey) continue;
        let v2Group = data.v2.group[keyUpdate];
        if (!v2Group) v2Group = {};
        for (let keyDelete of arr) v2Group[keyDelete] = {};
        data.v2.group[keyUpdate] = v2Group;
    };
    for (let keyV2Group of Object.keys(data.v2.group)) {
        let iV2Group = data.v2.group[keyV2Group];
        let keysiV2Group = Object.keys(iV2Group);
        for (let keyChild of keysiV2Group) {
            let v2GroupDelete = data.v2.group[keyChild];
            if (v2GroupDelete) helpers.file.log("v2GroupDelete.02", JSON.stringify({ [keyChild]: v2GroupDelete }));
        };
    };

    /*********|05. Xử lý trùng giữa các group |*********/
    keys.temp.group = [];
    keys.v2.group = Object.keys(data.v2.group).sort((a, b) => a.length - b.length);
    for (let keyGroup of keys.v2.group) {
        let item = data.v2.group[keyGroup];
        let keysChild = Object.keys(item);
        for (let keyChild of keysChild) {
            let child = data.v2.group[keyChild];
            if (!child) continue;
            let keysGrandChild = Object.keys(child);
            for (let keyGrandChild of keysGrandChild) {
                if (!keysChild.includes(keyGrandChild)) continue;
                delete child[keyGrandChild];
            };
        };
    };
    for (let keyV2Group of Object.keys(data.v2.group)) {
        let iV2Group = data.v2.group[keyV2Group];
        let keysiV2Group = Object.keys(iV2Group);
        for (let keyChild of keysiV2Group) {
            let v2GroupDelete = data.v2.group[keyChild];
            if (v2GroupDelete) helpers.file.log("v2GroupDelete.03", JSON.stringify({ [keyChild]: v2GroupDelete }));
        };
    };
    keys.v2.group = Object.keys(data.v2.group).sort((a, b) => a.length - b.length);
    for (let keyGroup of keys.v2.group) {
        let item = data.v2.group[keyGroup];
        let keysChild = Object.keys(item);
        if (!keysChild.length) delete data.v2.group[keyGroup];
    };
    for (let keyV2Group of Object.keys(data.v2.group)) {
        let iV2Group = data.v2.group[keyV2Group];
        let keysiV2Group = Object.keys(iV2Group);
        for (let keyChild of keysiV2Group) {
            let v2GroupDelete = data.v2.group[keyChild];
            if (v2GroupDelete) helpers.file.log("v2GroupDelete.04", JSON.stringify({ [keyChild]: v2GroupDelete }));
        };
    };

    return data;
};

const handleSimilarWords02 = ({ keywords, merge, diacritics, words, group, v2, ignore }) => {
    let data = {
        words: Object.assign({}, words),// over here
        merge: Object.assign({}, merge),
        diacritics: Object.assign({}, diacritics),
        v4: {},
        v1: {
            related: {},
            children: {},
        },
        v2: {
            merge: {},
            group: {},
        },
        dup: {
            diacritics: [],
            meaning: [],
        },
        save: {
            diacritics: [],
        },
        children: [],
        keywords: [],
        branch: [],
    };
    if (!keywords || !merge || !words || !group) return data;
    let keys = {
        words: [],
        group: Object.keys(group).sort((a, b) => a.length - b.length),
        v1: {
            related: [],
            children: [],
        },
        v2: {
            merge: [],
            group: [],
        },
        v4: {},
        data: {
            words: [],
            merge: [],
        },
    };


    /*********|01. Lọc từ gần nghĩa|*********/
    let sml = handleSimilarWords({ keywords, words, merge, isAssign: true });
    data.v1 = sml.v1;
    data.v2 = sml.v2;
    data.words = sml.words;
    data.merge = sml.merge;
    keys.v2.merge = Object.keys(data.v2.merge).sort().sort((a, b) => b.length - a.length);
    keys.v2.group = Object.keys(data.v2.group).sort().sort((a, b) => b.length - a.length);


    /*********|04. Cập nhật children|*********/
    for (let keyGroup of keys.group) {
        let item = group[keyGroup];
        let keysGroup = Object.keys(item);
        let obj = Object.assign({}, data.words[keyGroup] || v2[keyGroup]);
        if (!obj) helpers.file.log("obj", keyGroup);
        let children = obj.children || [];
        for (let keyChild of keysGroup) {
            let child = data.words[keyChild] || v2[keyChild];
            if (!child) continue;
            child = Object.assign({ Keyword: keyChild }, child);
            data.children.push(keyChild);
            children.push(child);
            delete data.words[keyChild];
        };
        if (children.length === 1) helpers.file.log("children.01.only", keyGroup);
        obj.children = arraySort(children, ["Volume"], { reverse: true });
        data.words[keyGroup] = obj;
    };


    /*********|05. Cập nhật v4 data|*********/
    ignore = [...ignore, ...data.children];
    keys.words = Object.keys(data.words).sort().sort((a, b) => b.length - a.length);
    let keysV2 = Object.keys(v2).sort((a, b) => a.length - b.length);
    for (let key of keys.words) {
        let word = data.words[key];
        data.v4[key] = word;
        if (!data.keywords.includes(key)) data.keywords.push(key);
        for (let rlt of word.related) {
            if (!data.keywords.includes(rlt)) data.keywords.push(rlt);
        };
        let children = word.children || [];
        for (let child of children) {
            for (let rltChild of child.related) {
                if (!data.keywords.includes(rltChild)) data.keywords.push(rltChild);
            };
        };
    };
    for (let keyV2 of keysV2) {
        if (ignore.includes(keyV2)) continue;
        if (data.keywords.includes(keyV2)) continue;
        let wordV2 = v2[keyV2];
        let isInList = false;
        let key = {
            main: keyV2,
            diacritic: null,
            merge: null,
            rd: helpers.text.remove.diacritics(keyV2),
            updated: data.diacritics[keyV2] || keyV2,
        };
        if (key.main === key.rd) {
            // trường hợp này là từ khóa remove unicode
            key.diacritic = diacritics[key.main] || key.main;
            isInList = data.keywords.includes(key.diacritic);
            if (isInList) continue;
            key.main = key.diacritic;
        };
        key.merge = data.merge[key.main] || key.main;
        if (key.merge !== key.main) {
            // trường hợp này cần lấy theo key đã gom nhóm
            isInList = data.keywords.includes(key.merge);
            if (isInList) continue;
            key.main = key.merge;
        };
        if (!keywords[keyV2]) continue;
        let { Volume, KD, related } = wordV2;
        // helpers.file.log(`${Volume}\t\t${keyV2}\t\t${related.length}`);
        data.v4[key.updated] = { Volume, KD, related };
    };


    /*********|06. Cập nhật v4 diacritic|*********/
    keys.v4 = Object.keys(data.v4).sort().sort((a, b) => b.length - a.length);
    for (let keyV4 of keys.v4) {
        let key = {
            main: keyV4,
            rd: helpers.text.remove.diacritics(keyV4),
            diacritic: null,
        };
        let obj = {
            main: data.v4[key.main],
        };
        if (key.main !== key.rd) continue;
        // trường hợp này từ khóa có unicode
        key.diacritic = data.diacritics[key.rd];
        if (key.diacritic) {
            data.dup.diacritics.push(key.main);
            delete data.v4[key.main];
            data.v4[key.diacritic] = Object.assign({}, obj.main);
        } else {
            data.branch.push(key.main);
        };
    };

    return data;
};

const handleSimilarWords03 = ({ keywords, merge, diacritics, words, branch, config }) => {
    let data = {
        words: {},
        merge: Object.assign({}, merge),
        diacritics: Object.assign({}, diacritics),
        v1: {
            related: {},
            children: {},
        },
        v2: {
            merge: {},
            group: {},
        },
        dup: {
            diacritics: [],
            meaning: [],
        },
        save: {
            diacritics: [],
        },
        children: [],
    };
    if (!keywords || !merge || !words) return data;
    let keys = {
        words: [],
        v1: {
            related: [],
            children: [],
        },
        v2: {
            merge: [],
            group: [],
        },
        data: {
            words: [],
            merge: [],
            categories: [],
        },
        temp: {
            words: [],
            finish: {
                similar: [],
            },
        },
    };
    let temp = {
        words,
        children: [],
    };


    /*********|01. Lọc từ gần nghĩa|*********/
    let sml = handleSimilarWords({ keywords, words, merge, isAssign: true });
    data.v1 = sml.v1;
    data.v2 = sml.v2;
    temp.words = sml.words;
    data.merge = sml.merge;
    keys.v2.merge = Object.keys(data.v2.merge).sort().sort((a, b) => b.length - a.length);
    keys.v2.group = Object.keys(data.v2.group).sort().sort((a, b) => b.length - a.length);


    /*********|02. Xử lý cặp key 1-1 data.v2.group|*********/
    for (let keyGroup of keys.v2.group) {
        let key = {
            main: keyGroup,
            child: null,
            updated: null,
            delete: null,
        };
        let obj = {
            main: data.v2.group[key.main],
            check: temp.words[key.main],
            child: null,
            updated: null,
            delete: null,
        };
        let keys42 = Object.keys(obj.main);
        if (!keys42.length) {
            delete data.v2.group[keyGroup];
            continue;
        };
        if (keys42.length === 1) {
            key.child = keys42.shift();
            obj.child = temp.words[key.child];
            // delete data.v2.group[key.v42];
            if (!obj.check && !obj.child) {
                helpers.file.log("error.obj", `${key.main}/${key.child}`);
                continue;
            };
            if (!obj.check) {
                helpers.file.log("!obj.check", key.main);
                data.dup.meaning.push(key.main);
                continue;
            };
            if (!obj.child) {
                helpers.file.log("!obj.child", key.child);
                data.dup.meaning.push(key.child);
                continue;
            };
            // tời đây thì yêu cầu đã có from và to
            // đối với cặp 1-1 thì 1 trong 2 là parent hoặc child có ở words.v4 là được; thằng còn lại sẽ vào duplicate
            if (obj.child.Volume > obj.check.Volume) {
                key.updated = key.child;
                key.delete = key.main;
            } else {
                key.updated = key.main;
                key.delete = key.child;
            };
            obj.updated = temp.words[key.updated];
            obj.delete = temp.words[key.delete];
            let related = Object.assign([], obj.updated.related);
            if (!related.includes(key.delete)) related.push(key.delete);
            for (let rlt of obj.delete.related) if (!related.includes(rlt) && rlt !== key.updated) related.push(rlt);
            key.updated = data.diacritics[key.updated] || key.updated;
            let k = related.indexOf(key.updated);
            if (k >= 0) related.splice(k, 1);// trường hợp này vẫn còn xót(vd từ "đạp điện" khi move qua "xe đạp điện" thì related của "đạp điện" có chứa "xe đạp điện")
            let { Volume, KD } = obj.updated;
            temp.words[key.updated] = { Volume, KD, related };
            data.dup.meaning.push(key.delete);
            delete temp.words[key.delete];
            // delete data.merge[key.updated];
            data.merge[key.delete] = key.updated;
            continue;
        };
        let children = obj.check.children || [];
        for (let keyChild of keys42) {
            let child = temp.words[keyChild];
            if (!child) continue;
            child = Object.assign({ Keyword: keyChild }, child);
            data.children.push(keyChild);
            children.push(child);
            delete temp.words[keyChild];// over here
        };
        obj.check.children = arraySort(children, ["Volume"], { reverse: true });
        temp.words[key.main] = obj.check;
    };


    /*********|03. Xử lý config từ client|*********/
    let finish = config.finish || { remove: [], similar: {} };
    let categories = config.categories || {};
    let group = {};
    let branchKey = "thương hiệu";
    keys.data.categories = Object.keys(categories);
    keys.temp.finish.similar = Object.keys(finish.similar);


    /*********|03.01 xử lý từ khóa tương tự|*********/
    for (let keySimilar of keys.temp.finish.similar) {
        let key = {
            from: keySimilar,
            to: finish.similar[keySimilar],
            children: [],
        };
        let obj = {
            key: null,
            from: temp.words[key.from],
            to: temp.words[key.to],
            updated: null,
            children: [],
        };
        if (!obj.from) continue;
        key.children.push(key.from);
        obj.children.push({
            Keyword: key.from,
            Volume: obj.from.Volume,
            KD: obj.from.KD,
            related: obj.from.related,
        });
        if (!obj.to) {
            let children = obj.from.children || [];
            for (let [c, child] of children.entries()) {
                if (child.Keyword === key.to) {
                    obj.to = child;
                    index = c;
                } else {
                    key.children.push(child.Keyword);
                    obj.children.push(child);
                };
            };
        };
        if (!obj.to) continue;
        let { related } = obj.to;
        let Volume = obj.to.Volume || obj.from.Volume;
        let KD = obj.to.KD || obj.from.KD;
        obj.to.children = obj.to.children || [];
        for (let child of obj.to.children) {
            if (!key.children.includes(child.Keyword)) obj.children.push(child);
        };
        for (let rlt of obj.from.related) if (!related.includes(rlt) && rlt !== key.from) related.push(rlt);
        obj.updated = {
            Volume, KD, related,
            children: obj.children,
        };
        temp.words[key.to] = obj.updated;
        delete temp.words[key.from];
    };


    /*********|03.02 cấu hình gom nhóm theo chuẩn từ client|*********/
    for (let key of keys.data.categories) {
        let arr = categories[key];
        let obj = temp.words[key];
        if (!obj) {
            let item = keywords[key] || {};
            let { Volume, KD, related } = item;
            obj = { Volume, KD, related };
        };
        obj.children = obj.children || [];
        temp.words[key] = obj;
        for (let Keyword of arr) {
            group[Keyword] = key;
            let isChild = !!categories[Keyword];
            if (isChild) temp.children.push(Keyword);
        };
    };
    keys.temp.words = Object.keys(temp.words).sort().sort((a, b) => b.length - a.length);


    /*********|03.03 xóa nhóm không liên quan - gom nhóm children - thương hiệu|*********/
    for (let key of keys.temp.words) {
        let obj = {
            key: { Keyword: key },
            main: temp.words[key],
            group: null,
            branch: temp.words[branchKey] || {},
        };
        // xóa các nhóm doanh nghiệp không qua tâm
        if (finish.remove.includes(key)) {
            helpers.file.log("finish.remove", key);
            delete temp.words[key];
            continue;
        };
        let check = {
            group: group[key],
        };
        // nhóm các group theo cấu hình từ client
        if (check.group) {
            obj.group = temp.words[check.group];
            if (!obj.group) {
                let item = keywords[check.group] || {};
                let { Volume, KD, related } = item;
                obj.group = { Volume, KD, related };
            };
            obj.group.children = obj.group.children || [];
            obj.group.children.push(Object.assign(obj.key, obj.main));
            temp.words[check.group] = obj.group;
            delete temp.words[key];
            continue;
        };
        // nhóm thương hiệu
        if (branch.includes(key)) {
            obj.branch.children = obj.branch.children || [];
            obj.branch.children.push(Object.assign(obj.key, obj.main));
            temp.words[branchKey] = obj.branch;
            delete temp.words[key];
            continue;
        };
    };


    /*********|03.04 xóa children ra khỏi nhóm lớn vì đã trong nhóm con|*********/
    for (let keyChild of temp.children) {
        let key = {
            main: group[keyChild],
            child: keyChild,
            children: [],
        };
        let obj = {
            main: temp.words[key.main],
            child: temp.words[key.child],
            updated: null,
            children: [],
        };
        if (!obj.child || !obj.main) continue;
        obj.main.children = obj.main.children || [];
        obj.child.children = obj.child.children || [];
        for (let child of obj.main.children) {
            if (child.Keyword === key.child) {
                child.children = child.children || [];
                child.children = Object.assign(child.children, obj.child.children);
            };
        };
        delete temp.words[key.child];
    };


    /*********|03.05 phân tích volume|*********/
    keys.temp.words = Object.keys(temp.words);
    for (let Keyword of keys.temp.words) {
        let item = temp.words[Keyword];
        let keysChild = [];
        let itemChildren = item.children || [];
        for (let child of itemChildren) keysChild.push(child.Keyword);
        let obj01 = keysChild.length ? { keysChild } : {};
        let obj02 = item;
        data.words[Keyword] = Object.assign(obj01, obj02);
    };

    return data;
};

const handleOutLine = ({ keywords, words }) => {
    let data = { heads: [], items: [], keywords: [] };
    if (!words || !keywords) return data;
    let keys = Object.keys(words);
    for (let keyMain of keys) {
        let item = words[keyMain];
        let itemSum = item.sum || {};
        let itemAvg = item.avg || {};
        let itemRelated = item.related || [];
        let itemChildren = item.children || [];
        let ih1 = {
            h1: keyMain,
            Keyword: keyMain,
            Volume: itemAvg.Volume,
            VolumeMain: item.Volume,
            KD: itemAvg.KD,
            count: itemSum.count,
            related: itemRelated.join(","),
        };
        data.heads.push(ih1);
        let nh1 = data.items.length;
        let idh1 = `${keyMain}-${nh1}`;
        let nameh1 = keyMain;
        let pidh1 = 0;
        let objh1 = createObject({ obj: { id: idh1, name: nameh1, pid: pidh1 }, h: ih1 });
        let h2 = [];
        let objh2, ih2, idh2, nameh2, nh2;
        if (!data.keywords.includes(keyMain)) data.keywords.push(keyMain);
        for (let rltItem of itemRelated) if (!data.keywords.includes(rltItem)) data.keywords.push(rltItem);
        for (let child01 of itemChildren) {
            let child01Sum = child01.sum || {};
            let child01Avg = child01.avg || {};
            let child01Related = child01.related || [];
            let child01Children = child01.children || [];
            // if (!child01Avg.Volume) continue;//edit here
            ih2 = {
                h2: child01.Keyword,
                Keyword: child01.Keyword,
                Volume: child01Avg.Volume,
                VolumeMain: child01.Volume,
                KD: child01Avg.KD,
                count: child01Sum.count,
                related: child01Related.join(","),
            };
            nh2 = objh1.children.length;
            idh2 = `${child01.Keyword}-${nh1}-${nh2}`;
            nameh2 = child01.Keyword;
            h2.push(ih2);
            objh2 = createObject({ obj: { id: idh2, name: nameh2, pid: idh1 }, h: ih2 });
            let h3 = [];
            let objh3, ih3, idh3, nameh3, nh3;
            if (!data.keywords.includes(child01.Keyword)) data.keywords.push(child01.Keyword);
            for (let rlt01 of child01Related) if (!data.keywords.includes(rlt01)) data.keywords.push(rlt01);
            for (let child02 of child01Children) {
                let child02Sum = child02.sum || {};
                let child02Avg = child02.avg || {};
                let child02Related = child02.related || [];
                ih3 = {
                    h3: child02.Keyword,
                    Keyword: child02.Keyword,
                    Volume: child02Avg.Volume,
                    VolumeMain: child02.Volume,
                    KD: child02Avg.KD,
                    count: child02Sum.count,
                    related: child02Related.join(","),
                };
                nh3 = objh2.children.length;
                idh3 = `${child02.Keyword}-${nh1}-${nh2}-${nh3}`;
                nameh3 = child02.Keyword;
                h3.push(ih3);
                objh3 = createObject({ obj: { id: idh3, name: nameh3, pid: idh2 }, h: ih3 });
                delete objh3.children;
                objh2.children.push(objh3);
                if (!data.keywords.includes(child02.Keyword)) data.keywords.push(child02.Keyword);
                for (let rlt02 of child02Related) if (!data.keywords.includes(rlt02)) data.keywords.push(rlt02);
            };
            if (h3.length <= 1) {
                for (let rlt01 of child01Related) {
                    let kw = keywords[rlt01];
                    if (!kw) continue;
                    ih3 = {
                        h3: rlt01,
                        Keyword: rlt01,
                        Volume: kw.Volume,
                        KD: kw.KD,
                        count: 1,
                    };
                    nh3 = objh2.children.length;
                    idh3 = `${rlt01}-${nh1}-${nh2}-${nh3}`;
                    nameh3 = rlt01;
                    h3.push(ih3);
                    objh3 = createObject({ obj: { id: idh3, name: nameh3, pid: idh2 }, h: ih3 });
                    delete objh3.children;
                    objh2.children.push(objh3);
                    if (!data.keywords.includes(rlt01)) data.keywords.push(rlt01);
                };
                arraySort(h3, ["Volume", "KD"], { reverse: true });
            };
            h2 = [...h2, ...h3];
            if (!objh2.children.length) delete objh2.children;
            objh1.children.push(objh2);
        };
        if (h2.length <= 1) {
            for (let rltItem of itemRelated) {
                let kw = keywords[rltItem];
                if (!kw || !kw.Volume) continue;
                ih2 = {
                    h2: rltItem,
                    Keyword: rltItem,
                    Volume: kw.Volume,
                    KD: kw.KD,
                    count: 1,
                };
                nh2 = objh1.children.length;
                idh2 = `${rltItem}-${nh1}-${nh2}`;
                nameh2 = rltItem;
                h2.push(ih2);
                objh2 = createObject({ obj: { id: idh2, name: nameh2, pid: idh1 }, h: ih2 });
                if (!objh2.children.length) delete objh2.children;
                objh1.children.push(objh2);
                if (!data.keywords.includes(rltItem)) data.keywords.push(rltItem);
            };
            if (!objh1.children.length) delete objh1.children;
            arraySort(h2, ["Volume", "KD"], { reverse: true });
        };
        data.heads = [...data.heads, ...h2];
        data.items.push(objh1);
    };

    return data;
};

const createObject = ({ obj, h }) => {
    const dfObj = { children: [] };
    return Object.assign(obj, dfObj, h);
};

const slitChildren = ({ data = [], level = 1, maxLevel = 1, item, key }) => {
    if (!item || !Array.isArray(item.children)) return data;
    for (let child of item.children) {
        let obj01 = { STT: data.length + 1, [`level${level}`]: child[key] };
        let obj02 = child;
        data.push(Object.assign(obj01, obj02));
        if (!Array.isArray(child.children) || !child.children.length) continue;
        let lv = level + 1;
        if (lv > maxLevel) maxLevel = lv;
        let obj = slitChildren({ data, item: child, key, level: lv, maxLevel });
        data = obj.data;
        maxLevel = obj.maxLevel;
    }
    return { data, level, maxLevel };
};

module.exports = {
    groupKeywords,
};
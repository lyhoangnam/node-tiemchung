const fs = require("fs-extra");
const arraySort = require("array-sort");
const path = require("path");
const moment = require("moment");
const { api, helpers } = require("../../utils");
const { stored } = api.net.config;
const cafe = require("./config");
const ah = require("./ahrefs.helpers");
const { numberStyle } = helpers.excel;
const { escape } = helpers.text;
const hfile = helpers.file;
const df = cafe.default;
const isTest = 1;

const groupKeywords = async ({ member, keywords, data, config }) => {
    if (!config || isNaN(config.x)) return { error: cafe.sms.invalid_inputs };
    config.branchGroup = config.branchGroup || df.branchGroup;
    config.ignore = config.ignore || { list: [], reg: [] };
    config.ignore.list = config.ignore.list || [];
    config.ignore.reg = config.ignore.reg || [];
    config.duplicate = config.duplicate || df.duplicate;
    config.rehandle = config.rehandle || df.rehandle;
    hfile.log("ahrefs.keywords.group.start", moment().format(cafe.format.dateTime));


    /*********|first config|*********/
    hfile.log("config.ignore.reg.01", config.ignore.reg.length);
    const { x } = config;
    const res_kwi = await api.net.data.bot.load.list({ storeName: stored.link.ahrefs.GetKeywordsIgnore, filter: {} });
    let kwid = res_kwi.data || [];
    for (let item of kwid) config.ignore.reg.push(item.KeywordName);
    hfile.log("config.ignore.reg.02", config.ignore.reg.length);
    hfile.log("ahrefs.keywords.group.data.01", data.length);
    let list = {
        input: [],
        related: [],
        permute: [],
        remove: [],
    };
    let temp = {
        input: {},
        keywords: {},
        words: {
            v1: {},// tách từ sau đó gom nhóm cơ bản
            v2: {},// loại các từ chỉ xuất hiện 1 lần; lưu từ đồng nghĩa
            v3: {},// đã thực hiện việc kiểm tra từ gần nghĩa; một lần nữa loại các từ giống nhau chỉ lấy từ có Volume cao nhất
        },
        text: "",
    };
    let keys = {
        input: [],
        keywords: [],
        merge: [],
        words: {
            v1: [],
            v2: [],
            v3: [],
        },
        diacritics: [],
    };
    let updated = {
        rare: [],
        details: [],
        diacritics: {},
        chart: {},
        remove: [],
        merge: {},
        duplicate: {
            meaning: [],
            diacritics: [],
        },
    };
    for (let kw of keywords) {
        list.input.push(kw.text);
        if (!list.related.includes(kw.text)) list.related.push(kw.text);
        for (let kwrlt of kw.related) if (!list.related.includes(kwrlt)) list.related.push(kwrlt);
    };
    for (let Keyword of list.input) ah.handlePermuteWords({ words: temp.input, Keyword, ignore: config.ignore });
    keys.input = Object.keys(temp.input);
    for (let Keyword of keys.input) {
        let word = temp.input[Keyword];
        if (word.related.length < list.input.length) continue;
        config.ignore.list.push(Keyword);
    };
    hfile.log("config.ignore.list", JSON.stringify(config.ignore.list));
    for (let Keyword of list.related) {
        data.push({
            Keyword,
            Volume: 0,
            KD: 0,
            HostName: "google.com",
            Position: 11,
            Traffic: 0,
            Href: "google.com",
        });
    };
    hfile.log("ahrefs.keywords.group.data.02", data.length);


    /*********|Step 1.1: xử lý bad-words và permute words|*********/
    for (let item of data) {
        let Keyword = item.Keyword;
        let isIgnore = ah.checkIgnore({ Keyword, ignore: config.ignore });
        if (isIgnore) {
            if (!updated.remove.includes(Keyword)) updated.remove.push(Keyword);
            continue;
        };
        const { HostName, Position, Volume, KD, Traffic, Href } = item;
        let obj = { replace: null, permute: null };
        obj.replace = ah.handleReplaceText({ Keyword, replaceObj: config.txtReplace.text });
        Keyword = obj.replace.text;// sau khi replace text mới tách chữ
        for (let dup of obj.replace.dup) if (!updated.duplicate.meaning.includes(dup)) updated.duplicate.meaning.push(dup);
        obj.permute = ah.handlePermuteWords({ words: temp.words.v1, Keyword, ignore: config.ignore });
        temp.words.v1 = obj.permute.words;
        if (!list.permute.includes(Keyword)) list.permute.push(Keyword);
        for (let kwpm of obj.permute.list) if (!list.permute.includes(kwpm)) list.permute.push(kwpm);
        let word = temp.keywords[Keyword];
        if (!word) word = { Volume, KD, items: [] };
        word.items.push({ HostName, Position, Traffic, Href });
        temp.keywords[Keyword] = word;
    };
    keys.words.v1 = Object.keys(temp.words.v1);
    hfile.log("temp.words.v1", keys.words.v1.length);
    hfile.log("list.permute", list.permute.length);


    /*********|Step 1.2: diacritics tất cả permute words|*********/
    let step0102 = ah.handleRemoveDiacritics({ permute: list.permute, keywords: temp.keywords });
    updated.diacritics = step0102.v2;
    keys.diacritics = Object.keys(updated.diacritics);
    hfile.log("keys.diacritics", keys.diacritics.length);


    /*********|Step 2.1: loại từ khóa rare và gom nhóm từ đồng nghĩa|*********/
    let sml0201 = ah.handleRareKeywords({ keywords: temp.keywords, words: temp.words.v1 });
    temp.words.v2 = sml0201.words.v2;
    updated.rare = sml0201.rare;// first time
    updated.merge = sml0201.merge;
    keys.words.v2 = Object.keys(temp.words.v2);
    hfile.log("keys.words.v2", keys.words.v2.length);


    /*********|Step 2.2: gom nhóm từ đồng nghĩa|*********/
    let sml0202 = ah.handleSimilarWords({
        config,
        keywords: temp.keywords,
        words: temp.words.v2,
        diacritics: updated.diacritics,
        isIgnoreSubkey: true,
    });
    updated.merge = Object.assign(updated.merge, sml0202.merge);
    temp.words.v3 = sml0202.words;
    for (let Keyword of updated.remove) {
        let isExist = list.remove.includes(Keyword);
        if (isExist) continue;
        let obj = { Keyword, dup: 1, type: "bad-words" };
        list.remove.push(Keyword);
        // updated.details.push(obj);
    };
    for (let Keyword of sml0202.remove) {
        let isExist = list.remove.includes(Keyword);
        let isRare = updated.rare.includes(Keyword);
        if (isExist || isRare) continue;
        let kw = temp.keywords[Keyword];
        let Volume = kw.Volume || 0;
        let KD = kw.KD || 0;
        list.remove.push(Keyword);
        let obj = { Keyword, dup: 1, type: "miss", Volume, KD };
        updated.details.push(obj);
    };


    /*********|Step 2.3: handle outline|*********/
    let n = 0;
    let step23 = ah.handleOutLine({ keywords: temp.keywords, words: temp.words.v3 });
    keys.keywords = Object.keys(temp.keywords);
    temp.chart = {
        tool: "tool-keywords-planner",
        data: {
            Keyword: "Home",
            id: 0,
            root: true,
            expand: true,
            children: step23.items,
        },
    };
    for (let Keyword of keys.keywords) {
        let kw = temp.keywords[Keyword];
        let x = kw.items.length - 1;
        if (list.remove.includes(Keyword) || x <= 0) continue;// tìm các từ khóa dup 2 lần
        let obj = { Keyword, dup: x, type: "duplicate-in-files" };
        // updated.details.push(obj);
    };
    temp.text = JSON.stringify(step23.heads);
    for (let Keyword of updated.rare) {
        let reg = new RegExp(`"${escape.regExp(Keyword)}"`, "gi");
        let isExist = list.remove.includes(Keyword);
        let isInListKW = keys.keywords.includes(Keyword);
        let isInListV3 = reg.test(temp.text);
        if (isExist || isInListV3 || !isInListKW) continue;
        let kw = temp.keywords[Keyword];
        let Volume = kw.Volume || 0;
        let KD = kw.KD || 0;
        let obj = { Keyword, dup: 1, type: "rare", Volume, KD };
        list.remove.push(Keyword);
        updated.details.push(obj);
    };
    temp.rare = arraySort(temp.rare, ["Volume", "KD"], { reverse: true });
    keys.merge = Object.keys(updated.merge);
    keys.diacritics = Object.keys(updated.diacritics);
    for (let keyFrom of keys.merge) {
        let reg = new RegExp(`"${escape.regExp(keyFrom)}"`, "gi");
        let keyTo = updated.merge[keyFrom];// bỏ keyFrom - dùng keyTo
        let isExist = list.remove.includes(keyFrom);
        let isInListKW = keys.keywords.includes(keyFrom);
        let isInListV3 = reg.test(temp.text);
        if (isExist || isInListV3 || !isInListKW) continue;
        let obj = { Keyword: keyFrom, dup: 1, type: "replace", name: keyTo };
        list.remove.push(keyFrom);
        updated.details.push(obj);
    };
    // for (let keyFrom of keys.diacritics) {
    //     let reg = new RegExp(`"${escape.regExp(keyFrom)}"`, "gi");
    //     let keyTo = updated.diacritics[keyFrom];// bỏ keyFrom - dùng keyTo
    //     let isInListV3 = reg.test(temp.text);
    //     let kw = temp.keywords[keyTo];
    //     if (!isInListV3 || !kw) continue;
    //     let obj = { Keyword: keyFrom, dup: 1, type: "diacritics", name: keyTo };
    //     list.remove.push(keyFrom);
    //     updated.details.push(obj);
    // };
    for (let item of updated.details) n += item.dup;
    hfile.log("ahrefs.keywords.details", n);


    /*********|write file|*********/
    let fsConfig = { encoding: "utf-8" };
    fs.ensureDirSync(`./data/export/${x}`);
    let fileWrite = {
        filter: `./data/export/${x}/${x}-filter.json`,
        keywords: `./data/export/${x}/${x}-ahrefs.keywords.json`,
        v1: `./data/export/${x}/${x}-ahrefs.v1.json`,
        v2: `./data/export/${x}/${x}-ahrefs.v2.json`,
        v3: `./data/export/${x}/${x}-ahrefs.v3.json`,
        merge: `./data/export/${x}/${x}-merge.json`,

        diacritics: `./data/export/${x}/${x}-ahrefs.diacritics.json`,
        details: `./data/export/${x}/${x}-ahrefs.details.json`,
        heads: `./data/export/${x}/${x}-heads.json`,
        chart: `./data/export/${x}/${x}-chart.json`,

        exportData: `./data/export/${x}/${x}-file-data.xlsx`,
        exportOutline: `./data/export/${x}/${x}-keywords.outline.xlsx`,
        exportEmail: `./data/export/${x}/${x}-email.xlsx`,
    };
    fs.writeJSONSync(fileWrite.keywords, temp.keywords, fsConfig);
    fs.writeJSONSync(fileWrite.v1, temp.words.v1, fsConfig);
    fs.writeJSONSync(fileWrite.v2, temp.words.v2, fsConfig);
    fs.writeJSONSync(fileWrite.v3, temp.words.v3, fsConfig);
    fs.writeJSONSync(fileWrite.merge, updated.merge, fsConfig);
    fs.writeJSONSync(fileWrite.diacritics, updated.diacritics, fsConfig);
    fs.writeJSONSync(fileWrite.details, updated.details, fsConfig);
    fs.writeJSONSync(fileWrite.heads, step23.heads, fsConfig);
    fs.writeJSONSync(fileWrite.chart, temp.chart, fsConfig);


    /*********|send email|*********/
    const dfe = df.excel;
    let sheet01 = {
        sheetName: "keywords.outline",
        columns: [
            { header: "STT", key: "STT", style: numberStyle },
        ],
        config: { worksheet: dfe.worksheet },
        rows: step23.heads,
    };
    hfile.log("sheet01", sheet01.rows.length);
    for (let i = 1; i <= step23.maxLevel; i++) {
        sheet01.columns.push({
            header: `Level${i}`,
            key: `level${i}`,
            width: 25,
        });
    };
    sheet01.columns.push({ header: "Volume", key: "Volume", style: numberStyle });
    sheet01.columns.push({ header: "KD", key: "KD", style: numberStyle });
    sheet01.columns.push({ header: "Avg Volume", key: "AvgVolume", style: numberStyle });
    sheet01.columns.push({ header: "Avg KD", key: "AvgKD", style: numberStyle });
    sheet01.columns.push({ header: "Difficulty", key: "KD", style: numberStyle });
    let filter = {
        htmlText: {
            file: "./data/templates/email.ahrefs.keywords.export.html",
            data: { member, keywords: list.input },
        },
        email: {
            Profile: df.email.profile,
            MailFromAlias: "Bot Isaura - Ahrefs",
            MailCC: df.email.cc,
            MailSubject: "Kết quả gom nhóm từ khóa",
            MailTo: member.Email,
            MailBody: null,
            AttachFiles: [
                path.resolve(fileWrite.exportData),
                path.resolve(fileWrite.exportOutline),
            ],
        },
        outline: {
            x,
            fileName: "keywords.outline",
            data: [sheet01],
        },
        socket: {
            SessionID: api.net.config.SessionID,
            key: "res.ahrefs.keywords.export",
            to: [member.SocketID],
            data: {
                from: { id: member.SocketID },
                payload: { is_success: true, data: { id: x } },
            },
        },
    };
    await helpers.excel.export(filter.outline);
    if (isTest) return { is_success: true, data: "Ahazon seo" };
    let objFileReplace = helpers.file.replace(filter.htmlText);
    if (Array.isArray(objFileReplace.errors) && objFileReplace.errors.length) {
        hfile.log("ahrefs.email.replace.error", objFileReplace.errors);
        return { errors: objFileReplace.errors };
    };
    filter.email.MailBody = objFileReplace.text;
    fs.writeFileSync(fileWrite.exportEmail, objFileReplace.text, fsConfig);
    for (let keyFile of Object.keys(fileWrite)) {
        let filePath = fileWrite[keyFile];
        let fileFrom = path.resolve(filePath);
        let fileTo = `${df.copy.dirTo}/${x}/${path.basename(filePath)}`;
        fs.copy(fileFrom, fileTo);
    };
    hfile.log("send.mail.start");
    api.net.email.sender.send(filter.email).then((res_email) => {
        hfile.log("send.mail.end", res_email);
        if (!res_email.isSuccess) hfile.log("ahrefs.keywords.send.mail.error", res_email);
    });
    hfile.log("socket.start");
    api.webhook.socket.emit(filter.socket);
    hfile.log("ahrefs.keywords.group.stop", moment().format(cafe.format.dateTime));
    let res = { is_success: true, data: filter.socket.data };

    return res;
};

module.exports = {
    groupKeywords,
};
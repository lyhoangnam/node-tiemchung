const arraySort = require("array-sort");
const { helpers } = require("../../utils");
const fs = require("fs-extra");
const { escape } = helpers.text;
const hfile = helpers.file;
const fsConfig = { encoding: "utf-8" };
const isTest = 1;

const checkIgnore = ({ Keyword, ignore }) => {
    if (!Keyword) return true;
    let isIgnore = ignore.list.includes(Keyword);
    if (isIgnore) return true;
    for (let regText of ignore.reg) {
        let reg = new RegExp(regText, "gi");
        isIgnore = reg.test(Keyword);
        if (isIgnore) break;
    };
    return isIgnore;
};

const handleReplaceText = ({ Keyword, replaceObj = {} }) => {
    let data = { text: Keyword, dup: [] };
    if (!Keyword) return data;
    Keyword = Keyword.trim().replace(/\s\s/g, "");
    for (let txtFrom of Object.keys(replaceObj)) {
        let txtTo = replaceObj[txtFrom];
        let reg = new RegExp(txtFrom, "gi");
        if (reg.test(data.text)) {
            data.dup.push(data.text);
            data.text = data.text.replace(txtFrom, txtTo);
        };
    };

    return data;
};

const handlePermuteWords = ({ words = {}, Keyword, ignore, missing = 0 }) => {
    if (!Keyword) return {};
    let word;
    let list = [];
    let arr = Keyword.split(" ");
    let n = arr.length;
    if (n === 1) {
        word = words[Keyword];
        if (!word) word = { related: [] };
        list.push(Keyword);
        words[Keyword] = word;
    } else {
        for (let w = 0; w < n - 1; w++) {
            let sub = [arr[w]];
            let text;
            for (let i = w + 1; i < n; i++) {
                sub.push(arr[i]);
                text = sub.join(" ");
                if (list.includes(text)) continue;
                let isIgnore = checkIgnore({ Keyword: text, ignore });
                if (isIgnore) continue;
                word = words[text] || { related: [] };
                list.push(text);
                if (Keyword !== text && !word.related.includes(Keyword)) word.related.push(Keyword);
                words[text] = word;
            };
            if (w <= missing || missing < 0) continue;
            let miss = Keyword.split(" ");
            miss.splice(w, 1);
            text = miss.join(" ");
            if (!list.includes(text)) list.push(text);
            word = words[text] || { related: [] };
            if (!word.related.includes(Keyword)) word.related.push(Keyword);
            words[text] = word;
        };
    };

    return { words, list };
};

const handleRemoveDiacritics = ({ permute, keywords }) => {
    let data = { v1: {}, v2: {} };
    if (!Array.isArray(permute)) return;
    for (let kw of permute) {
        let rd = helpers.text.remove.diacritics(kw);
        if (rd === kw) continue;
        let item = data.v1[rd];
        if (!item) item = { inlist: [], outlist: [], all: [] };
        let key = !!keywords[kw] ? "inlist" : "outlist";
        if (!item.all.includes(kw)) {
            item.all.push(kw);
            item[key].push({ text: kw, ascii: helpers.text.sum.ascii(kw) });
        };
        data.v1[rd] = item;
    };
    for (let rd of Object.keys(data.v1)) {
        let item = data.v1[rd];
        if (item.all.length <= 1) {
            data.v2[rd] = item.all[0];
        } else {
            let items = item.inlist.length ? item.inlist : item.outlist;
            arraySort(items, ["ascii"], { reverse: true });
            data.v2[rd] = items[0].text;
        };
    };

    return data;
};

const handleRareKeywords = ({ keywords, words, sortBy = "Volume", rare = [], merge = {} }) => {
    let data = {
        words: { v1: {}, v2: {} },
        reverse: {},
        rare,
        merge,
    };
    let keys = {
        words: Object.keys(words),
        reverse: [],
    };
    if (!keywords || !words) return data;

    /*********|lọc tách rare keywords|*********/
    for (let Keyword of keys.words) {
        let word = words[Keyword];
        let { related } = word;
        if (!related.length) {
            if (!data.rare.includes(Keyword) && keywords[Keyword]) data.rare.push(Keyword);
            continue;
        };
        if (related.length <= 1) {
            let iKey = Keyword;
            let jKey = related[0];
            let iObj = keywords[iKey] || { Volume: 0 };
            let jObj = keywords[jKey] || { Volume: 0 };
            let key = { updated: null, remove: null };
            if (jObj.Volume >= iObj.Volume) key = { updated: jKey, remove: iKey };
            else key = { updated: iKey, remove: jKey };
            if (!data.rare.includes(key.remove) && keywords[key.remove]) data.rare.push(key.remove);
            let iUpdate = data.words.v1[key.updated] || { related: [] };
            data.words.v1[key.updated] = iUpdate;
            continue;
        };
        related.sort();
        data.words.v1[Keyword] = { related };
        let reverse = JSON.stringify(related);
        let arr = data.reverse[reverse];
        if (!arr) arr = [];
        if (!arr.includes(Keyword)) arr.push(Keyword);
        data.reverse[reverse] = arr;
    };

    /*********|handle từ đồng nghĩa|*********/
    keys.reverse = Object.keys(data.reverse);
    for (let reverse of keys.reverse) {
        let arr = data.reverse[reverse];
        if (!arr.length) continue;
        if (arr.length <= 1) {
            let key = arr[0];
            let kw = data.words.v1[key];
            data.words.v2[key] = kw;
            continue;
        };
        switch (sortBy) {
            case "Volume":
                let sum = { max: -1, Keyword: null };
                for (let Keyword of arr) {
                    let kw = keywords[Keyword];
                    if (!kw) continue;
                    if (kw.Volume > sum.max) sum = { max: kw.Volume, Keyword };
                };
                if (!sum.Keyword) sum.Keyword = arr.sort((a, b) => b.length - a.length)[0];// bot tự phân tích và gom nhóm với từ khóa không thuộc list
                for (let Keyword of arr) {
                    if (Keyword === sum.Keyword || !keywords[Keyword]) continue;
                    data.merge[Keyword] = sum.Keyword;
                };
                data.words.v2[sum.Keyword] = data.words.v1[sum.Keyword];
                break;
            default:
                break;
        };
    };
    delete data.reverse;// xóa nó đi; để làm gì cho nặng máy

    return data;
};

const handleSimilarWords = ({ config, keywords, words, wRelated = null, diacritics, sortBy = "RelatedLength", isIgnoreSubkey = true, count = 1 }) => {
    let data = {
        words: {},
        merge: {},
        v1: {
            related: {},
            children: {},
            merge: {},
        },
        v2: {
            merge: {},
            group: {},
            related: [],
        },
        remove: [],
    };
    if (!keywords || !words || isNaN(config.x)) return data;


    /*********|1. Lọc từ gần nghĩa|*********/
    const { x } = config;
    let temp = {
        words: "",
        update: {},
        v2: {
            words: "",
        },
    };
    let keys = {
        keywords: [],
        update: [],
        words: Object.keys(words).sort((a, b) => a.length - b.length),
        merge: [],
        v1: {
            related: [],
            children: [],
            merge: [],
        },
        v2: {
            merge: [],
            group: [],
        },
    };
    for (let i = 0; i < keys.words.length - 1; i++) {
        let iKey = keys.words[i];
        let iObj = words[iKey];
        if (keywords[iKey] && !keys.keywords.includes(iKey)) keys.keywords.push(iKey);
        let iRelated = iObj.related;
        if (!iRelated.length) continue;
        for (let rlt of iRelated) if (keywords[rlt] && !keys.keywords.includes(rlt)) keys.keywords.push(rlt);


        for (let j = i + 1; j < keys.words.length; j++) {
            let jKey = keys.words[j];
            let jObj = words[jKey];
            let jRelated = jObj.related;
            if (!jRelated.length) continue;
            let ijRelated = [];
            let compareFrom = [];
            let compareTo = [];
            let key = null
            let subkey = null;
            let obj = null;
            let iCheck = JSON.stringify(iRelated);
            let jCheck = JSON.stringify(jRelated);

            // trường hợp này đồng nghĩa 100%
            if (iCheck === jCheck) {
                key = iKey;
                subkey = jKey;
                if (keywords[subkey]) {
                    obj = data.v1.related[key] || {};
                    obj[subkey] = {};
                    data.v1.related[key] = obj;
                };
                continue;
            };
            switch (sortBy) {
                case "RelatedLength":
                    if (iRelated.length > jRelated.length) {// || !isIgnoreSubkey
                        // chữ ngắn mang nhiều nghĩa hơn chữ dài - lấy chữ ngắn làm gốc so với chữ dài
                        key = iKey;
                        subkey = jKey;
                        compareFrom = iRelated;
                        compareTo = jRelated;
                    } else {
                        // chữ dài mang nhiều nghĩa hơn chữ ngắn - lấy chữ dài làm gốc so với chữ ngắn
                        key = jKey;
                        subkey = iKey;
                        compareFrom = jRelated;
                        compareTo = iRelated;
                    };
                    if (!key.indexOf(subkey)) continue;
                    break;
                case "KeywordLength":
                    if (jKey.length <= iKey.length) continue;
                    key = iKey;
                    subkey = jKey;
                    compareFrom = iRelated;
                    compareTo = jRelated;
                    break;
                default:
                    break;
            };
            if (isIgnoreSubkey) {
                if (!keywords[subkey]) continue;// từ khóa con không thuộc tệp ban đầu
                if (compareFrom.length <= 1) continue;// từ mang 1 nghĩa thì khỏi phân nhóm
            };
            let expectedRelatedLength = ~~compareFrom.length * config.duplicate.percent;
            for (let rlt of compareFrom) if (compareTo.includes(rlt)) ijRelated.push(rlt);
            let change = compareFrom.length - ijRelated.length;
            let isInExpectedPercent = ijRelated.length >= expectedRelatedLength;
            let isInExpectedChange = change <= config.duplicate.change;
            let isChild = ijRelated.length === compareTo.length;
            let isLongKeyword = subkey.split(" ").length > 1;
            if (!ijRelated.length) continue;
            if (ijRelated.length <= 1 && compareTo.length > 1 && isLongKeyword && isIgnoreSubkey) continue;// chúng ta không thuộc về nhau
            if (key === subkey) continue;// parent và child là 1 thì bỏ qua
            if (!isInExpectedPercent && !isInExpectedChange) {
                let isAlreadyInRelated = compareFrom.includes(subkey);
                // !isIgnoreSubkey = isCare
                if (isChild && (!isIgnoreSubkey || !isAlreadyInRelated)) {
                    obj = data.v1.children[key] || {};
                    obj[subkey] = {};
                    data.v1.children[key] = obj;
                };
                continue;
            };
            obj = data.v1.related[key] || {};
            obj[subkey] = {};
            data.v1.related[key] = obj;
        };
    };


    /*********|2.1 Chuyển đổi v1.related và v1.children vào 1 đối tượng là v1.merge|*********/
    keys.v1.related = Object.keys(data.v1.related);
    keys.v1.children = Object.keys(data.v1.children);
    for (let Keyword of keys.v1.children) {
        let item = data.v1.children[Keyword];
        let keysChild = Object.keys(item);
        if (keysChild.length === 0) continue;
        let iMerge;
        if (keysChild.length === 1) {
            let key = { updated: Keyword, remove: keysChild[0] };
            iMerge = data.v1.merge[key.updated];
            if (!iMerge) iMerge = {};
            iMerge[key.remove] = {};
            data.v1.merge[key.updated] = iMerge;
            continue;
        };
        iMerge = data.v1.merge[Keyword] || {};
        data.v1.merge[Keyword] = Object.assign(iMerge, item);
    };
    for (let Keyword of keys.v1.related) {
        let item = data.v1.related[Keyword];
        let keysChild = Object.keys(item);
        if (keysChild.length === 0) continue;
        let iMerge;
        if (keysChild.length === 1) {
            let key = { updated: Keyword, remove: keysChild[0] };
            iMerge = data.v1.merge[key.updated];
            if (!iMerge) iMerge = {};
            iMerge[key.remove] = {};
            data.v1.merge[key.updated] = iMerge;
            continue;
        };
        iMerge = data.v1.merge[Keyword] || {};
        data.v1.merge[Keyword] = Object.assign(iMerge, item);
    };


    /*********|2.2 Phân tích v1.merge xem nhóm nào có 1 child thì merge lại|*********/
    fs.writeJSONSync(`./data/export/${x}/data.v1.related.${count}.json`, data.v1.related, fsConfig);
    fs.writeJSONSync(`./data/export/${x}/data.v1.children.${count}.json`, data.v1.children, fsConfig);
    fs.writeJSONSync(`./data/export/${x}/data.v1.merge.${count}.json`, data.v1.merge, fsConfig);
    let fMerge = {
        x,
        keywords,
        words: data.v1.merge,
        smlCount: count,
        sortBy: count <= 1 ? "Volume" : "KeywordLength",
    };
    let dMerge = handleUpdateMerge(fMerge);
    data.merge = Object.assign(data.merge, dMerge.merge);
    if (isTest) {
        fs.writeJSONSync(`./data/export/${x}/dMerge.group.${fMerge.sortBy}.${count}.json`, dMerge.group, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.words.${fMerge.sortBy}.${count}.json`, dMerge.words, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.merge.${fMerge.sortBy}.${count}.json`, dMerge.merge, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.related.${fMerge.sortBy}.${count}.json`, dMerge.related, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.remove.${fMerge.sortBy}.${count}.json`, dMerge.remove, fsConfig);
    };


    /*********|3.1 Cập nhật lại giá trị ở bước 2.2 vào v2|*********/
    keys.v2.group = Object.keys(dMerge.group);
    wRelated = wRelated || words;
    for (let Keyword of keys.v2.group) {
        let iMerge = dMerge.group[Keyword];
        let item = { related: Object.keys(iMerge) };
        data.v2.group[Keyword] = item;
        data.words[Keyword] = item;
    };


    /*********|3.2 Tìm những từ còn xót lại sau khi gom nhóm|*********/
    for (let Keyword of keys.words) {
        let item = data.words[Keyword];
        if (!item) data.v2.related.push(Keyword);
    };
    keys.words = Object.keys(data.words).sort().sort((a, b) => b.length - a.length);
    temp.v2.words = JSON.stringify(data.words);
    keys.keywords = keys.keywords.sort().sort((a, b) => a.length - b.length);
    for (let iKey of keys.keywords) {
        let reg = new RegExp(`"${escape.regExp(iKey)}"`, "gi");
        let isExist = reg.test(temp.v2.words);
        if (isExist) continue;
        let item;
        let rd = diacritics[iKey];
        if (rd) {
            reg = new RegExp(`"${escape.regExp(rd)}"`, "gi");
            isExist = reg.test(temp.v2.words);
            if (isExist) continue;
            if (!data.v2.merge[rd] && keywords[rd]) iKey = rd;
        };
        isExist = false;
        for (let jKey of keys.words) {
            if (isExist) break;
            item = data.words[jKey];
            let isContains = jKey.includes(iKey) || iKey.startsWith(jKey) || iKey.endsWith(jKey);
            isExist = isExist || isContains;
            if (isContains && !item.related.includes(iKey)) item.related.push(iKey);// lines 2085 keys 65
        };
        if (isExist) continue;
        let related = handlePermuteWords({ Keyword: iKey, ignore: config.ignore, missing: -1 }).list;
        keys.v2.merge.push(iKey);
        data.v2.merge[iKey] = { related };
    };
    keys.words = Object.keys(data.words).sort().sort((a, b) => a.length - b.length);
    if (sortBy === "RelatedLength" && false) {
        for (let Keyword of keys.words) {
            let word = data.words[Keyword];
            if (!word) continue;
            let iChildren = word.children || [];
            let fsml = { config, keywords, words: {}, wRelated, diacritics, sortBy: "KeywordLength", isIgnoreSubkey: true, count: 1 };
            let child, iuRelated, iuChildren;
            for (let rlt of word.related) {
                child = data.words[rlt];
                if (child) temp.update[rlt] = child;
                let rltRelated = handlePermuteWords({ Keyword: rlt, ignore: config.ignore, missing: -1 }).list;
                fsml.words[rlt] = { related: rltRelated };
            };
            let sml = handleSimilarWords(fsml);
            let keysChild = Object.keys(sml.words);
            let _related = Object.keys(sml.v2.merge);
            let children = [];
            for (let keyChild of keysChild) {
                let smlWord = sml.words[keyChild];
                child = { Keyword: keyChild, children: [] };
                let smlRelated = smlWord.related || [];
                let iUpdate = data.words[keyChild] || temp.update[keyChild];
                for (let smlrlt of smlRelated) {
                    let isChild =
                        smlrlt !== keyChild &&
                        (keyChild.includes(smlrlt) || smlrlt.includes(keyChild));
                    if (isChild) child.children.push({ Keyword: smlrlt });
                    else if (!_related.includes(smlrlt) && !keysChild.includes(smlrlt)) _related.push(smlrlt);
                };
                if (iUpdate) {
                    hfile.log("child.01", `${keyChild} ==> ${Keyword}`);
                    iuRelated = iUpdate.related || [];
                    iuChildren = iUpdate.children || [];
                    if (iuRelated.length) {
                        for (let crlt of iuRelated) {
                            if (!keywords[crlt]) continue;
                            child.children.push({ Keyword: crlt });
                        };
                    };
                    if (iuChildren.length) {
                        for (let grandChild of iuChildren) {
                            if (iuRelated.includes(grandChild.Keyword) || !keywords[grandChild.Keyword]) continue;
                            child.children.push({ Keyword: grandChild.Keyword });
                        };
                    };
                    temp.update[keyChild] = iUpdate;
                    keys.update.push(keyChild);
                };
                if (!child.children.length) delete child.children;
                iChildren.push(child);
            };
            if (iChildren.length <= 0) {
                // tách _related ra thành children
                for (let _rlt of _related) {
                    let iUpdate = data.words[_rlt] || temp.update[_rlt];
                    child = { Keyword: _rlt, children: [] };
                    if (iUpdate) {
                        hfile.log("child.02", `${_rlt} ==> ${Keyword}`);
                        iuRelated = iUpdate.related || [];
                        iuChildren = iUpdate.children || [];
                        if (iuRelated.length) {
                            for (let crlt of iuRelated) {
                                if (!keywords[crlt]) continue;
                                child.children.push({ Keyword: crlt });
                            };
                        };
                        if (iuChildren.length) {
                            for (let grandChild of iuChildren) {
                                if (iuRelated.includes(grandChild.Keyword) || !keywords[grandChild.Keyword]) continue;
                                child.children.push({ Keyword: grandChild.Keyword });
                            };
                        };
                        temp.update[_rlt] = iUpdate;
                        keys.update.push(_rlt);
                    };
                    if (!child.children.length) delete child.children;
                    iChildren.push(child);
                };
            } else if (_related.length) {
                // chia _related thành granChild
                child = { Keyword: "_related", children: [] };
                for (let _rlt of _related) {
                    let smlUpdate = temp.update[_rlt] || data.words[_rlt];
                    if (!smlUpdate) child.children.push({ Keyword: _rlt });
                    else {
                        hfile.log(`smlUpdate.${_rlt}`, JSON.stringify(smlUpdate));
                        let spliceChild = { Keyword: _rlt, children: smlUpdate.children };
                        iChildren.push(spliceChild);
                        temp.update[_rlt] = smlUpdate;
                        keys.update.push(_rlt);
                    };
                };
                iChildren.push(child);
            };
            for (let iChild of iChildren) children.push(iChild);
            word.children = children;// line 12314 keys 63
        };
        keys.words = Object.keys(data.words);
        for (let Keyword of keys.words) {
            delete data.words[Keyword].related;
            if (!keys.update.includes(Keyword)) continue;
            hfile.log("remove data.words", Keyword);
            // delete data.words[Keyword];
        };
    };
    if (count <= 1) {
        // delete word.related;
        // let fsml = {
        //     config,
        //     keywords,
        //     words: data.v2.merge,
        //     wRelated: words,
        //     diacritics,
        //     sortBy: "KeywordLength",
        //     isIgnoreSubkey: true,
        //     count: count + 1,
        // };
        // let sml = handleSimilarWords(fsml);
        // data.words = Object.assign(sml.words, data.words);
        temp.words = JSON.stringify(data.words);
        // let keysSingle = Object.keys(sml.v2.merge);
        // for (let keySingle of keysSingle) {
        //     let reg = new RegExp(`"${escape.regExp(keySingle)}"`, "gi");
        //     let isExist = reg.test(temp.words);
        //     if (isExist) continue;
        //     let item;
        //     let Keyword = keys.words.find(text => keySingle.includes(text));
        //     if (Keyword) {
        //         item = data.words[Keyword];
        //         if (!item) continue;
        //         if (Array.isArray(item.children)) {
        //             let child = item.children.find(c => c.Keyword === "_related");
        //             if (child) child.related.push(keySingle);
        //             else item.children.push({ Keyword: keySingle });
        //         } else {
        //             item.related.push(keySingle);
        //         };
        //     } else {
        //         item = wRelated[keySingle];
        //         if (item) {
        //             if (Array.isArray(item.children)) data.words[keySingle] = item;
        //             else {
        //                 let children = [];
        //                 let related = item.related || [];
        //                 for (let rlt of related) children.push({ Keyword: rlt });
        //                 data.words[keySingle] = { children };
        //             };
        //         } else if (!data.remove.includes(keySingle)) {
        //             data.remove.push(keySingle);// trường hợp này thì Keyword nằm trong related mà không nằm ở ngoài nhóm của words
        //         };
        //     };
        // };
        temp.words = JSON.stringify(data.words);
        for (let Keyword of Object.keys(words).sort((a, b) => a.length - b.length)) {
            let reg = new RegExp(`"${escape.regExp(Keyword)}"`, "gi");
            let kw = keywords[Keyword];
            let isExist = !kw || reg.test(temp.words) || data.remove.includes(Keyword) || data.v2.merge[Keyword];
            if (isExist) continue;
            let rd = diacritics[Keyword];
            if (!rd) {
                data.remove.push(Keyword);
                continue;
            };
            reg = new RegExp(`"${escape.regExp(rd)}"`, "gi");
            kw = keywords[rd];
            isExist = !kw || reg.test(temp.words) || data.remove.includes(Keyword) || data.v2.merge[Keyword];
            if (isExist) continue;
            data.remove.push(Keyword);
        };
        //tiếng anh cho người mới bắt đầu
    };
    // hfile.log("data.remove", JSON.stringify(data.remove));
    fs.writeJSONSync(`./data/export/${x}/data.v2.merge.${count}.json`, data.v2.merge, fsConfig);
    fs.writeJSONSync(`./data/export/${x}/data.v2.group.${count}.json`, data.v2.group, fsConfig);
    fs.writeJSONSync(`./data/export/${x}/data.words.${count}.json`, data.words, fsConfig);
    fs.writeJSONSync(`./data/export/${x}/data.remove.${count}.json`, data.remove, fsConfig);


    return data;
};

const handleUpdateMerge = ({ keywords, words, sortBy = "Volume", merge = {}, wRelated = {}, remove = [], x = 0, smlCount = 1, count = 1 }) => {
    let data = {
        words,
        merge,
        remove,
        group: {},
        related: wRelated,
    };
    let keys = {
        words: [],
        merge: [],
        group: [],
        related: [],
        reverse: [],
        rehandle: [],
    };
    let temp = {
        rehandle: {},
        reverse: {},
    };
    if (isTest) fs.writeJSONSync(`./data/export/0/dMerge.input.${sortBy}.${smlCount}.${count}.json`, words, fsConfig);


    /*********|1. các nhóm có children giống nhau thì chỉ giữ lại 1 group|*********/
    keys.words = Object.keys(data.words).sort().sort((a, b) => b.length - a.length);
    // hfile.log(`update.merge.before.${count}`, keys.words.length);
    for (let Keyword of keys.words) {
        let item = data.words[Keyword];
        let keysChild = Object.keys(item).sort();
        let reverse = JSON.stringify(keysChild);
        let arr = temp.reverse[reverse];
        if (!arr) arr = [];
        if (!arr.includes(Keyword)) arr.push(Keyword);
        temp.reverse[reverse] = arr;
    };
    keys.reverse = Object.keys(temp.reverse);
    for (let reverse of keys.reverse) {
        let keysChild = temp.reverse[reverse];
        if (keysChild.length < 2) continue;
        let arr = JSON.parse(reverse);
        let keysRemove = keysChild;
        let sum = { max: -1, Keyword: null };
        if (arr.length > 1) {
            switch (sortBy) {
                case "Volume":
                    for (let keyChild of keysChild) {
                        let kw = keywords[keyChild];
                        if (!kw) continue;
                        if (kw.Volume > sum.max) sum = { max: kw.Volume, Keyword: keyChild };
                    };
                    break;
                default:
                    break;
            };
            if (!sum.Keyword) sum.Keyword = keysChild.sort((a, b) => b.length - a.length)[0];
            let k = keysRemove.indexOf(sum.Keyword);
            if (k >= 0) keysRemove.splice(k, 1);
        };
        for (let keyRemove of keysRemove) {
            delete data.words[keyRemove];
            if (sum.Keyword) data.merge[keyRemove] = sum.Keyword;
        };
    };
    keys.words = Object.keys(data.words).sort().sort((a, b) => b.length - a.length);
    // hfile.log(`update.merge.after.${count}`, keys.words.length);


    /*********|2. xóa key trùng 1-1 lần 1|*********/
    let x1 = JSON.stringify(data.words);
    for (let Keyword of keys.words) {
        let item = data.words[Keyword];
        if (!item) continue;
        let keysChild = Object.keys(item);
        if (keysChild.length <= 1) {
            let key = { updated: null, remove: null };
            let iKey = Keyword;
            let jKey = keysChild[0];
            switch (sortBy) {
                case "Volume":
                    let iObj = keywords[iKey] || { Volume: 0 };
                    let jObj = keywords[jKey] || { Volume: 0 };
                    if (jObj.Volume >= iObj.Volume) key = { updated: jKey, remove: iKey };
                    else key = { updated: iKey, remove: jKey };
                    break;
                case "KeywordLength":
                    if (jKey.length < iKey.length) key = { updated: jKey, remove: iKey };
                    else key = { updated: iKey, remove: jKey };
                    break;
                default:
                    break;
            };
            if (key.updated !== key.remove) data.merge[key.remove] = key.updated;
            data.group[key.updated] = {};
            delete data.words[key.updated];
            continue;
        };
        for (let keyChild of keysChild) delete data.group[keyChild];
    };


    /*********|3. sau khi xóa thì replace key-value với data.words|*********/
    keys.merge = Object.keys(data.merge).sort((a, b) => a.length - b.length);
    for (let Keyword of keys.merge) {
        let key = {
            from: escape.regExp(Keyword),
            to: data.merge[Keyword],
        };
        if (keywords[key.from]) {
            let rlt = data.related[key.to] || {};
            rlt[key.from] = {};
            data.related[key.to] = rlt;
        };
        key.from = `"${key.from}"`;
        key.to = `"${key.to}"`;
        data.remove.push(Keyword);
        let text = JSON.stringify(data.words);
        let reg = new RegExp(key.from, "gi");
        text = text.replace(reg, key.to);
        data.words = JSON.parse(text);
    };


    /*********|4. xử lý data return|*********/
    let x2 = JSON.stringify(data.words);
    if (x1.length > x2.length) {
        // inprocess
        let dMerge = handleUpdateMerge({
            keywords,
            words: data.words,
            merge: data.merge,
            remove: data.remove,
            wRelated: data.related,
            count: count + 1,
        });
        data.group = Object.assign(data.group, dMerge.group);
        data.words = dMerge.words;
    };
    if (count <= 1) {
        /*********|5.1 cập nhật lại related return|*********/
        let related = {};
        keys.related = Object.keys(data.related);
        for (let Keyword of keys.related) {
            let rlt = data.related[Keyword];
            let keysChild = Object.keys(rlt);
            if (keysChild.length <= 1) continue;
            delete data.related[Keyword];
            let item = data.words[Keyword] || {};
            data.words[Keyword] = Object.assign(item, rlt);
        };
        keys.related = Object.keys(data.related);
        for (let Keyword of keys.related) {
            let rlt = data.related[Keyword];
            let keysChild = Object.keys(rlt);
            related[Keyword] = keysChild[0];
        };

        /*********|5.3 các single word mà không thuộc keywords thì xóa|*********/
        let dh = handleKeysContains({ words: data.words });
        temp.rehandle = dh.rehandle;
        keys.rehandle = Object.keys(temp.rehandle);
        for (let Keyword of keys.rehandle) {
            let item = {};
            let arr = temp.rehandle[Keyword];
            let keysChild = [Keyword, ...arr];
            let sum = { max: -1, Keyword: null };
            let keyFirst = keysChild.sort((a, b) => a.length - b.length)[0];
            let keyLast = keysChild.sort((a, b) => b.length - a.length)[0];
            switch (sortBy) {
                case "Volume":
                    for (let keyChild of keysChild) {
                        let kw = keywords[keyChild];
                        if (!kw) continue;
                        if (kw.Volume > sum.max) sum = { max: kw.Volume, Keyword: keyChild };
                    };
                    break;
                default:
                    break;
            };
            if (!sum.Keyword) sum.Keyword = keyLast;
            for (let keyChild of keysChild) {
                let child = data.words[keyChild] || {};
                if (keyChild === sum.Keyword) continue;
                if (keyChild !== keyFirst && keywords[keyChild]) item[keyChild] = {};
                for (let keyGrandChild of Object.keys(child)) if (keywords[keyGrandChild] && ![sum.Keyword, keyFirst].includes(keyGrandChild)) item[keyGrandChild] = {};
                delete data.words[keyChild];
                delete data.group[keyChild];// nằm bên nhóm thì ko để nằm bên single nữa
            };
            if (Object.keys(item).length > 1) data.words[sum.Keyword] = item;
            else data.group[sum.Keyword] = {};
        };
        // hfile.log("temp.rehandle", JSON.stringify(temp.rehandle));

        /*********|5.4 các single word mà không thuộc keywords thì xóa|*********/
        keys.group = Object.keys(data.group);
        for (let Keyword of keys.group) {
            let kw = keywords[Keyword];
            if (!kw) delete data.group[Keyword];
        };

        /*********|5.5 đảo vị cho dễ hình dung|*********/
        data = {
            words: data.group,
            merge: data.merge,
            remove: data.remove,
            group: data.words,
            related,
        };
        fs.writeJSONSync(`./data/export/${x}/dMerge.output.words.${sortBy}.${smlCount}.${count}.json`, data.words, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.output.merge.${sortBy}.${smlCount}.${count}.json`, data.merge, fsConfig);
        fs.writeJSONSync(`./data/export/${x}/dMerge.output.group.${sortBy}.${smlCount}.${count}.json`, data.group, fsConfig);
    };

    return data;
};

const handleKeysContains = ({ words }) => {
    let data = { rehandle: {} };
    if (!words) return data;
    let keys = {
        words: Object.keys(words).sort(),
        rehandle: [],
    };
    for (let i = 0; i < keys.words.length - 1; i++) {
        let iKey = keys.words[i];
        for (let j = i + 1; j < keys.words.length; j++) {
            let jKey = keys.words[j];
            let isContains = jKey.startsWith(iKey) || jKey.endsWith(iKey);
            if (!isContains) continue;
            let rhd = data.rehandle[iKey] || [];
            keys.rehandle.push(jKey);
            rhd.push(jKey);
            data.rehandle[iKey] = rhd;
        };
    };

    return data;
};

const handleOutLine = ({ keywords, words }) => {
    let data = { heads: [], items: [], maxLevel: 1 };
    if (!words || !keywords) return data;
    let res = slitChildren({
        keywords,
        heads: data.heads,
        item: words,
        key: "Keyword",
    });
    data.maxLevel = res.maxLevel;
    data.items = res.items;
    // hfile.log("data.items", JSON.stringify(data.items));
    hfile.log("outline.maxLevel", res.maxLevel);
    // hfile.log("outline.data", JSON.stringify(res.heads));

    return data;
};

const slitChildren = ({ heads = [], keywords, level = 1, maxLevel = 1, item, key }) => {
    let res = { heads, level, maxLevel, items: [] };
    if (!item) return res;
    let children = [];
    let isStart = heads.length === 0;
    if (isStart) {
        let keys = Object.keys(item);
        for (let Keyword of keys) {
            let kw = keywords[Keyword] || { Volume: 0, KD: 0 };
            const { Volume, KD } = kw;
            let obj01 = { Keyword, Volume, KD };
            let obj02 = item[Keyword];
            let obj = Object.assign(obj01, obj02);
            item[Keyword] = obj;
            children.push(obj);
        };
    } else {
        for (let [c, child] of item.children.entries()) {
            const { Keyword } = child;
            let kw = keywords[Keyword] || { Volume: 0, KD: 0 };
            const { Volume, KD } = kw;
            let obj01 = { Keyword, Volume, KD };
            let obj02 = child;
            let obj = Object.assign(obj01, obj02);
            item.children[c] = obj;
            children.push(obj);
        };
    };
    children = arraySort(children, ["Volume"], { reverse: true });
    let sum = { Volume: 0, KD: 0, count: 0 };
    for (let child of children) {
        const { Keyword, Volume, KD } = child;
        let iChild = { STT: res.heads.length + 1, [`level${level}`]: child[key], Keyword, Volume, KD };
        sum.Volume += Volume || 0;
        sum.KD += KD || 0;
        sum.count += 1;
        res.heads.push(iChild);
        if (!Array.isArray(child.children) || !child.children.length) continue;
        let lv = level + 1;
        if (lv > res.maxLevel) res.maxLevel = lv;
        let obj = slitChildren({ heads: res.heads, keywords, item: child, key, level: lv, maxLevel: res.maxLevel });
        res.maxLevel = obj.maxLevel;
    };
    if (isStart) res.items = children;
    else {
        item.children = children;
        // item.AvgVolume = parseFloat((sum.Volume / sum.count).toFixed(2));
        // item.AvgKD = parseFloat((sum.KD / sum.count).toFixed(2));
    };

    return res;
};

module.exports = {
    checkIgnore,
    handleReplaceText,
    handlePermuteWords,
    handleRemoveDiacritics,
    handleRareKeywords,
    handleSimilarWords,
    handleOutLine,
    slitChildren,
};
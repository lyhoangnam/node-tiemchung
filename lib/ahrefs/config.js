const { api, helpers } = require("../../utils");
const { numberStyle } = helpers.excel;
module.exports = {
    default: {
        branchGroup: "thương hiệu",
        duplicate: {
            percent: 0.8,// nếu trùng related hơn 80% xem như trùng
            change: 4,// hoặc khác nhau không quá 4 từ cũng xe như trùng
        },
        rehandle: 20,
        excel: {
            columns: {
                sheet01: [
                    { header: "STT", key: "STT", style: numberStyle },
                    { header: "HostName", key: "hostname", width: 25 },
                    { header: "Hạng", key: "ranking", width: 15, style: numberStyle },
                    { header: "DA", key: "DA", width: 15, style: numberStyle },
                    { header: "Backlinks", key: "backlinks", width: 15, style: numberStyle },
                    { header: "Referring", key: "referring", width: 15, style: numberStyle },
                    { header: "Keywords", key: "keywords", width: 15, style: numberStyle },
                ],
                sheet02: [
                    { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                    { header: "Keyword", key: "Keyword", width: 25 },
                    { header: "HostName", key: "HostName", width: 25 },
                    { header: "Position", key: "Position", width: 15 },
                    { header: "Volume", key: "Volume", width: 15 },
                    { header: "KD", key: "KD", width: 15 },
                    { header: "Traffic", key: "Traffic", width: 15 },
                    { header: "Href", key: "Href", width: 40 },
                ],
            },
            worksheet: {
                name: "file-data.xlsx",
                views: [{
                    state: "frozen",
                    xSplit: 1,
                    ySplit: 1,
                    activeCell: "A1"
                }],
            },
        },
        copy: {
            dirTo: "../core.es6.vn/public/download",
        },
        email: {
            profile: "bot",
            cc: "lyhoangnamfpt@gmail.com",
        },
    },
    format: {
        time: "x",
        date: "YYYY-MM-DD",
        dateTime: "YYYY-MM-DD hh-mm-ss A",
    },
    sms: {
        invalid_inputs: api.sms.invalid_inputs,
    },
};
const { workerData, parentPort } = require("worker_threads");
const fs = require("fs-extra");
const moment = require("moment");
const { api, helpers } = require("../../utils");
const hfile = helpers.file;
const cafe = require("./config");
const df = cafe.default;
const isTest = 1;

const exportKeywords = async (payload) => {
    const { member, keywords, friends, config } = payload;
    if (!Array.isArray(friends)) return { error: cafe.sms.invalid_inputs };
    if (!config || !config.ignore || !Array.isArray(config.ignore.reg)) return { error: cafe.sms.invalid_inputs };

    /*********|first config|*********/
    if (isTest) helpers.file.clear();
    hfile.log("ahrefs.keywords.export", "start");
    const dfe = df.excel;
    let sheet01 = {
        sheetName: "friends",
        columns: dfe.columns.sheet01,
        config: { worksheet: dfe.worksheet },
        rows: [],
    };
    let sheet02 = {
        sheetName: "keywords",
        columns: dfe.columns.sheet02,
        config: { worksheet: dfe.worksheet },
        rows: [],
    };

    /*********|loop data|*********/
    let todate = moment().format(cafe.format.date);
    todate = "keywords";
    for (let [f, friend] of friends.entries()) {
        const { hostname, ranking, DA, backlinks, referring, keywords } = friend;
        let dir = `./data/ahrefs/${todate}/${hostname}`;
        let obj = {
            STT: f + 1,
            hostname,
            ranking,
            DA,
            backlinks: backlinks.count,
            referring: referring.count,
            keywords: keywords.count,
        };
        sheet01.rows.push(obj);
        let isExistDir = fs.existsSync(dir);
        if (!isExistDir) {
            hfile.log("ahrefs-folder-not-found", hostname);
            continue;
        };
        let files = [];
        let list = [];
        for (let fileName of fs.readdirSync(dir)) {
            let reg = new RegExp(`${hostname}.keywords.\\d+\\b.json`, "g");
            let isData = reg.test(fileName);
            if (!isData) continue;
            let filePath = `${dir}/${fileName}`;
            files.push(filePath);
        };
        for (let filePath of files) {
            let jsonData = fs.readJSONSync(filePath, { encoding: "utf-8" });
            if (!Array.isArray(jsonData) || jsonData.length < 2) continue;
            let ss = jsonData[0];
            if (typeof (ss) !== "string" || ss.toLowerCase() !== "ok") continue;
            let { rows } = jsonData[1];
            list = [...list, ...rows];
        };
        const res = handleData({ rows: list, hostname });
        if (!res.is_success) continue;
        for (let item of res.data) sheet02.rows.push(item);
    };
    sheet01.sheetName += ` (${sheet01.rows.length})`;
    sheet02.sheetName += ` (${sheet02.rows.length})`;
    hfile.log("ahrefs.keywords.length", sheet02.rows.length);
    let filter = {
        excel: {
            fileName: dfe.worksheet.name,
            data: [sheet01, sheet02],
        },
        socket: {
            SessionID: api.net.config.SessionID,
            key: "res.ahrefs.keywords.export",
            to: [member.SocketID],
            data: {
                from: { id: member.SocketID },
                payload: null,
            },
        },
    };
    let d = { x: 1683081571774 };
    if (!isTest) {
        const res_export = await helpers.excel.export(filter.excel);
        if (!res_export || !res_export.is_success) return res_export;
        d = res_export.data;
        fs.ensureDirSync(df.copy.dirTo);
        let fileFrom = d.fullPath;
        let fileTo = `${df.copy.dirTo}/${d.x}/${d.fileName}`;
        fs.copySync(fileFrom, fileTo);
    };
    let fsConfig = { encoding: "utf-8" };
    let dir = `./data/export/${d.x}`;
    fs.ensureDirSync(dir);
    fs.writeJSONSync(`${dir}/${d.x}-filter.json`, payload, fsConfig);
    let data = { id: d.x };
    const ahrefGroup = require("./ahrefs.group");
    config.x = d.x;
    ahrefGroup.groupKeywords({ member, keywords, data: sheet02.rows, config })
        .then(() => {
            if (isTest) console.log("ahrefs.keywords.group.done");
            hfile.log("ahrefs.keywords.group.done", moment().format(cafe.format.dateTime));
        }).catch((error) => {
            hfile.log("ahrefs.keywords.group.error", error);
            if (isTest) return;
            let isTypeError = error instanceof Error;
            let res = isTypeError ? { error: error.toString() } : { error };
            filter.socket.data.payload = res;
            api.webhook.socket.emit(filter.socket);
        });

    return { is_success: true, data };
};

const handleData = ({ hostname, rows }) => {
    if (!Array.isArray(rows)) return { error: cafe.sms.invalid_inputs };
    if (!rows.length) return { is_success: true, data: [] };
    let item00 = rows[0];
    if (!Array.isArray(item00)) return { error: cafe.sms.invalid_inputs };
    let check01 = item00[0].toLowerCase();
    let check02 = item00[item00.length - 1].toLowerCase();
    let drawcolumns = null;
    let drawcolumns01 = [
        { key: "Language" },
        { key: "Position", type: "int" },
        { key: "SERP" },
        { key: "UpdateDate" },
        { key: "Href" },
        { key: "CPC", type: "int" },
        { key: "Unknown02" },
        { key: "Unknown03" },
        { key: "Traffic", type: "int" },
        { key: "KD", type: "int" },
        { key: "Keyword" },
        { key: "Unknown04" },
        { key: "Unknown05" },
        { key: "Unknown06" },
        { key: "Volume", type: "int" },
        { key: "Unknown07" },
    ];
    let drawcolumns02 = [
        { key: "Unknown01" },
        { key: "Keyword" },
        { key: "SERP" },
        { key: "Volume", type: "int" },
        { key: "KD", type: "int" },
        { key: "CPC", type: "int" },
        { key: "Traffic", type: "int" },
        { key: "Position", type: "int" },
        { key: "Href" },
        { key: "Unknown02" },
        { key: "Unknown03" },
        { key: "Unknown04" },
        { key: "Unknown05" },
        { key: "Unknown06" },
        { key: "UpdateDate" },
        { key: "Language" },
    ];
    if (check01 === "vi") drawcolumns = drawcolumns01;
    else if (check02 === "vi") drawcolumns = drawcolumns02;
    if (!drawcolumns) return { error: cafe.sms.invalid_inputs };
    let list = [];
    for (let row of rows) {
        let obj = { STT: list.length + 1, HostName: hostname };
        for (let [c, col] of drawcolumns.entries()) {
            if (!col.key.toLowerCase().indexOf("unknown")) continue;
            let value = row[c];
            if (col.type === "int") {
                value = parseInt(value, 10);
                if (isNaN(value)) value = 0;
            };
            obj[col.key] = value;
        };
        list.push(obj);
    };

    return { is_success: true, data: list };
};

exportKeywords(workerData).then((res) => {
    parentPort.postMessage(res);
});

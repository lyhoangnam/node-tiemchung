const fs = require("fs-extra");
const arraySort = require("array-sort");
const { helpers } = require("../../utils");
const cafe = {
    default: {
        DateOfBirth: "1957-01-01",
        insert: {
            maxRow: 1000,
        },
    },
    sms: {
        invalid_excel_file: "File excel không hợp lệ!",
    },
};
const { cpo } = helpers.text;
const hFile = helpers.file;
const hExcel = helpers.excel;
const vld = helpers.text.validate;
const df = cafe.default;

const check = async ({ fileName }) => {
    hFile.log("check.start", fileName);
    let data = await hExcel.read({ fileName });
    const columns = [
        "Quarter", "STT", "BlockID", "FullName", "YearOfBirth", "BlockRelationship", "Address01",
        "Populate", "BlockMiddleIncome", "WorkingJob", "WorkingMonth", "IndividualMiddleIncome",
        "BailoutPackage", "PoorRateCount", "PoorRateHealthInsurance", "PoorRateLevelOfEducation",
        "PoorRateWorkingJob", "PoorRateSocialSecurity", "PoorRateHouse", "PoorRateIncome", "PoorRateDependent",
    ];
    let insert = `insert into @temp(${columns.join(",")}) values`;
    let list = [];
    hFile.log(insert);
    for (let item of data) {
        const {
            Quarter, STT, BlockID, FullName, YearOfBirth, BlockRelationship, Address01, WorkingJob, WorkingMonth, IndividualMiddleIncome,
        } = item;
        let BailoutPackage = [];
        let arrbp = (item.BailoutPackage || "").split(",");
        for (let bp of arrbp) {
            bp = bp.trim();
            if (!bp) continue;
            BailoutPackage.push(bp);
        };
        let PoorRateCount = isNaN(item.PoorRateCount) ? 0 : parseInt(item.PoorRateCount, 10);
        let Populate = isNaN(item.Populate) ? `'null'` : parseInt(item.Populate, 10);
        let BlockMiddleIncome = isNaN(item.BlockMiddleIncome) ? 0 : parseInt(item.BlockMiddleIncome, 10);
        let PoorRateHealthInsurance = !!(item.PoorRateHealthInsurance || "").trim() ? 1 : 0;
        let PoorRateLevelOfEducation = !!(item.PoorRateLevelOfEducation || "").trim() ? 1 : 0;
        let PoorRateWorkingJob = !!(item.PoorRateWorkingJob || "").trim() ? 1 : 0;
        let PoorRateSocialSecurity = !!(item.PoorRateSocialSecurity || "").trim() ? 1 : 0;
        let PoorRateHouse = !!(item.PoorRateHouse || "").trim() ? 1 : 0;
        let PoorRateIncome = !!(item.PoorRateIncome || "").trim() ? 1 : 0;
        let PoorRateDependent = !!(item.PoorRateDependent || "").trim() ? 1 : 0;
        let arr = [
            `'${Quarter}'`,
            `'${STT}'`,
            `'${BlockID}'`,
            `N'${FullName}'`,
            `${YearOfBirth}`,
            `N'${BlockRelationship}'`,
            `N'${Address01}'`,
            `${Populate}`,
            `${BlockMiddleIncome}`,
            `N'${WorkingJob}'`,
            `${WorkingMonth}`,
            `${IndividualMiddleIncome}`,
            `N'${JSON.stringify(BailoutPackage)}'`,
            `${PoorRateCount}`,
            `${PoorRateHealthInsurance}`,
            `${PoorRateLevelOfEducation}`,
            `${PoorRateWorkingJob}`,
            `${PoorRateSocialSecurity}`,
            `${PoorRateHouse}`,
            `${PoorRateIncome}`,
            `${PoorRateDependent}`
        ];
        let text = `(${arr.join(",")})`;
        text = text.trim().replace(/N'null'/g, "null").replace(/'null'/g, "null");
        list.push(text);
        if (list.length >= df.insert.maxRow) {
            hFile.log(list.join(",\n"));
            hFile.log(`\n${insert}`);
            list = [];
        };
    };
    hFile.log(list.join(",\n"));
};

const check_bk = async ({ fileName }) => {
    console.log("ubnd.check", "start", fileName);
    let data = {
        sheet01: await helpers.excel.read({ fileName, sheetName: "Sheet1" }),
        sheet02: await helpers.excel.read({ fileName, sheetName: "Sheet2" }),
    };
    if (!Array.isArray(data.sheet01)) return { error: cafe.sms.invalid_excel_file };
    console.log("ubnd.check", "data.sheet01", data.sheet01.length);
    console.log("ubnd.check", "data.sheet02", data.sheet02.length);
    let keys = {
        sheet01: {
            FullName: [],
            FullNameSubAddress: [],
        },
        sheet02: {
            FullName: [],
            FullNameSubAddress: [],
        },
        all: {
            FullName: [],
            FullNameSubAddress: [],
        },
    };
    let temp = {
        sheet01: {
            FullName: {},
            FullNameSubAddress: {},
        },
        sheet02: {
            FullName: {},
            FullNameSubAddress: {},
        },
        all: {
            FullName: {},
            FullNameSubAddress: {},
        },
    };
    let list = {
        sheet01: {
            FullName: [],
            FullNameSubAddress: [],
            onlyFullName: [],
            onlyFSA: [],
        },
        sheet02: {
            FullName: [],
            FullNameSubAddress: [],
            onlyFullName: [],
            onlyFSA: [],
        },
        all: {
            FullName: [],
            FullNameSubAddress: [],
        },
    };
    // sheet01
    for (let item of data.sheet01) {
        let { STT, Part, FullName, Gender, DateOfBirth, IDCard, PhoneNumber, Address01, Address02, SubAddress } = item;
        if (!FullName) hFile.log("FullName.error", STT);
        FullName = vld.FullName(FullName);
        DateOfBirth = DateOfBirth || df.DateOfBirth;
        let iDateOfBirth = vld.DateOfBirth({ STT, DateOfBirth });
        let iAddress01 = vld.Address(Address01);
        let iAddress02 = vld.Address(Address02);
        let iSubAddress = vld.Address(SubAddress);
        let isInValidAddress = iAddress01.isInValid && iAddress02.isInValid && iSubAddress.isInValid;
        // if (isInValidAddress) hFile.log("Address.error", `${Part} ${STT}: ${FullName}`);

        let key = {
            FullName: `${FullName}-${Part}`,
            FSA: `${FullName}-${iSubAddress.Address}-${Part}`,
            all: {
                FullName: `${FullName}-Đợt 1 và 2`,
                FSA: `${FullName}-${iSubAddress.Address}-Đợt 1 và 2`,
            },
        };
        let tFullName = temp.sheet01.FullName[key.FullName] || [];
        let tFSA = temp.sheet01.FullNameSubAddress[key.FSA] || [];
        tFullName.push(STT);
        tFSA.push(STT);
        temp.sheet01.FullName[key.FullName] = tFullName;
        temp.sheet01.FullNameSubAddress[key.FSA] = tFSA;

        temp.all.FullName[key.all.FullName] = temp.all.FullName[key.all.FullName] || [[], []];
        temp.all.FullNameSubAddress[key.all.FSA] = temp.all.FullNameSubAddress[key.all.FSA] || [[], []];
        temp.all.FullName[key.all.FullName][0] = tFullName;
        temp.all.FullNameSubAddress[key.all.FSA][0] = tFSA;
    };
    // hFile.log("temp.all.01", JSON.stringify(temp.all));
    // sheet02
    for (let item of data.sheet02) {
        let { STT, Part, FullName, SubAddress } = item;
        if (!FullName) hFile.log("FullName.error", STT);
        FullName = vld.FullName(FullName);
        let iSubAddress = vld.Address(SubAddress);
        let isInValidAddress = iSubAddress.isInValid;
        if (isInValidAddress) hFile.log("Address.error", `${Part} ${STT}: ${FullName}`);

        let key = {
            FullName: `${FullName}-${Part}`,
            FSA: `${FullName}-${iSubAddress.Address}-${Part}`,
        };
        let tFullName = temp.sheet02.FullName[key.FullName] || [];
        let tFSA = temp.sheet02.FullNameSubAddress[key.FSA] || [];
        tFullName.push(STT);
        tFSA.push(STT);
        temp.sheet02.FullName[key.FullName] = tFullName;
        temp.sheet02.FullNameSubAddress[key.FSA] = tFSA;

        temp.all.FullName[key.FullName] = temp.all.FullName[key.FullName] || [[], []];
        temp.all.FullNameSubAddress[key.FSA] = temp.all.FullNameSubAddress[key.FSA] || [[], []];
        temp.all.FullName[key.FullName][1] = tFullName;
        temp.all.FullNameSubAddress[key.FSA][1] = tFSA;
    };
    // hFile.log("temp.shhet02", JSON.stringify(temp.sheet02));
    // hFile.log("temp.all.02", JSON.stringify(temp.all));

    // sheet01
    keys.sheet01.FullName = Object.keys(temp.sheet01.FullName);
    keys.sheet01.FullNameSubAddress = Object.keys(temp.sheet01.FullNameSubAddress);
    for (let FullName of keys.sheet01.FullName) {
        let arr = temp.sheet01.FullName[FullName];
        let count = arr.length;
        if (count > 1) {
            // hFile.log("FullName.dup.sheet01", FullName, arr.join(","));
            list.sheet01.FullName.push({ FullName, count });
        };
    };
    for (let FSA of keys.sheet01.FullNameSubAddress) {
        let arr = temp.sheet01.FullNameSubAddress[FSA];
        let count = arr.length;
        if (count > 1) {
            // hFile.log("FullNameSubAddress.dup.sheet01", FSA, arr.join(","));
            list.sheet01.FullNameSubAddress.push({ FSA, count });
        };
    };
    arraySort(list.sheet01.FullName, ["count"], { reverse: true });
    arraySort(list.sheet01.FullNameSubAddress, ["count"], { reverse: true });
    // hFile.log("max.sheet01.FullName.dup", list.sheet01.FullName.length);
    // hFile.log("max.sheet01.FSA.dup", list.sheet01.FullNameSubAddress.length);

    // sheet02
    keys.sheet02.FullName = Object.keys(temp.sheet02.FullName);
    keys.sheet02.FullNameSubAddress = Object.keys(temp.sheet02.FullNameSubAddress);
    for (let FullName of keys.sheet02.FullName) {
        let arr = temp.sheet02.FullName[FullName];
        let count = arr.length;
        if (count > 1) {
            // hFile.log("FullName.dup.sheet02", FullName, arr.join(","));
            list.sheet02.FullName.push({ FullName, count, STT: arr });
        };
    };
    for (let FSA of keys.sheet02.FullNameSubAddress) {
        let arr = temp.sheet02.FullNameSubAddress[FSA];
        let count = arr.length;
        if (count > 1) {
            // hFile.log("FullNameSubAddress.dup.sheet02", FSA, arr.join(","));
            list.sheet02.FullNameSubAddress.push({ FSA, count, STT: arr });
        };
    };
    arraySort(list.sheet02.FullName, ["count"], { reverse: true });
    arraySort(list.sheet02.FullNameSubAddress, ["count"], { reverse: true });
    // hFile.log("max.sheet02.FullName.dup", list.sheet02.FullName.length);
    // hFile.log("max.sheet02.FSA.dup", list.sheet02.FullNameSubAddress.length);
    // hFile.log("max.sheet02.FullName.dup", list.sheet02.FullName.length, list.sheet02.FullName[0], list.sheet02.FullName[1], list.sheet02.FullName[2]);
    // hFile.log("max.sheet02.FSA.dup", list.sheet02.FullNameSubAddress.length, list.sheet02.FullNameSubAddress[0], list.sheet02.FullNameSubAddress[1], list.sheet02.FullNameSubAddress[2]);

    // all
    keys.all.FullName = Object.keys(temp.all.FullName);
    keys.all.FullNameSubAddress = Object.keys(temp.all.FullNameSubAddress);
    for (let FullName of keys.all.FullName) {
        let arr = temp.all.FullName[FullName];
        let count = arr.length;
        let arr01 = arr[0];
        let arr02 = arr[1];
        // if (arr01.length && arr02.length) hFile.log("FullName.dup.all", FullName, `${arr[0].map(x => `,STT.${x}`)} | ${arr[1].join(",")}`);
        // else hFile.log("FullName.dup.sheet02", FullName, arr);
        if (arr01.length && !arr02.length) list.sheet01.onlyFullName.push({ FullName, STT: arr });
        if (arr02.length && !arr01.length) list.sheet02.onlyFullName.push({ FullName, STT: arr });
        list.all.FullName.push({ FullName, count, STT: arr });
    };
    for (let FSA of keys.all.FullNameSubAddress) {
        let arr = temp.all.FullNameSubAddress[FSA];
        let count = arr.length;
        let arr01 = arr[0];
        let arr02 = arr[1];
        // if (arr01.length && arr02.length) hFile.log("FullNameSubAddress.dup.all", FSA, `${arr[0].map(x => `,STT.${x}`)} | ${arr[1].join(",")}`);
        // else hFile.log("FullNameSubAddress.dup.sheet02", FSA, arr);
        if (arr01.length && !arr02.length) list.sheet01.onlyFSA.push({ FSA, STT: arr });
        if (arr02.length && !arr01.length) list.sheet02.onlyFSA.push({ FSA, STT: arr });
        list.all.FullNameSubAddress.push({ FSA, count, STT: arr });
    };
    arraySort(list.all.FullName, ["count"], { reverse: true });
    arraySort(list.all.FullNameSubAddress, ["count"], { reverse: true });
    hFile.log("max.all.FullName.dup", list.all.FullName.length);
    hFile.log("max.all.FSA.dup", list.all.FullNameSubAddress.length);
    // hFile.log("max.all.FullName.dup", list.all.FullName.length, list.all.FullName[0], list.all.FullName[1], list.all.FullName[2]);
    // hFile.log("max.all.FSA.dup", list.all.FullNameSubAddress.length, list.all.FullNameSubAddress[0], list.all.FullNameSubAddress[1], list.all.FullNameSubAddress[2]);

    // hFile.log("sheet01.only.Fullname", list.sheet01.onlyFullName.length, list.sheet01.onlyFullName[0]);
    // hFile.log("sheet02.only.Fullname", list.sheet02.onlyFullName.length, list.sheet02.onlyFullName[0]);
    // hFile.log("sheet01.only.FullNameSubAddress", list.sheet01.FullNameSubAddress.length, list.sheet01.FullNameSubAddress[101]);
    // hFile.log("sheet02.only.FullNameSubAddress", list.sheet02.FullNameSubAddress.length, list.sheet02.FullNameSubAddress[201]);

    // for (let item of list.sheet01.onlyFullName) {
    //     const { FullName, STT } = item;
    //     hFile.log("sheet01.only", FullName, STT.join(","));
    // };
    // for (let item of list.sheet01.onlyFSA) {
    //     const { FSA, STT } = item;
    //     hFile.log("sheet01.only", FSA, STT.join(","));
    // };

    // for (let item of list.sheet02.onlyFullName) {
    //     const { FullName, STT } = item;
    //     hFile.log("sheet02.only", FullName, STT.join(","));
    // };
    for (let item of list.sheet02.onlyFSA) {
        const { FSA, STT } = item;
        // hFile.log("sheet02.only", FSA, STT.join(","));
    };


    return { is_success: true };
};

module.exports = {
    check,
    handle: require("./ubnd.handle"),
};
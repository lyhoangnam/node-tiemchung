const fs = require("fs-extra");
const arraySort = require("array-sort");
const { api, helpers } = require("../../utils");
const cafe = require("./config");
const hFile = helpers.file;
const hText = helpers.text;
const hExcel = helpers.excel;
const fs_ecd = { encoding: "utf-8" };
const fmn = hText.format.number;
const df = cafe.default;

const handleDataOrphans = async () => {
    /*********|first config|*********/
    const fileName = "./data/danh-sach-thieu-nhi-anh-huong-do-covid.xlsx";
    hFile.log("handle.orphans", "start");
    let data = await hExcel.read({ fileName });
    hFile.log("handle.orphans", "data", data.length);
    let temp = {
        citizens: {},
        wards: {},
    };
    for (let item of data) temp = handleDataItemOrphans({ item, temp });
    fs.writeJSONSync("./data/orphans/temp.json", temp, fs_ecd);
    let list = {
        citizens: [],
    };
    let keys = {
        citizens: Object.keys(temp.citizens),
    };
    for (let [c, CitizenName] of keys.citizens.entries()) {
        let obj = temp.citizens[CitizenName];
        const { BailoutList, SchoolAchievement, EconomicStatus, FamilySituation, Address } = obj;
        let CitizenID = fmn({ number: c, format: "000" });
        let Ward = parseInt(obj.Ward.replace("P", ""), 10);
        let cti = { CitizenID, CitizenName, Ward, SchoolAchievement, EconomicStatus, FamilySituation, Address, items: [] };
        // CitizenName
        // School: name, class, status
        // Economic: status
        // Family: godfather, situation
        for (let [t, txt] of BailoutList.entries()) {
            let child = {
                CreateMonth: `T${fmn({ number: t + 1, format: "00" })}`,
                BailoutSource: `Phường ${Ward}`,
                BailoutName: txt,
            };
            cti.items.push(child);
        };
        list.citizens.push(cti);
    };
    fs.writeJSONSync("./data/orphans/list.citizens.json", list.citizens, fs_ecd);
};

const handleDataItemOrphans = ({ item, temp }) => {
    /*********|first handle|*********/
    if (!item || !temp) return;
    const { STT, Ward, PhoneNumber, IDCard, FamilySituation, SchoolName, EconomicStatus } = item;
    let CitizenName = hText.validate.FullName(item.CitizenName);
    let obj = {
        citizen: temp.citizens[CitizenName],
        ward: temp.wards[Ward],
    };
    let oAdd = hText.validate.Address(item.Address);
    let oDOB = hText.validate.DateOfBirth({ STT, DateOfBirth: item.DateOfBirth });
    let oIDCardDate = hText.validate.DateOfBirth({ STT, DateOfBirth: item.IDCardCreateDate });
    let DateOfBirth = oDOB && oDOB.isValid() ? oDOB.format(cafe.format.date) : null;
    let IDCardCreateDate = oIDCardDate && oIDCardDate.isValid() ? oIDCardDate.format(cafe.format.date) : null;
    let { Address } = oAdd;
    let cti = {
        Ward, CitizenName, Address, PhoneNumber, DateOfBirth,
        FamilySituation, IDCardCreateDate, IDCard,
        EconomicStatus, BailoutList: [], SchoolAchievement: [],
    };


    /*********|handle data detail|*********/
    let BailoutList = item.BailoutList || "";
    let SchoolAchievement01 = item.SchoolAchievement01 || "";
    let SchoolAchievement02 = item.SchoolAchievement02 || "";
    if (SchoolName) cti.SchoolAchievement.push(SchoolName);
    let arr = {
        bailout: BailoutList.split("\r\n"),
        school01: SchoolAchievement01.split("\r\n"),
        school02: SchoolAchievement02.split("\r\n"),
    };
    for (let txt of arr.bailout) {
        txt = txt.trim().replace("- ", "").replace("/", "");
        if (!txt) continue;
        cti.BailoutList.push(txt);
    };
    for (let txt of arr.school01) {
        txt = txt.trim().replace("- ", "").replace("/", "");
        if (!txt) continue;
        cti.SchoolAchievement.push(txt);
    };
    for (let txt of arr.school02) {
        txt = txt.trim().replace("- ", "").replace("/", "");
        if (!txt) continue;
        cti.SchoolAchievement.push(txt);
    };
    if (!obj.citizen) obj.citizen = hText.cpo(cti);
    if (!obj.ward) obj.ward = { Ward, items: {} };
    let child = {
        ward: obj.ward.items[CitizenName],
    };
    if (!child.ward) child.ward = hText.cpo(cti);
    obj.ward.items[CitizenName] = child.ward;
    temp.wards[Ward] = obj.ward;
    temp.citizens[CitizenName] = obj.citizen;

    return temp;
};

module.exports = {
    data: {
        orphans: handleDataOrphans,
    },
};
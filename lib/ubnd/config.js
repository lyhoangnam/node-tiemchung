module.exports = {
    default: {
        columns: {
            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        },
        orphans: {
            ignore: {
                value: ["Không có", "Không", "Không ở trên địa bàn phường"],
            },
        },
    },
    filePath: {
        orphans: {
            dir: "./data/danh-sach-tre-mo-coi-do-covid",
            main: "main-data.xlsx",
            data: {
                all: "data.all.json",
                missing: "data.missing.json",
                alert: "data.alert.json",
                bailout: "data.bailout.json",
            },
        },
    },
    mix: {
        wards: {
            p02: "Phường 2",
            p03: "Phường 3",
            p04: "Phường 4",
            p06: "Phường 6",
            p08: "Phường 8",
            p09: "Phường 9",
            p10: "Phường 10",
            p13: "Phường 13",
            p14: "Phường 14",
            p15: "Phường 15",
            p16: "Phường 16",
            p18: "Phường 18",
        },
        months: {
            January: "2023-01-01",
            February: "2023-02-01",
            March: "2023-03-01",
            April: "2023-04-01",
            May: "2023-05-01",
            June: "2023-06-01",
            July: "2023-07-01",
            August: "2023-08-01",
            September: "2023-09-01",
            October: "2023-10-01",
            November: "2023-11-01",
            December: "2023-12-01",
        },
    },
    format: {
        date: "YYYY-MM-DD",
        dateTime: "YYYY-MM-DD HH:mm:ss",
    },
};
const fs = require("fs-extra");
const arraySort = require("array-sort");
const { api, helpers } = require("../../utils");
const cafe = require("./config");
const hFile = helpers.file;
const hText = helpers.text;
const hExcel = helpers.excel;
const fs_ecd = { encoding: "utf-8" };
const df = cafe.default;

const handleDataOrphans = async () => {
    /*********|first config|*********/
    hFile.log("abcd.orphans");
    const fp = cafe.filePath.orphans;
    const dir = fp.dir;
    const filePath = {
        main: `${dir}/${fp.main}`,
        data: {
            all: `${dir}/${fp.data.all}`,
            missing: `${dir}/${fp.data.missing}`,
            alert: `${dir}/${fp.data.alert}`,
            bailout: `${dir}/${fp.data.bailout}`,
        },
    };
    console.log("filePath", filePath);
    let data = {
        main: await hExcel.read({ fileName: filePath.main, sheetName: "Sheet1" }),
        in4: null,
        ward: null,
        district: null,
    };
    let list = {
        missing: [],
        alert: [],
        bailout: [],
    };
    let temp = {
        all: {},
        ward: {},
        bailout: {},
    };
    let keys = {
        all: [],
        ward: [],
        bailout: {
            source: [],
            items: [],
            citizens: [],
            list: [],
        },
    };

    /*********|refix data|*********/
    hFile.log("data", data.main.length);
    for (let item of data.main) {
        let obj = handleDataItemOrphans({ item, temp });
        temp.all[obj.all.key] = obj.all;
        temp.ward[obj.ward.key] = obj.ward;
    };
    // hFile.log("temp.ward", JSON.stringify(temp.ward));

    /*********|mixing data|*********/
    keys.ward = Object.keys(cafe.mix.wards);
    for (let from of keys.ward) {
        let to = cafe.mix.wards[from];
        let fileName = `${dir}/${from}.xlsx`;
        data.in4 = await hExcel.read({ fileName, sheetName: "in4" });
        data.ward = await hExcel.read({ fileName, sheetName: "ward" });
        data.district = await hExcel.read({ fileName, sheetName: "district" });

        /*********|update in4|*********/
        for (let item of data.in4) {
            item.Address = item.Address || `_ ${to}`;
            const { SchoolClass, SchoolStatus, FamilyStatus, EconomicStatus } = item;
            let FullName = hText.validate.FullName(item.FullName);
            if (item.Gender) item.Gender = item.Gender.trim().toLowerCase();
            let Gender = item.Gender === "nữ" ? 2 : 1;
            let key = {
                all: `${to}-${FullName}`,
            };
            let obj = {
                all: temp.all[key.all],
            };
            if (!obj.all) {
                hFile.log(`${to}.missing.in4`, key.all);
                obj = handleDataItemOrphans({ item, temp });
            };
            obj.all.Gender = Gender;
            obj.all.SchoolClass = SchoolClass;
            obj.all.SchoolStatus = SchoolStatus;
            obj.all.EconomicStatus = EconomicStatus;
            obj.all.FamilyStatus = FamilyStatus;
            temp.all[key.all] = obj.all;
        };

        /*********|update ward.support|*********/
        for (let item of data.ward) {
            let FullName = hText.validate.FullName(item.FullName);
            let key = {
                all: `${to}-${FullName}`,
            };
            let obj = {
                all: temp.all[key.all],
            };
            if (!obj.all) {
                hFile.log(`${to}.missing.ward`, key.all);
                continue;
            };
            for (let col of df.columns.months) {
                let value = item[col];
                let child = obj.all[col];
                if (!child) child = [];
                if (value) {
                    if (typeof (value) === "string")
                        value = hText.to.upper.firstLetter(value.trim())
                            .replace(/(\r\n|\n|\r)/gm, "|")
                            .replace(/( \| )|( \|)|(\| )/gm, "|")
                            .replace(/\.000/gm, ",000")
                            .replace(/\.500/gm, ",500")
                            .replace(/(;  được Phụ nữ)/gi, ", được phụ nữ");
                    if (!df.orphans.ignore.value.includes(value)) {
                        let CreateDate = cafe.mix.months[col];
                        child.push({ source: to, value, CreateDate });
                    };
                };
                obj.all[col] = child;
            };
            temp.all[key.all] = obj.all;
        };

        /*********|update district.support|*********/
        for (let item of data.district) {
            let FullName = hText.validate.FullName(item.FullName);
            let key = {
                all: `${to}-${FullName}`,
            };
            let obj = {
                all: temp.all[key.all],
            };
            if (!obj.all) {
                hFile.log(`${to}.missing.district`, key.all);
                item.Address = item.Address || `_ ${to}`;
                obj = handleDataItemOrphans({ item, temp });
            };
            for (let col of df.columns.months) {
                let value = item[col];
                let child = obj.all[col];
                if (!child) child = [];
                if (value) {
                    let CreateDate = cafe.mix.months[col];
                    if (typeof (value) === "string")
                        value = hText.to.upper.firstLetter(value.trim())
                            .replace(/(\r\n|\n|\r)/gm, "|")
                            // .replace(/1.000.000 báo kqđ/gi, "1,000,000đ báo kqđ")
                            .replace(/(1.000.000đ báo kqđ)|(1,000,000 báo kqđ)/gi, "1,000,000đ báo kqđ")
                            .replace(/kqđ/gi, "Khăn Quang Đỏ")
                            .replace(/500,000 quà, \|500,000 tiền mặt/gi, "500,000 quà|500,000 tiền mặt")
                            .replace(/1,000,000 công ty Amway/gi, "1,000,000đ công ty amway")
                            .replace(/amway/gi, "Amway");
                    child.push({ source: "quận", value, CreateDate });
                };
                obj.all[col] = child;
            };
            temp.all[key.all] = obj.all;
        };
    };

    /*********|recheck data|*********/
    keys.all = Object.keys(temp.all);
    hFile.log("insert into @temp(CitizenID,CitizenName,Ward,Gender,[Address],School,Family,Economic,DateOfBirth,PhoneNumber,CreateDate,CreateBy) values");
    for (let [k, key] of keys.all.entries()) {
        let obj = {
            all: temp.all[key],
            bailout: null,
        };
        let sum = [];
        const { FullName, Ward, Address, Gender, DateOfBirth, PhoneNumber } = obj.all;
        let CitizenID = `CTT${hText.format.number({ number: k + 1, format: "000000000" })}`;
        let CitizenName = FullName;
        let School = null;
        let Family = null;
        let Economic = null;
        if (obj.all.School) School = JSON.stringify({ name: obj.all.School, class: obj.all.SchoolClass, status: obj.all.SchoolStatus });
        if (obj.all.FamilySituation) Family = JSON.stringify({ situation: obj.all.FamilySituation, godfather: obj.all.FamilyStatus });
        if (obj.all.EconomicStatus) Economic = JSON.stringify({ status: obj.all.EconomicStatus });
        let insert = [
            `'${CitizenID}'`,
            `N'${CitizenName}'`,
            Ward,
            `'${Gender}'`,
            `N'${Address}'`,
            `N'${School}'`.replace(/(\\r\\n|\\n|\\r)/gm, ""),
            `N'${Family}'`.replace(/(\\r\\n|\\n|\\r)/gm, ""),
            `N'${Economic}'`.replace(/(\\r\\n|\\n|\\r)/gm, ""),
            `'${DateOfBirth}'`,
            `N'${PhoneNumber}'`,
            "GETDATE()",
            "'System'",
        ];
        let text = hText.to.insert(`(${insert.join(",")}),`);
        // hFile.log(text);
        for (let col of df.columns.months) {
            let arr = obj.all[col];
            if (!arr) {
                if (!list.missing.includes(key)) list.missing.push(key);
                arr = []
                obj.all[col] = arr;
            };
            sum = [...sum, ...arr];
        };
        if (sum.length <= 0) list.alert.push(key);
        for (let item of sum) {
            const { CreateDate, value } = item;
            obj.bailout = temp.bailout[item.source];
            if (!obj.bailout) obj.bailout = {};
            let child = obj.bailout[value];
            if (!child) child = { source: item.source, value, citizens: {} };
            // let key = parseInt(item.source.replace(/quận/gi, "9999").replace(/phường/gi, ""), 10);
            // key = `${hText.format.number({ number: key, format: "0000" })}-${CreateDate}-${value}`;
            let cti = child.citizens[CitizenID];
            if (!cti) cti = { items: [] };
            if (!cti.items.includes(CreateDate)) cti.items.push(CreateDate);
            child.citizens[CitizenID] = cti;
            //     if (!child.citizens.includes(CitizenID)) {
            //         child.citizens.push(CitizenID);
            //         child.items.push({ CreateDate, CitizenID });
            //     };
            obj.bailout[value] = child;
            temp.bailout[item.source] = obj.bailout;
        };
        temp.all[key] = obj.all;
    };

    /*********|insert data|*********/
    hFile.log("insert into @temp(BailoutID,BailoutName,BailoutSource,CitizenID,CreateDate,CreateBy) values");
    keys.bailout.source = Object.keys(temp.bailout);
    for (let BailoutSource of keys.bailout.source) {
        let obj = temp.bailout[BailoutSource];
        let key = parseInt(BailoutSource.replace(/quận/gi, "9999").replace(/phường/gi, ""), 10);
        let items = [];
        keys.bailout.items = Object.keys(obj);
        for (let BailoutName of keys.bailout.items) {
            let child = {
                origin: obj[BailoutName],
                update: { BailoutName, items: [] },
            };
            keys.bailout.citizens = Object.keys(child.origin.citizens).sort();
            for (let CitizenID of keys.bailout.citizens) {
                let grandchild = {
                    origin: child.origin.citizens[CitizenID],
                    update: { items: [] },
                };
                grandchild.origin.items.sort();
                child.update.items.push({ CitizenID, items: grandchild.origin.items });
            };
            items.push(child.update);
        };
        items = arraySort(items, ["CreateDate", "value"]);
        list.bailout.push({ key, source: BailoutSource, items });
    };
    list.bailout = arraySort(list.bailout, ["key"]);
    // hFile.log("list.bailout", JSON.stringify(list.bailout));

    for (let item of list.bailout) {
        const BailoutSource = item.source;
        for (let obj of item.items) {
            const { BailoutName } = obj;
            for (let child of obj.items) {
                const { CitizenID } = child;
                for (let CreateDate of child.items) {
                    let key = `${item.key}-${CreateDate}-${BailoutName}`;
                    if (!keys.bailout.list.includes(key)) keys.bailout.list.push(key);
                    const BailoutID = `BAILOUT${hText.format.number({ number: keys.bailout.list.length, format: "000000000" })}`;
                    let arr = [
                        `'${BailoutID}'`,
                        `N'${BailoutName}'`,
                        `N'${BailoutSource}'`,
                        `'${CitizenID}'`,
                        `'${CreateDate}'`,
                        `'System'`
                    ];
                    let text = `(${arr.join(",")}),`;
                    hFile.log(text);
                };
            };
        };
    };
    // hFile.log("keys.bailout.list", JSON.stringify(keys.bailout.list));


    /*********|save data|*********/
    list.alert.sort();
    fs.writeJSONSync(filePath.data.all, temp.all, fs_ecd);
    fs.writeJSONSync(filePath.data.missing, list.missing, fs_ecd);// chưa có liệt kê vào danh sách chăm sóc
    fs.writeJSONSync(filePath.data.alert, list.alert, fs_ecd);// các bé chưa nhận được bất kỳ phần quà nào
    fs.writeJSONSync(filePath.data.bailout, temp.bailout, fs_ecd);
};

const handleDataItemOrphans = ({ item, temp }) => {
    let obj = { all: { key: null }, ward: { key: null } };
    if (!item) return obj;
    const { STT, Address, PhoneNumber, School, FamilySituation } = item;
    let FullName = hText.validate.FullName(item.FullName);
    let oAdd = hText.validate.Address(Address);
    let oDOB = hText.validate.DateOfBirth({ STT, DateOfBirth: item.DateOfBirth });
    let DateOfBirth = oDOB && oDOB.isValid() ? oDOB.format(cafe.format.date) : null;
    let arr = {
        ward: ["error.ward", STT, FullName, item.Address],
    };
    if (!oAdd.Ward) {
        hFile.log(arr.ward.join("\t"));
        return obj;
    };
    let key = {
        all: `${oAdd.Ward}-${FullName}`,
        ward: oAdd.Ward,
    };
    obj.all = temp.all[key.all];
    obj.ward = temp.ward[key.ward];
    let Ward = parseInt(oAdd.Ward.replace(/phường/gi, ""), 10);
    let child = {
        all: { STT, FullName, DateOfBirth, Ward, Address: oAdd.Address, PhoneNumber, School, FamilySituation },
        ward: { STT, FullName, DateOfBirth, Ward, Address: oAdd.Address },
    };
    if (!obj.all) obj.all = Object.assign({ key: key.all }, child.all);
    if (!obj.ward) obj.ward = { key: key.ward, Ward: oAdd.Ward, items: [] };
    obj.ward.items.push(child.ward);

    return obj;
};

module.exports = {
    data: {
        orphans: handleDataOrphans,
    },
};
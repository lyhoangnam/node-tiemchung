const moment = require("moment");
const { api, helpers } = require("../../utils");
const hFile = helpers.file;
const retailer = "xdgk";
const oa = "xdgk";
const cafe = {
    default: {
        to: {
            user_id: ["3951996588939845795"],
        },
    },
    format: {
        dateTime: "YYYY-MM-DD hh:mm:ss A",
    },
    sms: {
        invalid_inputs: api.sms.invalid_inputs,
        contact_admin: api.sms.contact_admin,
    },
};
const df = cafe.default;

const handleEventsKiot = async ({ event }) => {
    if (!event || !Array.isArray(event.Notifications) || !event.Notifications.length) return { error: cafe.sms.invalid_inputs };
    let [item] = event.Notifications;
    if (!item || !item.Action || !Array.isArray(item.Data) || !item.Data.length) return { error: cafe.sms.invalid_inputs };
    let arr = item.Action.split(".").slice(0, -1);
    let action = arr.join(".");
    switch (action) {
        case "order.update":
            handleOrderUpdate({ item: item.Data[0] });
            break;
        default:
            hFile.log("xdgk.webhook.events.kiot.action.notfound", action);
            break;
    };
};

const handleOrderUpdate = async ({ item }) => {
    if (!item || !item.Code) return { error: cafe.sms.invalid_inputs };
    if (item.Status !== 1) return hFile.log("xdgk.order.update", `${item.Code}.${item.StatusValue}`);
    const { CustomerId } = item;
    const order_code = item.Code;
    const order_createDate = moment(item.CreatedDate).format(cafe.format.dateTime);
    console.log("load.customer.detail", CustomerId);
    let filter = {
        csd: { id: CustomerId },
        message: null,
    };
    let cus = await api.kiot.data.load.customers.detail({ retailer, filter: filter.csd });
    if (!cus || !cus.code) return { error: cafe.sms.contact_admin };
    const display_name = cus.name;
    let text = `${order_code} | ${display_name} | ${cus.contactNumber} | ${cus.address} | ${order_createDate}`;
    filter.message = {
        recipient: { user_id: df.to.user_id[0] },
        message: { text },
    };
    console.log("filter", filter.message);
    let res_message = await api.zalo.data.message.text({ oa, filter: filter.message });
    console.log("res_message", res_message);
};

module.exports = {
    handleEvents: {
        kiot: handleEventsKiot,
    },
};
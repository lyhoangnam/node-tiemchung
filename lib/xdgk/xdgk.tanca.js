const { api, helpers } = require("../../utils");
const hFile = helpers.file;
const oa = "xdgk";

const loadTasksProjectsList = async () => {
    let res_projects = await api.tanca.data.load.tasks.projects.list({ oa });
    if (res_projects.error_code) return res_projects;
    let { items } = res_projects.data;
    hFile.log("items", JSON.stringify(items));
};

module.exports = {
    load: {
        tasks: {
            projects: {
                list: loadTasksProjectsList,
            },
        },
    },
};
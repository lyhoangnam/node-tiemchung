module.exports = {
    kiot: require("./xdgk.kiot"),
    zalo: require("./xdgk.zalo"),
    tanca: require("./xdgk.tanca"),
    webhook: require("./xdgk.webhook"),
};
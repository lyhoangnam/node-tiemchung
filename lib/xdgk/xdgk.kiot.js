const fs = require("fs-extra");
const moment = require("moment");
const arraySort = require("array-sort");
const { api, helpers } = require("../../utils");
const { numberStyle } = helpers.excel;
const hFile = helpers.file;
const hText = helpers.text;
const hTimer = helpers.timer;
const pageSize = 100;
const retailer = "xdgk";
const cafe = {
    default: {
        filter: {
            categories: { pageSize, hierachicalData: false },
            products: { pageSize, includeInventory: true, includeQuantity: true, isActive: true },
            stock: { pageSize, lastModifiedFrom: "2020/01/01" },
            purchaseorders: { pageSize },
            transfers: { pageSize },
        },
    },
    filePath: {
        save: {
            categories: "./data/kiot/kiot.xdgk.categories.json",
            products: "./data/kiot/kiot.xdgk.products.json",
            productsDrawData: "./data/kiot/kiot.xdgk.products.drawData.json",
            stock: "./data/kiot/kiot.xdgk.stock.json",
            stockDrawData: "./data/kiot/kiot.xdgk.stock.drawData.json",
            purchaseorders: "./data/kiot/kiot.xdgk.purchaseorders.json",
            purchaseordersDrawData: "./data/kiot/kiot.xdgk.purchaseorders.drawData.json",
            transfers: "./data/kiot/kiot.xdgk.transfers.json",
            transfersDrawData: "./data/kiot/kiot.xdgk.transfers.drawData.json",
        },
    },
    timer: {
        wait: { from: 500, to: 2000 },
    },
    format: {
        dateTime: "YYYY-MM-DD hh:mm:ss A",
    },
    sms: {
        contact_admin: api.sms.contact_admin,
    },
};
const df = cafe.default;
const isLocal = 1;

const loadBacklinks = async ({ folderPath = "./data/ahrefs/backlinks/xedapgiakho.blogspot.com" }) => {
    let files = await hFile.loadDir({ folderPath });
    if (!Array.isArray(files)) return;
    let temp = {
        from: {},
    };
    for (let item of files) {
        let fileName = `${folderPath}/${item.name}`;
        let [_, { rows }] = fs.readJSONSync(fileName);
        for (let row of rows) {
            let from = row[1];
            let to = row[27];
            // console.log("from", from);
            let host = hText.getHost(from);
            let obj = temp.from[host] || { from: [], to: [] };
            if (!obj.from.includes(from)) obj.from.push(from);
            if (!obj.to.includes(to)) obj.to.push(to);
            temp.from[host] = obj;
        };
    };
    hFile.log(temp.from);
};

const loadKiotCategories = async () => {
    let filter = df.filter.categories;
    const res_categories = await api.kiot.data.load.categories({ retailer, filter });
    if (!res_categories || !Array.isArray(res_categories.data)) return { error: cafe.sms.contact_admin };
    let list = [];
    let temp = {};
    for (let item of res_categories.data) {
        const { categoryId } = item;
        list.push(item);
        temp[categoryId] = item;
    };
    let data = { list, obj: temp };
    fs.writeJSONSync(cafe.filePath.save.categories, data, { encoding: "utf-8" });
    console.log("done");
    return { is_success: true, data };
};

const loadKiotProducts = async ({ currentItem = 0, total = 0, data = [] }) => {
    if (isLocal) {
        data = fs.readJSONSync(cafe.filePath.save.productsDrawData);
        // handleDataProduct({ data });
        console.log("products.all", data.length);
        let list = [];
        for (let item of data) {
            let { code } = item;
            // let text = code.replace(/[0-9]/g, "");
            // let t = text.length;
            // let n = code.length - t;
            // let x = t + n;
            let x = `${item.id}`.length;
            if (x <= 12) continue;
            let obj = Object.assign({ x }, item);
            list.push(obj);
        };
        console.log("products.code", list.length);
        for (let item of list) {
            hFile.log(`${item.code}\t${item.x}\t${item.fullName}`);
        };
    } else {
        const timer = cafe.timer.wait;
        let filter = Object.assign({ currentItem }, df.filter.products);
        let x = hText.random.number(timer.from, timer.to);
        if (currentItem <= 0) delete filter.currentItem;
        else if (currentItem < total) await hTimer.wait.promise(x);
        console.log("loadKiot.products", `${currentItem}.${total}`);
        const res_products = await api.kiot.data.load.products({ retailer, filter });
        if (!res_products || !Array.isArray(res_products.data)) return { error: cafe.sms.contact_admin };
        if (currentItem <= 0) total = res_products.total;
        data = [...data, ...res_products.data];
        currentItem += pageSize;
        if (currentItem < total) return loadKiotProducts({ currentItem, total, data });
        handleDataProduct({ data });
        console.log("done");
        return { is_success: true, data };
    };
};

const loadKiotStock = async ({ currentItem = 0, data = [] }) => {
    if (isLocal) {
        data = fs.readJSONSync(cafe.filePath.save.stockDrawData);
        handleDataStock({ data });
    } else {
        const timer = cafe.timer.wait;
        let filter = Object.assign({ currentItem }, df.filter.stock);
        let total = 2618;
        let x = hText.random.number(timer.from, timer.to);
        if (currentItem <= 0) delete filter.currentItem;
        else if (currentItem < total) await hTimer.wait.promise(x);
        console.log("loadKiot.stock", JSON.stringify(filter));
        const res_stock = await api.kiot.data.load.stock({ retailer, filter });
        if (!res_stock || !Array.isArray(res_stock.data)) return { error: cafe.sms.contact_admin };
        data = [...data, ...res_stock.data];
        currentItem += pageSize;
        if (currentItem < total) return loadKiotStock({ currentItem, data });
        handleDataStock({ data });
        console.log("done");
        return { is_success: true, data };
    };
};

const loadKiotPurchaseorders = async ({ currentItem = 0, total = 0, data = [] }) => {
    if (isLocal) {
        data = fs.readJSONSync(cafe.filePath.save.purchaseordersDrawData);
        handleDataPurchaseOrders({ data });
    } else {
        const timer = cafe.timer.wait;
        let filter = Object.assign({ currentItem }, df.filter.purchaseorders);
        let x = hText.random.number(timer.from, timer.to);
        if (currentItem <= 0) delete filter.currentItem;
        else if (currentItem < total) await hTimer.wait.promise(x);
        console.log("loadKiot.purchaseorders", `${currentItem}.${total}`);
        const res_po = await api.kiot.data.load.purchaseorders({ retailer, filter });
        if (!res_po || !Array.isArray(res_po.data)) return { error: cafe.sms.contact_admin };
        if (currentItem <= 0) total = res_po.total;
        data = [...data, ...res_po.data];
        currentItem += pageSize;
        if (currentItem < total) return loadKiotPurchaseorders({ currentItem, total, data });
        handleDataPurchaseOrders({ data });
        console.log("done");
        return { is_success: true, data };
    };
};

const loadKiotTransfers = async ({ currentItem = 0, total = 0, data = [] }) => {
    if (isLocal) {
        data = fs.readJSONSync(cafe.filePath.save.transfers);
        handleDataTransfers({ data });
    } else {
        const timer = cafe.timer.wait;
        let filter = Object.assign({ currentItem }, df.filter.transfers);
        let x = hText.random.number(timer.from, timer.to);
        if (currentItem <= 0) delete filter.currentItem;
        else if (currentItem < total) await hTimer.wait.promise(x);
        console.log("loadKiot.transfers", `${currentItem}.${total}`);
        const res_transfers = await api.kiot.data.load.transfers({ retailer, filter });
        if (!res_transfers || !Array.isArray(res_transfers.data)) return { error: cafe.sms.contact_admin };
        if (currentItem <= 0) total = res_transfers.total;
        data = [...data, ...res_transfers.data];
        currentItem += pageSize;
        if (currentItem < total) return loadKiotTransfers({ currentItem, total, data });
        handleDataTransfers({ data });
        console.log("done");
        return { is_success: true, data };
    };
};

const handleDataProduct = ({ data = [] }) => {
    console.log("data", data.length);
    fs.writeJSONSync(cafe.filePath.save.productsDrawData, data, { encoding: "utf-8" });
    /*
        Giá vốn hiện tại trên Kiot là chỉ tính giá vốn cho lần nhập đầu tiên thôi
        Giá vốn nên tính là giá trung bình cho tổng các lần nhập kho
        VD: Xe Đạp Cho Bé 14 Inch XAMING Baga - xanh

        product.type=1: hàng hóa dạng combo
        product.type=2: hàng hóa còn lại
        product.type=3: hàng hóa dịch vụ        
    */
    let list = [];
    let temp = {};
    for (let item of data) {
        let { id, inventories } = item;
        let inventory = { total: 0, onHand: 0, list: [] };
        inventories = inventories || [];
        let cost0 = 0;
        let inventoryCost = 0;
        for (let iv of inventories) {
            //|| iv.branchId === 38148
            if (iv.onHand <= 0) continue;
            if (!inventory.list.length) cost0 = iv.cost;
            inventory.total += iv.cost;
            inventory.onHand += iv.onHand;
            inventory.list.push(iv.cost);
        };
        if (inventory.onHand) inventoryCost = inventory.total / inventory.list.length;
        if (inventoryCost <= 0) inventoryCost = cost0;
        item.inventoryCost = inventoryCost;
        item.inventoryOnHand = inventory.onHand;
        if (inventoryCost !== cost0) console.log(`inventory.${cost0}.${inventoryCost}.(${~~(cost0 - inventoryCost)})`, item.fullName);
        list.push(item);
        temp[id] = item;
    };
    arraySort(list, ["inventoryOnHand"], { reverse: true });
    // console.log("list", list.length);
    fs.writeJSONSync(cafe.filePath.save.products, { list, obj: temp }, { encoding: "utf-8" });
};

const handleDataStock = ({ data = [] }) => {
    console.log("data", data.length);
    let products = fs.readJSONSync(cafe.filePath.save.products, { encoding: "utf-8" }).obj;
    let list = [];
    let temp = {};
    for (let item of data) {
        let { id, code, inventories } = item;
        let onHand = 0;
        inventories = inventories || [];
        for (let iv of inventories) onHand += iv.onHand;
        let prd = products[id];
        if (!onHand || !prd || !prd.inventory.onHand) continue;
        // || [258310, 1000000506].includes(prd.categoryId)
        const { name, fullName, basePrice } = prd;
        let modifiedDate = moment(item.modifiedDate);
        let stock = temp[id];
        if (!stock) stock = { name, fullName, basePrice, code, modifiedDate: modifiedDate.format(cafe.format.dateTime), onHand: 0 };
        else console.log("stock", stock);
        stock.diff = moment().diff(modifiedDate, "days");
        stock.onHand = prd.inventory.onHand;
        stock.rate = basePrice * stock.onHand * stock.diff;
        temp[id] = stock;
    };
    let keys = Object.keys(temp);
    for (let id of keys) {
        let stock = temp[id];
        let obj = Object.assign({ id }, stock);
        list.push(obj);
    };
    arraySort(list, ["rate"], { reverse: true });
    console.log("list", list.length);
    fs.writeJSONSync(cafe.filePath.save.stock, { list, obj: temp }, { encoding: "utf-8" });
};

const handleDataPurchaseOrders = ({ data = [] }) => {
    console.log("data", data.length);
    fs.writeJSONSync(cafe.filePath.save.purchaseordersDrawData, data, { encoding: "utf-8" });

    let list = [];
    let temp = fs.readJSONSync(cafe.filePath.save.products, { encoding: "utf-8" }).obj;
    for (let item of data) {
        /*
            status=1 là phiếu tạm
            status=3 là nhập hoàn tất
            status=4 là hủy phiếu nhập hàng
        */
        let { purchaseDate, purchaseOrderDetails } = item;
        purchaseOrderDetails = purchaseOrderDetails || [];
        let a = moment(purchaseDate);
        for (let child of purchaseOrderDetails) {
            const { productId } = child;
            let prd = temp[productId];
            if (!prd) continue;
            if (!prd.inventoryPurchaseDate) prd.inventoryPurchaseDate = purchaseDate;
            let b = moment(prd.inventoryPurchaseDate);
            let diff = a.diff(b, "days");
            if (diff > 0) prd.inventoryPurchaseDate = purchaseDate;
            let h = moment().diff(moment(prd.inventoryPurchaseDate), "hours");
            prd.inventoryDiff = moment().diff(moment(prd.inventoryPurchaseDate), "days");
            prd.inventoryHumanize = moment.duration(h, "hours").humanize();
            temp[productId] = prd;
        };
    };
    let keys = Object.keys(temp);
    for (let id of keys) {
        let prd = temp[id];
        if (!prd.inventoryOnHand) continue;
        let rateYear = prd.inventoryCost * 5 / 100;// lãi xuất 5% trong 12 tháng
        let rateDate = rateYear / (12 * 30);
        prd.inventoryRate = rateDate * prd.inventoryDiff;
        let obj = Object.assign({ id }, prd);
        list.push(obj);
    };
    arraySort(list, ["inventoryDiff"], { reverse: true });
    console.log("list", list.length);
    fs.writeJSONSync(cafe.filePath.save.products, { list, obj: temp }, { encoding: "utf-8" });
};

const handleDataTransfers = ({ data = [] }) => {
    console.log("data", data.length);
    // fs.writeJSONSync(cafe.filePath.save.transfers, data, { encoding: "utf-8" });

    let list = [];
    let temp = fs.readJSONSync(cafe.filePath.save.products, { encoding: "utf-8" }).obj;
    for (let item of data) {
        /*
            status=1 là phiếu tạm
            status=3 là nhập hoàn tất
            status=4 là hủy phiếu nhập hàng
        */
        let { receivedDate, transferDetails } = item;
        transferDetails = transferDetails || [];
        let a = moment(receivedDate);
        for (let child of transferDetails) {
            const { productId } = child;
            let prd = temp[productId];
            if (!prd || prd.inventoryPurchaseDate) continue;
            prd.inventoryPurchaseDate = receivedDate;
            let b = moment(prd.inventoryPurchaseDate);
            let diff = a.diff(b, "days");
            if (diff > 0) prd.inventoryPurchaseDate = receivedDate;
            let h = moment().diff(moment(prd.inventoryPurchaseDate), "hours");
            prd.inventoryDiff = moment().diff(moment(prd.inventoryPurchaseDate), "days");
            prd.inventoryHumanize = moment.duration(h, "hours").humanize();
            temp[productId] = prd;
        };
    };
    let keys = Object.keys(temp);
    for (let id of keys) {
        let prd = temp[id];
        if (!prd.inventoryOnHand) continue;
        let rateYear = prd.inventoryCost * 5 / 100;// lãi xuất 5% trong 12 tháng
        let rateDate = rateYear / (12 * 30);
        prd.inventoryRate = rateDate * prd.inventoryDiff;
        let obj = Object.assign({ id }, prd);
        list.push(obj);
    };
    arraySort(list, ["inventoryDiff"], { reverse: true });
    console.log("list", list.length);
    fs.writeJSONSync(cafe.filePath.save.products, { list, obj: temp }, { encoding: "utf-8" });
};

const exportStock = () => {
    let data = fs.readJSONSync(cafe.filePath.save.products);
    let rows = [];
    for (let [index, item] of data.list.entries()) {
        let {
            id, code, fullName, categoryName, basePrice,
            inventoryDiff, inventoryHumanize, inventoryRate,
            inventoryPurchaseDate, inventoryOnHand, inventoryCost,
        } = item;
        let STT = index + 1;
        let cost = basePrice * inventoryOnHand;
        inventoryPurchaseDate = moment(inventoryPurchaseDate).format(cafe.format.dateTime);
        let obj = {
            STT, id, code, fullName, categoryName, basePrice, cost,
            inventoryDiff, inventoryHumanize, inventoryRate,
            inventoryPurchaseDate, inventoryCost, inventoryOnHand,
        };
        rows.push(obj);
    };
    console.log("rows", rows.length);
    let x = 0;
    let worksheet = {
        name: "file-data.xlsx",
        views: [{ state: "frozen", xSplit: 1, ySplit: 1, activeCell: "A1" }],
    };
    let sheet01 = {
        sheetName: "xdgk.stock",
        columns: [
            { header: "STT", key: "STT", style: numberStyle },
            { header: "Mã id", key: "id", width: 12 },
            { header: "Mã code", key: "code", width: 25 },
            { header: "Tên sản phẩm", key: "fullName", width: 40 },
            { header: "Nhóm hàng", key: "categoryName", width: 25 },
            { header: "Ngày nhập kho gần nhất", key: "inventoryPurchaseDate", width: 25 },
            { header: "Ngày tồn(số)", key: "inventoryDiff", style: numberStyle, width: 12 },
            { header: "Ngày tồn(chữ)", key: "inventoryHumanize", width: 12 },
            { header: "Đơn giá bán", key: "basePrice", style: numberStyle, width: 20 },
            { header: "Số lượng tồn", key: "inventoryOnHand", style: numberStyle, width: 12 },
            { header: "Tổng giá trị", key: "cost", style: numberStyle, width: 20 },
            { header: "Rate", key: "inventoryRate", style: numberStyle, width: 20 },
        ],
        config: { worksheet },
        rows,
    };
    let filter = { x, fileName: "xdgk.stock", data: [sheet01] };
    helpers.excel.export(filter);
};

module.exports = {
    loadBacklinks,
    exportStock,
    load: {
        categories: loadKiotCategories,
        products: loadKiotProducts,
        stock: loadKiotStock,
        purchaseorders: loadKiotPurchaseorders,
        transfers: loadKiotTransfers,
    },
};
const fs = require("fs-extra");
const moment = require("moment");
const arraySort = require("array-sort");
const { api, helpers } = require("../../utils");
const hText = helpers.text;
const hTimer = helpers.timer;
const oa = "xdgk";
const pageSize = 50;
const cafe = {
    default: {
        filter: {
            getfollowers: {
                count: pageSize
            },
        },
    },
    filePath: {
        save: {
            followersDrawData: "./data/zalo/zalo.xdgk.followers.drawData.json",
            profilesDrawData: "./data/zalo/zalo.xdgk.profiles.drawData.json",
        },
    },
    timer: {
        wait: { from: 500, to: 1000 },
    },
    sms: {
        invalid_inputs: api.sms.invalid_inputs,
        contact_admin: api.sms.contact_admin,
    },
};
const df = cafe.default;
const isLocal = 1;

const loadToken = async () => {
    let access_token = await api.zalo.data.load.token({ oa });
    console.log("access_token", access_token);
};

const loadFollowersList = async ({ offset = 0, total = 0, data = [] }) => {
    if (isLocal) {
        // Thư Đặng     3485730316551758116
        // Tony Tèo     3951996588939845795
        data = fs.readJSONSync(cafe.filePath.save.followersDrawData);
        handleDataFollowersList({ data });
    } else {
        const timer = cafe.timer.wait;
        let filter = { data: JSON.stringify(Object.assign({ offset }, df.filter.getfollowers)) };
        let x = hText.random.number(timer.from, timer.to);
        if (offset > 0 && offset < total) await hTimer.wait.promise(x);
        console.log("loadZalo.followers", `${offset}.${total}`);
        let res_followers = await api.zalo.data.load.followers({ oa, filter });
        if (!res_followers || res_followers.message !== "Success") return { error: cafe.sms.contact_admin };
        let res = res_followers.data;
        if (offset <= 0) total = res.total;
        data = [...data, ...res.followers];
        offset += pageSize;
        if (offset < total) return loadFollowersList({ offset, total, data });
        handleDataFollowersList({ data });
        console.log("done");
        return { is_success: true, data };
    };
};

const loadFollowersDetail = async ({ user_id }) => {
    if (!user_id) return ({ error: cafe.sms.invalid_inputs });
    console.log("loadZalo.followers.detail", user_id);
    let filter = { data: JSON.stringify({ user_id }) };
    let res_profile = await api.zalo.data.load.profile({ oa, filter });
    if (!res_profile || res_profile.message !== "Success") return { error: cafe.sms.contact_admin };
    let item = res_profile.data;

    return { is_success: true, data: item };
};

const handleDataFollowersList = async ({ data = [] }) => {
    console.log("data", data.length);
    fs.writeJSONSync(cafe.filePath.save.followersDrawData, data, { encoding: "utf-8" });
    const timer = cafe.timer.wait;
    let list = [];
    for (let item of data) {
        const { user_id } = item;
        let x = hText.random.number(timer.from, timer.to);
        if (list.length) await hTimer.wait.promise(x);
        let res = await loadFollowersDetail({ user_id });
        if (res.error) continue;
        list.push(res.data);
    };
    handleDataFollowersDetail({ data: list });
};

const handleDataFollowersDetail = async ({ data = [] }) => {
    console.log("data", data.length);
    fs.writeJSONSync(cafe.filePath.save.profilesDrawData, data, { encoding: "utf-8" });
};

module.exports = {
    load: {
        token: loadToken,
        followers: {
            list: loadFollowersList,
        },
    },
};
const path = require("path");
const fs = require("fs-extra");
const piexif = require("piexifjs");
const canvas = require("canvas");
// const exiftool = require("node-exiftool")
// const exiftoolBin = require("dist-exiftool")
// const ep = new exiftool.ExiftoolProcess(exiftoolBin)
// const getExif = require("get-exif");
// const modifyExif = require("modify-exif");
const { helpers } = require("../../utils");
const hText = helpers.text;
const cafe = {
    upload: {
        LOGO_MARGIN_PERCENTAGE: 20,
    },
    default: {
        ffmpeg: {
            path: "C:/ffmpeg/bin/ffmpeg.exe",
        },
        GPS: {
            "1": "N",
            "2": [],
            "3": "E",
            "4": [],
            "5": 0,
            "6": [[0, 1], [0, 1], [0, 100]]
        },
        resizes: [
            { width: 1536, height: 1020 },
            { width: 1024, height: 680 },
            { width: 768, height: 510 },
            { width: 600, height: 398 },
            { width: 300, height: 225 },
            { width: 300, height: 199 },
            { width: 150, height: 150 },
            { width: 100, height: 100 },
            { width: 50, height: 50 },
            { width: 48, height: 32 },
            { width: 36, height: 24 },
            { width: 24, height: 16 },
        ],
    },
};
const df = cafe.default;

const write = async ({ fileName = "", fileSaveName = "", options = {}, logo = {} }) => {
    let extname = path.extname(fileSaveName);
    let filePath = path.dirname(fileSaveName);
    let basename = path.basename(fileSaveName, extname);
    const img = {
        main: await canvas.loadImage(fileName),
        logo: await canvas.loadImage(logo.src),
    };
    let size = {
        fileName: fileSaveName,
        main: { width: img.main.width, height: img.main.height },
        logo: { width: img.main.width * logo.resize / 100, height: 0 },
        position: { x: 0, y: 0 },
    };
    let ipcLogo = size.logo.width / img.logo.width;
    size.logo.height = img.logo.height * ipcLogo;
    size.position.x = size.main.width * logo.margin / 100 - size.logo.width / 2;
    size.position.y = size.main.height * logo.margin / 100 - size.logo.height / 2;
    let sizeList = [size];
    if (options.resize) {
        for (let iSize of df.resizes) {
            let ipcMain = iSize.width / img.main.width;
            let iMain = { width: iSize.width, height: img.main.height * ipcMain };
            let iLogo = { width: iSize.width * logo.resize / 100, height: 0 };
            ipcLogo = iLogo.width / img.logo.width;
            iLogo.height = img.logo.height * ipcLogo;
            let x = iMain.width * logo.margin / 100 - iLogo.width / 2;
            let y = iMain.height * logo.margin / 100 - iLogo.height / 2;
            let obj = {
                main: iMain,
                logo: iLogo,
                position: { x, y },
                fileName: `${filePath}/${basename}-${iSize.width}x${iSize.height}.jpg`,
            };
            sizeList.push(obj);
        };
    };
    for (let [i, iSize] of sizeList.entries()) {
        // console.log(i, iSize);
        const cv = canvas.createCanvas(iSize.main.width, iSize.main.height);
        let context = cv.getContext("2d");
        context.drawImage(img.main, 0, 0, iSize.main.width, iSize.main.height);//dx,dy,width,height
        context.globalAlpha = logo.opacity / 100;
        context.drawImage(img.logo, iSize.position.x, iSize.position.y, iSize.logo.width, iSize.logo.height);
        let dataURL = cv.toDataURL("image/jpeg");
        let jpegData = hText.to.buffer(dataURL);
        let jpegBinary = jpegData.toString("binary");
        // fs.writeFileSync(iSize.fileName, jpegData);

        let oExif = piexif.load(jpegBinary);
        let exifObj = Object.assign({}, oExif);
        const optd = options.data;
        const ids = {
            DateTimeOriginal: piexif.ExifIFD.DateTimeOriginal,// 36867
            DateTimeDigitized: piexif.ExifIFD.DateTimeDigitized,// 36868
            Rating: piexif.ImageIFD.Rating,// 18246
            RatingPercent: piexif.ImageIFD.RatingPercent,// 33432
            Copyright: piexif.ImageIFD.Copyright,// 33432
            XPTitle: piexif.ImageIFD.XPTitle,// 40091
            XPComment: piexif.ImageIFD.XPComment,// 40092
            XPAuthor: piexif.ImageIFD.XPAuthor,// 40093
            XPKeywords: piexif.ImageIFD.XPKeywords,// 40094
            XPSubject: piexif.ImageIFD.XPSubject,// 40095
        };
        let latitude = piexif.GPSHelper.degToDmsRational(optd.latitudeDegrees);
        let longitude = piexif.GPSHelper.degToDmsRational(optd.longitudeDegrees);
        let uGPS = {
            "1": df.GPS["1"],
            "2": latitude,
            "3": df.GPS["3"],
            "4": longitude,
            "5": 0,
            "6": df.GPS["6"]
        };
        exifObj["GPS"] = uGPS;
        exifObj["Exif"][ids.DateTimeOriginal] = optd.DateTimeOriginal;
        exifObj["Exif"][ids.DateTimeDigitized] = optd.DateTimeDigitized;
        exifObj["0th"][ids.Rating] = optd.Rating;
        exifObj["0th"][ids.Copyright] = optd.Copyright;
        exifObj["0th"][ids.XPTitle] = [...Buffer.from(optd.XPTitle, "ucs2")];
        exifObj["0th"][ids.XPComment] = [...Buffer.from(optd.XPComment, "ucs2")];
        exifObj["0th"][ids.XPAuthor] = [...Buffer.from(optd.XPAuthor, "ucs2")];
        exifObj["0th"][ids.XPKeywords] = [...Buffer.from(optd.XPKeywords, "ucs2")];
        exifObj["0th"][ids.XPSubject] = [...Buffer.from(optd.XPSubject, "ucs2")];

        let exifBytes = piexif.dump(exifObj);
        let scrubbedHotelImageData = piexif.remove(jpegBinary);
        let exifModified = piexif.insert(exifBytes, scrubbedHotelImageData);
        let fileBuffer = Buffer.from(exifModified, "binary");
        fs.writeFileSync(iSize.fileName, fileBuffer);
    };
};

module.exports = {
    write,
};
const moment = require("moment");
const dangky = require("./dangky");
const path = require("path");
const fs = require("fs-extra");
const axios = require("axios");
const qs = require("qs");
const arraySort = require("array-sort");
const { api, helpers } = require("../utils");
const { stored } = api.net.config;
const tuflow = helpers.text.to.upper.firstLetterOfWord;

const start = async () => {
    await testRequest();
    // await testSingleSheet();
    // await testTwoSheet();
};

const testRequest = async () => {
    console.log("testRequest");
    const instance = axios.create({ baseURL: "http://maccargovn.localhost" });
    let href = "/";
    instance.get(href).then((res) => {
        fs.writeFileSync("./data/maccargovn.html", res.data, { encoding: "utf-8" });
    }).catch((error) => {
        console.log(error);
    });
};

const testProductGallery = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "ProductGallery-edit.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
    };
    let list = {
        sheet01: [],
    };
    console.log("Sheet1", data.sheet01.length);

    for (let item of data.sheet01) {
        let { OrderID, ProductID, ProductGallery } = item;
        if (!ProductGallery) continue;
        let text = `insert into @temp(OrderID,ProductID,ProductGallery) values(`;
        text += `'${OrderID}'`;
        text += `,'${ProductID}'`;
        text += `,N'${ProductGallery}'`;
        text += ")";
        helpers.file.write(text);
    };
};

const testInput = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "tCitizens.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Nam", key: "Male" },
                        { header: "Nữ", key: "FeMale" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Ngày cấp", key: "IDCardCreateDate", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "Địa chỉ", key: "Address", width: 40 },
                        { header: "Nghề nghiệp", key: "Job", width: 30 },
                        { header: "Ghi chú", key: "Note", width: 20 },
                        { header: "Case", key: "Tình trạng", width: 30 },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "tCitizens" }),
        sheet02: await helpers.excel.read({ fileName: df.file.excel, sheetName: "tSocialSecurities" }),
    };
    let list = {
        all: [],
        sheet01: [],
        sheet02: [],
    };
    let temp = {
        ss: {},
        all: {
            Address: {},
        },
        sheet01: {
            Address: {},
        },
        sheet02: {
            Address: {},
        },
    };
    let dup = {
        all: {},
        sheet01: [],
        sheet02: [],
    };
    let ee = {
        dup: {
            all: {
                sheetName: "trùng-02-sheet",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01: {
                sheetName: "sheet1-trùng-nội-bộ",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02: {
                sheetName: "sheet2-trùng-nội-bộ",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            all: {
                sheetName: "all",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01: {
                sheetName: "sheet1",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02: {
                sheetName: "sheet2",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01Only: {
                sheetName: "sheet1Only",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02Only: {
                sheetName: "sheet1Only",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length, data.sheet02.length);

    /*********|get data|*********/
    let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    data.ss = res_ss.data;
    for (let item of data.ss) {
        continue;
        let { IDCard, Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let key = {
            IDCard: `${IDCard}`,
            Address: `${FullName}-${Address}`,
            SubAddress: `${FullName}-${SubAddress}`,
        };
        let obj = {
            IDCard: temp.ss[key.IDCard] || { items: [] },
            Address: temp.ss[key.Address] || { items: [] },
            SubAddress: temp.ss[key.SubAddress] || { items: [] },
        };
        obj.IDCard.items.push(item);
        obj.Address.items.push(item);
        obj.SubAddress.items.push(item);
        temp.ss[key.IDCard] = obj.IDCard;
        temp.ss[key.Address] = obj.Address;
        if (`${SubAddress}` !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    };
    let keys = {
        ss: Object.keys(temp.ss),
        all: {
            Address: [],
        },
        sheet01: {
            Address: [],
        },
        sheet02: {
            Address: [],
        },
    };
    console.log("keys.ss", keys.ss.length);
    console.log("**** Sheet1 là danh sách Citizens 19,115r");
    console.log("**** Sheet2 là danh sách chị Trọng 5,999r");

    /*********|Chống trùng IDCard|*********/
    for (let [i, item] of data.sheet01.entries()) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        if (item.IDCardCreateDate) item.IDCardCreateDate = replace.YearOfBirth(item.IDCardCreateDate, item).format("YYYY-MM-DD");
        item.SubAddress = replace.Address(item.SubAddress);
        if (item.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(item.YearOfBirth, item).format("YYYY");
        let { Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");

            if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

            Address = Address.replace(/  /gi, " ");
            item.Address = Address;
        };
        isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        // if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${FullName}-${Address}`);
        let { CitizenID, Gender, PhoneNumber, IDCard, IDCardCreateDate, YearOfBirth, Stadtteile, HouseID, Demographic, CreateDate, Note } = item;

        Gender = Gender || 1;
        let text = `insert into tCitizens`;
        text += `(CitizenID,FullName,Gender,PhoneNumber,IDCard,IDCardCreateDate,YearOfBirth,[Address],SubAddress,Stadtteile,HouseID,Demographic,CreateDate,Note) values(`;
        text += `'${CitizenID}'`;
        text += `,N'${item.FullName}'`;
        text += `,'${Gender}'`;
        if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += `,null`;
        if (IDCard) text += `,N'${IDCard}'`; else text += `,null`;
        if (IDCardCreateDate) text += `,'${IDCardCreateDate}'`; else text += `,null`;
        if (YearOfBirth) text += `,'${YearOfBirth}'`; else text += `,null`;
        if (SubAddress) text += `,N'${SubAddress}'`; else text += `,null`;
        if (Address) text += `,N'${Address}'`; else text += `,null`;
        if (Stadtteile) text += `,${Stadtteile}`; else text += `,null`;
        if (HouseID) text += `,N'${HouseID}'`; else text += `,null`;
        if (Demographic) text += `,N'${Demographic}'`; else text += `,null`;
        if (CreateDate) text += `,'${moment(CreateDate).format("YYYY-MM-DD HH:mm:ss")}'`; else text += `,null`;
        if (Note) text += `,N'${Note}'`; else text += `,null`;
        text += ")";
        // if (i % 5001 === 5000) helpers.file.write("go");
        helpers.file.write(text);
    };

    for (let item of data.sheet02) {
        let { CitizenID, Case, Job, Note } = item;
        let text = `insert into tSocialSecurities(CitizenID,[Case],Job,Note) values(`;
        text += `'${CitizenID}'`;
        text += `,N'${item.Case}'`;
        if (Job) text += `,N'${Job}'`; else text += `,null`;
        if (Note) text += `,N'${Note}'`; else text += `,null`;
        text += ")";
        helpers.file.write(text);
    };
};

const test79 = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-10-09";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Giới tính", key: "Gender" },
                        { header: "Ngày sinh", key: "DateOfBirth" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "SSCard", key: "SSCard", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "Thường trú", key: "Address01", width: 40 },
                        { header: "Hiện nay", key: "Address02", width: 40 },
                        { header: "Tạm trú", key: "SubAddress", width: 40 },
                        { header: "Tổ dân phố", key: "StadtteileID" },
                        { header: "Khu phố", key: "BlockID" },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            AutoID: {},
        },
    };
    let ee = {
        data: {
            sheet01: {
                sheetName: "NQ97-đợt-01",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length);

    /*********|get data|*********/
    let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    data.ss = res_ss.data;
    for (let item of data.ss) {
        continue;
        let { IDCard, Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let key = {
            IDCard: `${IDCard}`,
            Address: `${FullName}-${Address}`,
            SubAddress: `${FullName}-${SubAddress}`,
        };
        let obj = {
            IDCard: temp.ss[key.IDCard] || { items: [] },
            Address: temp.ss[key.Address] || { items: [] },
            SubAddress: temp.ss[key.SubAddress] || { items: [] },
        };
        obj.IDCard.items.push(item);
        obj.Address.items.push(item);
        obj.SubAddress.items.push(item);
        temp.ss[key.IDCard] = obj.IDCard;
        temp.ss[key.Address] = obj.Address;
        if (`${SubAddress}` !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    };
    let keys = {
        ss: Object.keys(temp.ss),
        sheet01: {
            AutoID: [],
        },
    };
    console.log("keys.ss", keys.ss.length);
    console.log("**** Sheet1 là danh sách NQ97 đợt 01 13884r");

    /*********|Chống trùng IDCard|*********/
    for (let item of data.sheet01) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address01) item.Address01 = replace.Address(item.Address01);
        if (item.Address02) item.Address02 = replace.Address(item.Address02);

        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        item.SubAddress = replace.Address(item.SubAddress);
        item.Gender = replace.Gender(item.Gender);
        if (item.DateOfBirth) item.DateOfBirth = replace.YearOfBirth(item.DateOfBirth).format("YYYY-MM-DD");
        else item.DateOfBirth = null;
        if (item.Gender === 1) item.Male = item.DateOfBirth;
        else item.FeMale = item.DateOfBirth;
        if (isSubAddress) {
            item.AddressType = "Tạm trú";
            if (!item.Address) item.Address = item.SubAddress;
        } else {
            item.AddressType = "Thường trú";
        };
        let { AutoID, STT, IDCard, Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");
            if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

            Address = Address.replace(/  /gi, " ");
            item.Address = Address;
        };
        isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        // if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${FullName}-${Address}`);
        let key = {
            AutoID: `${AutoID}`,
        };
        let obj = {
            sheet01: {
                AutoID: temp.sheet01.AutoID[key.AutoID] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.AutoID.items.push(item);
        temp.sheet01.AutoID[key.AutoID] = obj.sheet01.AutoID;
    };
    keys.sheet01.AutoID = Object.keys(temp.sheet01.AutoID);
    for (let key of keys.sheet01.AutoID) {
        let obj = temp.sheet01.AutoID[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let { STT, FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, StadtteileID, BlockID, Note } = item;
        let child = { STT, FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, StadtteileID, BlockID, Note, Count };
        let isDup = false;
        if (Count > 1) {
            if (obj.isDupInDb) {
                isDup = true;
                helpers.file.write(`dup.in.database-${Count}`, `${child.FullName}-${child.Address}`);
            } else {
                helpers.file.write(`dup.in.file-${Count}`, `${child.FullName}-${child.Address}`);
            };
        };
        if (!isDup) list.sheet01.push(child);
    };
    console.log("sheet01", list.sheet01.length);//4,465+5,932+3,487=13,884
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let [i, item] of list.sheet01.entries()) {
        let { STT, FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, StadtteileID, BlockID, Note } = item;
        let text = "insert into temp(STT,FullName,Gender,DateOfBirth,IDCard,SSCard,PhoneNumber,Address01,Address02,SubAddress,StadtteileID,BlockID,Note) values(";
        text += `${STT}`;
        text += `,N'${FullName}'`;
        text += `,${Gender}`;
        if (DateOfBirth) text += `,'${DateOfBirth}'`; else text += ",'1956-01-01'";
        if (IDCard) text += `,N'${IDCard}'`; else text += ",null";
        if (SSCard) text += `,N'${SSCard}'`; else text += ",null";
        if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += ",null";
        if (Address01) text += `,N'${Address01.replace(/'/gi, "''")}'`; else text += ",null";
        if (Address02) text += `,N'${Address02.replace(/'/gi, "''")}'`; else text += ",null";
        if (SubAddress) text += `,N'${SubAddress.replace(/'/gu, "''")}'`; else text += ",null";
        text += `,${StadtteileID}`;
        text += `,N'${BlockID}'`;
        if (Note) text += `,N'${Note}'`; else text += ",null";
        text += ")";
        text = text
            .replace(/,N'null',/gi, ",null,")
            .replace(/,'null',/gi, ",null,")
            .replace(/\n/gi, "");
        helpers.file.write(text);
        ee.data.sheet01.rows.push(item);
    };
};

const test01 = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-10-16";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Giới tính", key: "Gender" },
                        { header: "Ngày sinh", key: "DateOfBirth" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Mã BHXH", key: "SSCard", width: 10 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 20 },
                        { header: "Địa chỉ CMND", key: "Address01", width: 35 },
                        { header: "Tạm trú", key: "SubAddress", width: 35 },
                        { header: "Chỗ ở hiện nay", key: "Address02", width: 35 },
                        { header: "Nghề nghiệp", key: "Job", width: 30 },
                        { header: "Tổ dân phố", key: "StadtteileID" },
                        { header: "Tình trạng", key: "Case", width: 30 },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "NQ97-03" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            IDCard: {},
            Address01: {},
            Address02: {},
            SubAddress: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "NQ97-đợt-02-trùng",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "NQ97-đợt-02",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4, StadtteileID: 0 };
    console.log("data", data.sheet01.length);

    /*********|get data|*********/
    let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    data.ss = res_ss.data;
    console.log("data.ss", data.ss.length);
    for (let item of data.ss) {
        let { FullName, IDCard, Address01, Address02, SubAddress } = item;
        let key = {
            IDCard: `${IDCard}`.toLowerCase(),
            Address01: `${FullName}-${Address01}`.toLowerCase(),
            Address02: `${FullName}-${Address02}`.toLowerCase(),
            SubAddress: `${FullName}-${SubAddress}`.toLowerCase(),
        };
        let obj = {
            IDCard: temp.ss[key.IDCard] || { items: [] },
            Address01: temp.ss[key.Address01] || { items: [] },
            Address02: temp.ss[key.Address02] || { items: [] },
            SubAddress: temp.ss[key.SubAddress] || { items: [] },
        };
        obj.IDCard.items.push(item);
        obj.Address01.items.push(item);
        obj.Address02.items.push(item);
        obj.SubAddress.items.push(item);
        if (`${IDCard}` !== "null") temp.ss[key.IDCard] = obj.IDCard;
        if (`${Address01}` !== "null") temp.ss[key.Address01] = obj.Address01;
        if (`${Address02}` !== "null") temp.ss[key.Address02] = obj.Address02;
        if (`${SubAddress}` !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    };
    let keys = {
        ss: Object.keys(temp.ss),
        sheet01: {
            dup: {
                STT: [],
                Case: [],
            },
            STT: [],
            IDCard: [],
            Address01: [],
            Address02: [],
            SubAddress: [],
        },
    };
    console.log("keys.ss", keys.ss.length);
    console.log("**** Sheet1 là danh sách NQ97 đợt 03 bổ sung 1,548r");

    /*********|Chống trùng IDCard|*********/
    for (let item of data.sheet01) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address01) item.Address01 = replace.Address(item.Address01);
        if (item.Address02) item.Address02 = replace.Address(item.Address02);
        if (item.SubAddress) item.SubAddress = replace.Address(item.SubAddress);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Gender = replace.Gender(item.Gender);
        if (item.DateOfBirth) item.DateOfBirth = replace.YearOfBirth(item.DateOfBirth).format("YYYY-MM-DD");
        else item.DateOfBirth = null;
        if (item.Gender === 1) item.Male = item.DateOfBirth;
        else item.FeMale = item.DateOfBirth;

        let { STT, FullName, IDCard, Address01, Address02, SubAddress } = item;
        let isAddress01InP16Q4 = reg.dvb.test(Address01) || reg.ttt.test(Address01) || reg.xc.test(Address01);
        let isAddress02InP16Q4 = reg.dvb.test(Address02) || reg.ttt.test(Address02) || reg.xc.test(Address02);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (!isAddress01InP16Q4 && !isAddress02InP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address01)) Address01 = Address01.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address01)) Address01 = Address01.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address01)) Address01 = Address01.replace(reg.xc1, " Xóm Chiếu");
            if (reg.dvb2.test(Address01)) Address01 = Address01.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address01)) Address01 = Address01.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address01)) Address01 = Address01.replace(reg.xc2, " Xóm Chiếu");

            if (reg.dvb1.test(Address02)) Address02 = Address02.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address02)) Address02 = Address02.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address02)) Address02 = Address02.replace(reg.xc1, " Xóm Chiếu");
            if (reg.dvb2.test(Address02)) Address02 = Address02.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address02)) Address02 = Address02.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address02)) Address02 = Address02.replace(reg.xc2, " Xóm Chiếu");

            if (Address01) Address01 = Address01.replace(/  /gi, " ");
            if (Address02) Address02 = Address02.replace(/  /gi, " ");
            item.Address01 = Address01;
            item.Address02 = Address02;
        };
        isAddress01InP16Q4 = reg.dvb.test(Address01) || reg.ttt.test(Address01) || reg.xc.test(Address01);
        isAddress02InP16Q4 = reg.dvb.test(Address02) || reg.ttt.test(Address02) || reg.xc.test(Address02);
        isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (!isAddress01InP16Q4 && !isAddress02InP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${STT}-${FullName}`);
        let key = {
            IDCard: (IDCard ? `${IDCard}` : `${STT}`).toLowerCase(),
            Address01: `${FullName}-${Address01}`.toLowerCase(),
            Address02: `${FullName}-${Address02}`.toLowerCase(),
            SubAddress: `${FullName}-${SubAddress}`.toLowerCase(),
        };
        let obj = {
            IDCard: temp.ss[key.IDCard],
            Address01: temp.ss[key.Address01],
            Address02: temp.ss[key.Address02],
            SubAddress: temp.ss[key.SubAddress],
            sheet01: {
                IDCard: temp.sheet01.IDCard[key.IDCard] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.IDCard.items.push(item);
        let isDupInDb = false;

        // check IDCard
        if (obj.IDCard) {
            isDupInDb = true;
            for (let iIDCard of obj.IDCard.items) {
                let iChild = {
                    FullName: iIDCard.FullName,
                    Gender: iIDCard.Gender,
                    DateOfBirth: iIDCard.DateOfBirth,
                    IDCard: iIDCard.IDCard,
                    SSCard: iIDCard.SSCard,
                    PhoneNumber: iIDCard.PhoneNumber,
                    Address01: iIDCard.Address01,
                    SubAddress: iIDCard.SubAddress,
                    Address02: iIDCard.Address02,
                    Job: iIDCard.Job,
                    StadtteileID: iIDCard.StadtteileID,
                    Case: "Trùng CMND",
                };
                dup.sheet01.push(iChild);
            };
            dup.sheet01.push(Object.assign({ Case: "Trùng CMND" }, item));
        } else {
            obj.IDCard = { items: [] };
            obj.IDCard.items.push(item);
            temp.ss[key.IDCard] = obj.IDCard;
        };

        // check Address01
        if (obj.Address01) {
            isDupInDb = true;
            for (let iAddress01 of obj.Address01.items) {
                let iChild = {
                    FullName: iAddress01.FullName,
                    Gender: iAddress01.Gender,
                    DateOfBirth: iAddress01.DateOfBirth,
                    IDCard: iAddress01.IDCard,
                    SSCard: iAddress01.SSCard,
                    PhoneNumber: iAddress01.PhoneNumber,
                    Address01: iAddress01.Address01,
                    SubAddress: iAddress01.SubAddress,
                    Address02: iAddress01.Address02,
                    Job: iAddress01.Job,
                    StadtteileID: iAddress01.StadtteileID,
                    Case: "Trùng địa chỉ",
                };
                dup.sheet01.push(iChild);
            };
            dup.sheet01.push(Object.assign({ Case: "Trùng địa chỉ" }, item));
        } else if (`${Address01}` !== "null") {
            obj.Address01 = { items: [] };
            obj.Address01.items.push(item);
            temp.ss[key.Address01] = obj.Address01;
        };

        // check Address02
        if (obj.Address02) {
            isDupInDb = true;
            for (let iAddress02 of obj.Address02.items) {
                let iChild = {
                    FullName: iAddress02.FullName,
                    Gender: iAddress02.Gender,
                    DateOfBirth: iAddress02.DateOfBirth,
                    IDCard: iAddress02.IDCard,
                    SSCard: iAddress02.SSCard,
                    PhoneNumber: iAddress02.PhoneNumber,
                    Address01: iAddress02.Address01,
                    SubAddress: iAddress02.SubAddress,
                    Address02: iAddress02.Address02,
                    Job: iAddress02.Job,
                    StadtteileID: iAddress02.StadtteileID,
                    Case: "Trùng địa chỉ",
                };
                dup.sheet01.push(iChild);
            };
            dup.sheet01.push(Object.assign({ Case: "Trùng địa chỉ" }, item));
        } else if (`${Address02}` !== "null") {
            obj.Address02 = { items: [] };
            obj.Address02.items.push(item);
            temp.ss[key.Address02] = obj.Address02;
        };

        // check SubAddress
        if (obj.SubAddress) {
            isDupInDb = true;
            for (let iSubAddress of obj.SubAddress.items) {
                let iChild = {
                    FullName: iSubAddress.FullName,
                    Gender: iSubAddress.Gender,
                    DateOfBirth: iSubAddress.DateOfBirth,
                    IDCard: iSubAddress.IDCard,
                    SSCard: iSubAddress.SSCard,
                    PhoneNumber: iSubAddress.PhoneNumber,
                    Address01: iSubAddress.Address01,
                    SubAddress: iSubAddress.SubAddress,
                    Address02: iSubAddress.Address02,
                    Job: iSubAddress.Job,
                    StadtteileID: iSubAddress.StadtteileID,
                    Case: "Trùng tạm trú",
                };
                dup.sheet01.push(iChild);
            };
            dup.sheet01.push(Object.assign({ Case: "Trùng tạm trú" }, item));
        } else if (`${SubAddress}` !== "null") {
            obj.SubAddress = { items: [] };
            obj.SubAddress.items.push(item);
            temp.ss[key.SubAddress] = obj.SubAddress;
        };
        if (!isDupInDb) temp.sheet01.IDCard[key.IDCard] = obj.sheet01.IDCard;
    };
    keys.sheet01.IDCard = Object.keys(temp.sheet01.IDCard);
    helpers.file.write("IDCard", keys.sheet01.IDCard.length);
    for (let iIDCard of keys.sheet01.IDCard) {
        let iObj = temp.sheet01.IDCard[iIDCard];
        let Count = iObj.items.length;
        let item = iObj.items[0];
        let { STT, FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, Job, StadtteileID, Note } = item;
        let key = {
            Address01: `${FullName}-${Address01}`.toUpperCase(),
            Address02: `${FullName}-${Address02}`.toUpperCase(),
            SubAddress: `${FullName}-${SubAddress}`.toUpperCase(),
        };
        let obj = {
            sheet01: {
                Address01: temp.sheet01.Address01[key.Address01] || { isDupInDb: false, items: [] },
                Address02: temp.sheet01.Address02[key.Address02] || { isDupInDb: false, items: [] },
                SubAddress: temp.sheet01.SubAddress[key.SubAddress] || { isDupInDb: false, items: [] },
            },
        };
        StadtteileID = StadtteileID || 0;
        if (obj.sheet01.SubAddress) {
            obj.sheet01.Address01.items = [...obj.sheet01.SubAddress.items, ...obj.sheet01.Address01.items];
        };
        obj.sheet01.Address01.items.push(item);
        obj.sheet01.Address02.items.push(item);
        obj.sheet01.SubAddress.items.push(Object.assign({ Case: "Trùng tạm trú" }, item));
        temp.sheet01.Address01[key.Address01] = obj.sheet01.Address01;
        temp.sheet01.Address02[key.Address02] = obj.sheet01.Address02;
        if (`${SubAddress}` !== "null") temp.sheet01.SubAddress[key.SubAddress] = obj.sheet01.SubAddress;
        let child = { STT, FullName: item.FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, Job, StadtteileID, Note, Case: "NQ97-03" };
        list.sheet01.push(child);
        if (!keys.sheet01.STT.includes(STT)) keys.sheet01.STT.push(STT);
        if (Count <= 1) continue;
        for (let iItem of iObj.items) {
            let iChild = {
                STT: iItem.STT,
                AddressType: iItem.AddressType,
                FullName: iItem.FullName,
                Gender: iItem.Gender,
                Male: iItem.Male,
                FeMale: iItem.FeMale,
                YearOfBirth: iItem.YearOfBirth,
                IDCard: iItem.IDCard,
                Address: iItem.Address,
                SubAddress: iItem.SubAddress,
                PhoneNumber: iItem.PhoneNumber,
                Job: iItem.Job,
                FinancialSupport: iItem.FinancialSupport,
                Note: iItem.Note,
                Case: "Trùng CMND",
            };
            // helpers.file.write("dup.IDCard", iChild);
            dup.sheet01.push(iChild);
        };
    };
    for (let item of dup.sheet01) {
        let { STT, Case } = item;
        let key = `${STT}-${Case}`;
        if (Case !== "Trùng CMND") continue;
        if (STT && !keys.sheet01.dup.STT.includes(STT) && !keys.sheet01.STT.includes(STT)) keys.sheet01.dup.STT.push(STT);
        if (STT && keys.sheet01.dup.Case.includes(key)) continue;
        keys.sheet01.dup.Case.push(key);
        ee.dup.sheet01.rows.push(item);
    };
    ee.dup.sheet01.rows = arraySort(ee.dup.sheet01.rows, ["IDCard"]);
    for (let item of dup.sheet01) {
        let { STT, Case } = item;
        let key = `${STT}-${Case}`;
        if (Case === "Trùng CMND") continue;
        if (STT && !keys.sheet01.dup.STT.includes(STT) && !keys.sheet01.STT.includes(STT)) keys.sheet01.dup.STT.push(STT);
        if (STT && keys.sheet01.dup.Case.includes(key)) continue;
        keys.sheet01.dup.Case.push(key);
        ee.dup.sheet01.rows.push(item);
    };
    console.log("sheet01", keys.sheet01.STT.length, keys.sheet01.dup.STT.length);
    // 1,445r + 103dup = 1,548r
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let item of list.sheet01) {
        let { STT, FullName, Gender, DateOfBirth, IDCard, SSCard, PhoneNumber, Address01, Address02, SubAddress, Job, StadtteileID, Note } = item;
        let text = "insert into temp(STT,FullName,Gender,DateOfBirth,IDCard,SSCard,PhoneNumber,Address01,Address02,SubAddress,Job,StadtteileID,Note) values(";
        text += `${STT}`;
        text += `,N'${FullName}'`;
        text += `,${Gender}`;
        if (DateOfBirth) text += `,'${DateOfBirth}'`; else text += ",'1956-01-01'";
        if (IDCard) text += `,N'${IDCard}'`; else text += ",null";
        if (SSCard) text += `,N'${SSCard}'`; else text += ",null";
        if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += ",null";
        if (Address01) text += `,N'${Address01.replace(/'/gi, "''")}'`; else text += ",null";
        if (Address02) text += `,N'${Address02.replace(/'/gi, "''")}'`; else text += ",null";
        if (SubAddress) text += `,N'${SubAddress.replace(/'/gu, "''")}'`; else text += ",null";
        if (Job) text += `,N'${Job}'`; else text += ",null";
        text += `,${StadtteileID}`;
        if (Note) text += `,N'${Note}'`; else text += ",null";
        text += ")";
        text = text
            .replace(/,N'null',/gi, ",null,")
            .replace(/,'null',/gi, ",null,")
            .replace(/\n/gi, "");
        helpers.file.write(text);
        ee.data.sheet01.rows.push(item);
    };

    /*********|export-excel|*********/
    let sheet00 = {
        columns: df.export.sheet01.columns,
        config: df.export.config,
        rows: ee.dup.sheet01.rows,
    };
    let fExport = {
        fileName: "dup.xlsx",
        data: [sheet00],
    };
    let res_export = await helpers.excel.export(fExport);
    console.log("res_export", res_export);
};

const testSingleSheet = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-11-01";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Địa chỉ thường trú", key: "Address01", width: 40 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Ngày sinh", key: "DateOfBirth" },
                        { header: "Ngày nhận", key: "ReceivedDate" },
                        { header: "Người tạo", key: "CreateBy" },
                        { header: "Case", key: "Tình trạng", width: 30 },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            IDCard: {},
            Address: {},
            Address01: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "sheet1-trùng-nội-bộ",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "sheet1",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    // let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length);

    let keys = {
        ss: Object.keys(temp.ss),
        sheet01: {
            STT: [],
            IDCard: [],
            Address01: [],
        },
    };

    /*********|Chống trùng IDCard sheet01|*********/
    for (let item of data.sheet01) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address01) item.Address01 = replace.Address(item.Address01);
        if (item.Address02) item.Address02 = replace.Address(item.Address02);
        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        if (item.IDCardCreateDate) item.IDCardCreateDate = replace.YearOfBirth(item.IDCardCreateDate).format("YYYY-MM-DD");
        item.SubAddress = replace.Address(item.SubAddress);
        if (item.DateOfBirth) item.DateOfBirth = replace.YearOfBirth(item.DateOfBirth).format("YYYY-MM-DD");
        else item.DateOfBirth = null;
        let { STT, IDCard, Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");

            if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

            Address = Address.replace(/  /gi, " ");
            item.Address = Address;
        };
        isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        // if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${FullName}-${Address}`);
        let key = {
            IDCard: (IDCard ? `${IDCard}` : `${STT}`).toLowerCase(),
        };
        key.IDCard = `${FullName}-${key.IDCard}`.toLocaleLowerCase();
        let obj = {
            sheet01: {
                IDCard: temp.sheet01.IDCard[key.IDCard] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.IDCard.items.push(item);
        temp.sheet01.IDCard[key.IDCard] = obj.sheet01.IDCard;
    };
    keys.sheet01.IDCard = Object.keys(temp.sheet01.IDCard);
    helpers.file.write("sheet1.IDCard", keys.sheet01.IDCard.length);
    for (let iIDCard of keys.sheet01.IDCard) {
        let iObj = temp.sheet01.IDCard[iIDCard];
        let Count = iObj.items.length;
        let item = iObj.items[0];
        let { STT, FullName, Address01, PhoneNumber, IDCard, DateOfBirth, ReceivedDate, CreateBy } = item;
        let key = {
            Address01: `${FullName}-${Address01}`.toUpperCase(),
        };
        let obj = {
            sheet01: {
                Address01: temp.sheet01.Address01[key.Address01] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.Address01.items.push(item);
        temp.sheet01.Address01[key.Address01] = obj.sheet01.Address01;
        let child = { STT, FullName, Address01, PhoneNumber, IDCard, DateOfBirth, ReceivedDate, CreateBy, Case: "NQ97-03" };
        list.sheet01.push(child);
        if (!keys.sheet01.STT.includes(STT)) keys.sheet01.STT.push(STT);
        if (Count <= 1) continue;
        for (let iItem of iObj.items) {
            let iChild = {
                STT: iItem.STT,
                FullName: iItem.FullName,
                Address01: iItem.Address01,
                PhoneNumber: iItem.PhoneNumber,
                IDCard: iItem.IDCard,
                DateOfBirth: iItem.DateOfBirth,
                ReceivedDate: iItem.ReceivedDate,
                CreateBy: iItem.CreateBy,
                Case: "Trùng CMND",
            };
            // helpers.file.write("dup.IDCard", iChild);
            dup.sheet01.push(iChild);
        };
    };
    keys.sheet01.Address01 = Object.keys(temp.sheet01.Address01);
    helpers.file.write("sheet1.Address01", keys.sheet01.Address01.length);
    for (let key of keys.sheet01.Address01) {
        let iObj = temp.sheet01.Address01[key];
        let item = iObj.items[0];
        let Count = iObj.items.length;
        let { STT, FullName, Address01, PhoneNumber, IDCard, DateOfBirth, ReceivedDate, CreateBy } = item;
        let child = { STT, FullName, Address01, PhoneNumber, IDCard, DateOfBirth, ReceivedDate, CreateBy };
        list.sheet01.push(child);
        if (!keys.sheet01.STT.includes(STT)) keys.sheet01.STT.push(STT);
        if (Count <= 1) continue;
        for (let iItem of iObj.items) {
            let iChild = {
                STT: iItem.STT,
                FullName: iItem.FullName,
                Address01: iItem.Address01,
                PhoneNumber: iItem.PhoneNumber,
                IDCard: iItem.IDCard,
                DateOfBirth: iItem.DateOfBirth,
                ReceivedDate: iItem.ReceivedDate,
                CreateBy: iItem.CreateBy,
                Case: "Trùng địa chỉ",
            };
            helpers.file.write("dup.Address01", iChild);
            dup.sheet01.push(iChild);
        };
    };
    // console.log("sheet01", list.sheet01.length, dup.sheet01.length);//18 dup 0
    // ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    // ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let item of dup.sheet01) ee.dup.sheet01.rows.push(item);


    // console.log("sheet02", list.sheet02.length, dup.sheet02.length);//18 dup 0
    // ee.dup.sheet02.sheetName += `-${dup.sheet02.length}`;
    // ee.data.sheet02.sheetName += `-${list.sheet02.length}`;
    // for (let item of dup.sheet02) ee.dup.sheet02.rows.push(item);
    // for (let item of list.merge) ee.data.merge.rows.push(item);
    // for (let item of list.all) ee.data.all.rows.push(item);

    // for (let item of list.sheet02) {
    //     let { key } = item;
    //     if (!keys.sheet01.Address.includes(key)) ee.data.sheet02Only.rows.push(item);
    //     ee.data.sheet02.rows.push(item);
    // };

    /*********|export-excel|*********/
    let sheet00 = {
        columns: df.export.sheet01.columns,
        config: df.export.config,
        rows: ee.dup.sheet01.rows,
    };
    let fExport = {
        fileName: "dup.xlsx",
        data: [sheet00],
    };
    let res_export = await helpers.excel.export(fExport);
    console.log("res_export", res_export);
};

const testTwoSheet = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-10-28";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Nam", key: "Male" },
                        { header: "Nữ", key: "FeMale" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Ngày cấp", key: "IDCardCreateDate", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },

                        { header: "Địa chỉ thường trú", key: "Address01", width: 40 },
                        { header: "Địa chỉ tạm trú", key: "SubAddress", width: 40 },
                        { header: "Địa chỉ hiện nay", key: "Address02", width: 40 },

                        { header: "Nghề nghiệp", key: "Job", width: 30 },
                        { header: "Ghi chú", key: "Note", width: 20 },
                        { header: "Case", key: "Tình trạng", width: 30 },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
        sheet02: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet2" }),
    };
    let list = {
        all: [],
        sheet01: [],
        sheet02: [],
        merge: [],
    };
    let temp = {
        ss: {},
        all: {
            Address: {},
        },
        sheet01: {
            Address: {},
        },
        sheet02: {
            Address: {},
        },
    };
    let dup = {
        all: {},
        sheet01: [],
        sheet02: [],
    };
    let ee = {
        dup: {
            all: {
                sheetName: "trùng-02-sheet",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01: {
                sheetName: "sheet1-trùng-nội-bộ",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02: {
                sheetName: "sheet2-trùng-nội-bộ",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            all: {
                sheetName: "all",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01: {
                sheetName: "sheet1",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02: {
                sheetName: "sheet2",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet01Only: {
                sheetName: "sheet1Only",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            sheet02Only: {
                sheetName: "sheet1Only",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
            merge: {
                sheetName: "merge",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    // let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length, data.sheet02.length);

    // /*********|get data|*********/
    // let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    // if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    // data.ss = res_ss.data;
    // for (let item of data.ss) {
    //     continue;
    //     let { IDCard, Address, SubAddress } = item;
    //     let FullName = item.FullName.toLowerCase();
    //     let key = {
    //         IDCard: `${IDCard}`,
    //         Address: `${FullName}-${Address}`,
    //         SubAddress: `${FullName}-${SubAddress}`,
    //     };
    //     let obj = {
    //         IDCard: temp.ss[key.IDCard] || { items: [] },
    //         Address: temp.ss[key.Address] || { items: [] },
    //         SubAddress: temp.ss[key.SubAddress] || { items: [] },
    //     };
    //     obj.IDCard.items.push(item);
    //     obj.Address.items.push(item);
    //     obj.SubAddress.items.push(item);
    //     temp.ss[key.IDCard] = obj.IDCard;
    //     temp.ss[key.Address] = obj.Address;
    //     if (`${SubAddress}` !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    // };
    let keys = {
        ss: Object.keys(temp.ss),
        all: {
            Address: [],
        },
        sheet01: {
            Address: [],
        },
        sheet02: {
            Address: [],
        },
    };
    // console.log("keys.ss", keys.ss.length);
    // console.log("**** Sheet1 là danh sách chị Huệ 1271r");
    // console.log("**** Sheet2 là danh sách chị Trọng 1275r");

    /*********|Chống trùng IDCard sheet01|*********/
    for (let item of data.sheet01) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address01) item.Address01 = replace.Address(item.Address01);
        if (item.Address02) item.Address02 = replace.Address(item.Address02);
        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        if (item.IDCardCreateDate) item.IDCardCreateDate = replace.YearOfBirth(item.IDCardCreateDate).format("YYYY-MM-DD");
        item.SubAddress = replace.Address(item.SubAddress);
        let yob = replace.GenderAndYearOfBirth(item.Male, item.FeMale);
        item.Gender = yob.Gender;
        if (yob.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(yob.YearOfBirth).format("YYYY-MM-DD");
        else item.YearOfBirth = null;
        if (yob.Gender === 1) item.Male = item.YearOfBirth;
        else item.FeMale = item.YearOfBirth;
        if (isSubAddress) {
            item.AddressType = "Tạm trú";
            if (!item.Address) item.Address = item.SubAddress;
        } else {
            item.AddressType = "Thường trú";
        };
        let { Address, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");

            if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

            Address = Address.replace(/  /gi, " ");
            item.Address = Address;
        };
        isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        // if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${FullName}-${Address}`);
        let key = {
            Address: `${FullName}-${Address}`,
        };
        let obj = {
            sheet01: {
                Address: temp.sheet01.Address[key.Address] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.Address.items.push(item);
        temp.sheet01.Address[key.Address] = obj.sheet01.Address;
    };
    keys.sheet01.Address = Object.keys(temp.sheet01.Address);
    helpers.file.write("sheet1", keys.sheet01.Address.length);
    for (let key of keys.sheet01.Address) {
        let obj = temp.sheet01.Address[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let {
            STT, AddressType, Male, FeMale, Gender, YearOfBirth, IDCard, IDCardCreateDate,
            FullName, Address, Address01, SubAddress, Address02, PhoneNumber, Job, Note
        } = item;
        let Case = item.Case || "Trùng địa chỉ";
        let child = {
            key, STT, AddressType, Male, FeMale, Gender, YearOfBirth, IDCard, IDCardCreateDate,
            FullName, Address, Address01, SubAddress, Address02, PhoneNumber, Job, Note, Case
        };
        let isDup = false;
        if (Count > 1) {
            for (let iItem of obj.items) {
                let iChild = {
                    STT: iItem.STT,
                    FullName: iItem.FullName,
                    Male: iItem.Male,
                    FeMale: iItem.FeMale,
                    Gender: iItem.Gender,
                    YearOfBirth: iItem.YearOfBirth,
                    IDCard: iItem.IDCard,
                    IDCardCreateDate: iItem.IDCardCreateDate,
                    Address: iItem.Address,
                    Address01: iItem.Address01,
                    SubAddress: iItem.SubAddress,
                    Address02: iItem.Address02,
                    PhoneNumber: iItem.PhoneNumber,
                    Job: iItem.Job,
                    Note: iItem.Note,
                    Case,
                };
                dup.sheet01.push(iChild);
            };
            if (obj.isDupInDb) {
                isDup = true;
                helpers.file.write(`dup.in.database-${Count}`, `${child.FullName}-${child.Address}`);
            } else {
                helpers.file.write(`dup.in.file-${Count}`, `${child.FullName}-${child.Address}`);
            };
        };
        if (!isDup) {
            list.all.push(child);
            list.sheet01.push(child);
        };
    };
    console.log("sheet01", list.sheet01.length, dup.sheet01.length);//18 dup 0
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let item of dup.sheet01) ee.dup.sheet01.rows.push(item);


    /*********|Chống trùng IDCard sheet2|*********/
    // for (let item of data.sheet02) {
    //     if (!item.FullName) {
    //         helpers.file.write("FullName.error", item.STT);
    //         continue;
    //     };
    //     item.FullName = replace.FullName(item.FullName);
    //     item.Note = item.Note || "";
    //     let arr = item.FullName.split("-");
    //     if (arr.length > 1) {
    //         item.FullName = arr.shift();
    //         item.Note += arr.join(" ");
    //     };
    //     let reg = {
    //         note: new RegExp(/\(.*?\)/, "gi"),
    //         tamtru: new RegExp(/tạm trú/, "gi"),
    //         otro: new RegExp(/ở trọ/, "gi"),
    //         dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
    //         ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
    //         xc: new RegExp(/ Xóm Chiếu/, "gi"),

    //         dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
    //         ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
    //         xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

    //         dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
    //         ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
    //         xc2: new RegExp(/Xóm Chiếu/, "gi"),
    //     };
    //     let isSubAddress = false;
    //     if (
    //         item.SubAddress ||
    //         reg.tamtru.test(item.Address01) ||
    //         reg.tamtru.test(item.Address02) ||
    //         reg.otro.test(item.Address01) ||
    //         reg.otro.test(item.Address02)
    //     ) isSubAddress = true;
    //     if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
    //     if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
    //     if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
    //     item.Note = item.Note.trim();
    //     if (item.Address01) item.Address01 = replace.Address(item.Address01);
    //     if (item.Address02) item.Address02 = replace.Address(item.Address02);
    //     if (item.Address02) item.Address = replace.Address(item.Address02);
    //     else item.Address = replace.Address(item.Address01);
    //     if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
    //     item.Address = replace.Address(item.Address);
    //     if (item.IDCardCreateDate) item.IDCardCreateDate = replace.YearOfBirth(item.IDCardCreateDate).format("YYYY-MM-DD");
    //     item.SubAddress = replace.Address(item.SubAddress);
    //     let yob = replace.GenderAndYearOfBirth(item.Male, item.FeMale);
    //     item.Gender = yob.Gender;
    //     if (yob.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(yob.YearOfBirth).format("YYYY-MM-DD");
    //     else item.YearOfBirth = null;
    //     if (yob.Gender === 1) item.Male = item.YearOfBirth;
    //     else item.FeMale = item.YearOfBirth;
    //     if (isSubAddress) {
    //         item.AddressType = "Tạm trú";
    //         if (!item.Address) item.Address = item.SubAddress;
    //     } else {
    //         item.AddressType = "Thường trú";
    //     };
    //     let { Address, SubAddress } = item;
    //     let FullName = item.FullName.toLowerCase();
    //     let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
    //     let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
    //     if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
    //         if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
    //         if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
    //         if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");

    //         if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
    //         if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
    //         if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

    //         Address = Address.replace(/  /gi, " ");
    //         item.Address = Address;
    //     };
    //     isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
    //     isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
    //     // if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) helpers.file.write("Address.error", `${FullName}-${Address}`);
    //     let key = {
    //         Address: `${FullName}-${Address}`,
    //     };
    //     let obj = {
    //         sheet02: {
    //             Address: temp.sheet02.Address[key.Address] || { isDupInDb: false, items: [] },
    //         },
    //     };
    //     obj.sheet02.Address.items.push(item);
    //     temp.sheet02.Address[key.Address] = obj.sheet02.Address;
    // };
    // keys.sheet02.Address = Object.keys(temp.sheet02.Address);
    // helpers.file.write("sheet2", keys.sheet02.Address.length);
    // for (let key of keys.sheet02.Address) {
    //     let obj = temp.sheet02.Address[key];
    //     let item = obj.items[0];
    //     let Count = obj.items.length;
    //     let {
    //         STT, AddressType, Male, FeMale, Gender, YearOfBirth, IDCard, IDCardCreateDate,
    //         FullName, Address, Address01, SubAddress, Address02, PhoneNumber, Job, Note
    //     } = item;
    //     let Case = item.Case || "Trùng địa chỉ";
    //     let child = {
    //         key, STT, AddressType, Male, FeMale, Gender, YearOfBirth, IDCard, IDCardCreateDate,
    //         FullName, Address, Address01, SubAddress, Address02, PhoneNumber, Job, Note, Case
    //     };
    //     let isDup = false;
    //     if (Count > 1) {
    //         for (let iItem of obj.items) {
    //             let iChild = {
    //                 STT: iItem.STT,
    //                 FullName: iItem.FullName,
    //                 Male: iItem.Male,
    //                 FeMale: iItem.FeMale,
    //                 Gender: iItem.Gender,
    //                 YearOfBirth: iItem.YearOfBirth,
    //                 IDCard: iItem.IDCard,
    //                 IDCardCreateDate: iItem.IDCardCreateDate,
    //                 Address: iItem.Address,
    //                 Address01: iItem.Address01,
    //                 SubAddress: iItem.SubAddress,
    //                 Address02: iItem.Address02,
    //                 PhoneNumber: iItem.PhoneNumber,
    //                 Job: iItem.Job,
    //                 Note: iItem.Note,
    //                 Case,
    //             };
    //             dup.sheet02.push(iChild);
    //         };
    //         if (obj.isDupInDb) {
    //             isDup = true;
    //             helpers.file.write(`dup.in.database-${Count}`, `${child.FullName}-${child.Address}`);
    //         } else {
    //             helpers.file.write(`dup.in.file-${Count}`, `${child.FullName}-${child.Address}`);
    //         };
    //     };
    //     if (!isDup) {
    //         // list.all.push(child);
    //         if (!keys.sheet01.Address.includes(key)) list.all.push(child);
    //         else list.merge.push(child);
    //         list.sheet02.push(child);
    //     };
    // };
    // console.log("sheet02", list.sheet02.length, dup.sheet02.length);//18 dup 0
    // ee.dup.sheet02.sheetName += `-${dup.sheet02.length}`;
    // ee.data.sheet02.sheetName += `-${list.sheet02.length}`;
    // for (let item of dup.sheet02) ee.dup.sheet02.rows.push(item);
    // for (let item of list.merge) ee.data.merge.rows.push(item);
    // for (let item of list.all) ee.data.all.rows.push(item);

    // for (let item of list.sheet02) {
    //     let { key } = item;
    //     if (!keys.sheet01.Address.includes(key)) ee.data.sheet02Only.rows.push(item);
    //     ee.data.sheet02.rows.push(item);
    // };

    /*********|export-excel|*********/
    // let sheet00 = {
    //     columns: df.export.sheet01.columns,
    //     config: df.export.config,
    //     rows: ee.data.all.rows,
    // };
    // let fExport = {
    //     fileName: "dup.xlsx",
    //     data: [sheet00],
    // };
    // let res_export = await helpers.excel.export(fExport);
    // console.log("res_export", res_export);
};

const test04 = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-10-01";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Nam", key: "Male" },
                        { header: "Nữ", key: "FeMale" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "Địa chỉ", key: "Address", width: 40 },
                        { header: "Nhà trọ", key: "SubAddress", width: 40 },
                        { header: "Nghề nghiệp", key: "Job", width: 30 },
                        { header: "Số tiền hỗ trợ", key: "FinancialSupport", width: 15, style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Tình trạng", key: "Case", width: 30 },
                        { header: "Trùng", key: "Count", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "NQ97-01" }),
        sheet02: await helpers.excel.read({ fileName: df.file.excel, sheetName: "update" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            IDCard: {},
            Address: {},
            SubAddress: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "NQ97-đợt-01-trùng",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "NQ97-đợt-01",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length, data.sheet02.length);

    /*********|get data|*********/
    let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    data.ss = res_ss.data;
    for (let item of data.ss) {
        continue;
        let { Address, YearOfBirth, SubAddress } = item;
        let FullName = item.FullName.toLowerCase();
        let key = {
            Address: `${FullName}-${Address}`,
            SubAddress: `${FullName}-${SubAddress}`,
        };
        let obj = {
            Address: temp.ss[key.Address] || { items: [] },
            SubAddress: temp.ss[key.SubAddress] || { items: [] },
        };
        obj.Address.items.push(item);
        obj.SubAddress.items.push(item);
        temp.ss[key.Address] = obj.Address;
        if (`${SubAddress}` !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    };
    let keys = {
        ss: Object.keys(temp.ss),
    };
    console.log("keys.ss", keys.ss.length);

    /*********|File-01|*********/
    console.log("**** Sheet1 là danh sách NQ97 đợt 01 15219r");
    for (let item of data.sheet01) {
        if (!item.FullName) {
            helpers.file.write("FullName.error", item.STT);
            continue;
        };
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/ Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/ Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/ Xóm Chiếu/, "gi"),

            dvb1: new RegExp(/\/Đoàn Văn Bơ/, "gi"),
            ttt1: new RegExp(/\/Tôn Thất Thuyết/, "gi"),
            xc1: new RegExp(/\/Xóm Chiếu/, "gi"),

            dvb2: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt2: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc2: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        item.SubAddress = replace.Address(item.SubAddress);
        item.Gender = replace.Gender(item.Gender);
        if (item.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(item.YearOfBirth).format("YYYY");
        else item.YearOfBirth = null;
        if (item.Gender === 1) item.Male = item.YearOfBirth;
        else item.FeMale = item.YearOfBirth;
        if (isSubAddress) {
            item.AddressType = "Tạm trú";
            if (!item.Address) item.Address = item.SubAddress;
        } else {
            item.AddressType = "Thường trú";
        };
        let FullName = item.FullName.toLowerCase();
        let { IDCard, Address, SubAddress } = item;
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (Address && !isAddressInP16Q4 && !isSubAddressInP16Q4) {
            if (reg.dvb1.test(Address)) Address = Address.replace(reg.dvb1, " Đoàn Văn Bơ");
            if (reg.ttt1.test(Address)) Address = Address.replace(reg.ttt1, " Tôn Thất Thuyết");
            if (reg.xc1.test(Address)) Address = Address.replace(reg.xc1, " Xóm Chiếu");

            if (reg.dvb2.test(Address)) Address = Address.replace(reg.dvb2, " Đoàn Văn Bơ");
            if (reg.ttt2.test(Address)) Address = Address.replace(reg.ttt2, " Tôn Thất Thuyết");
            if (reg.xc2.test(Address)) Address = Address.replace(reg.xc2, " Xóm Chiếu");

            Address = Address.replace(/  /gi, " ");
            // helpers.file.write("Address.error", `${FullName}-${item.Address}-${Address}`);
            item.Address = Address;
        };
        let key = {
            IDCard: `${IDCard}`,
            Address: `${FullName}-${Address}`,
            SubAddress: `${FullName}-${SubAddress}`,
        };
        let obj = {
            Address: temp.ss[key.Address],
            SubAddress: temp.ss[key.SubAddress],
            sheet01: {
                IDCard: temp.sheet01.IDCard[key.IDCard] || { isDupInDb: false, items: [] },
                Address: temp.sheet01.Address[key.Address] || { isDupInDb: false, items: [] },
                SubAddress: temp.sheet01.SubAddress[key.SubAddress] || { isDupInDb: false, items: [] },
            },
        };
        if (obj.Address) {
            obj.sheet01.Address.isDupInDb = true;
            obj.sheet01.Address.items = [...obj.Address.items, ...obj.sheet01.Address.items];
        };
        if (obj.SubAddress) {
            obj.sheet01.Address.isDupInDb = true;
            obj.sheet01.Address.items = [...obj.SubAddress.items, ...obj.sheet01.Address.items];
        };
        if (obj.sheet01.SubAddress) {
            obj.sheet01.Address.items = [...obj.sheet01.SubAddress.items, ...obj.sheet01.Address.items];
        };
        obj.sheet01.IDCard.items.push(item);
        obj.sheet01.Address.items.push(item);
        obj.sheet01.SubAddress.items.push(Object.assign(item, { Case: "Trùng tạm trú" }));
        temp.sheet01.Address[key.Address] = obj.sheet01.Address;
        if (`${IDCard}` !== "null") temp.sheet01.IDCard[key.IDCard] = obj.sheet01.IDCard;
        if (`${SubAddress}` !== "null") temp.sheet01.SubAddress[key.SubAddress] = obj.sheet01.SubAddress;
    };
    for (let key of Object.keys(temp.sheet01.Address)) {
        let obj = temp.sheet01.Address[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let { AddressType, FullName, Gender, Male, FeMale, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note, Case } = item;
        let child = { AddressType, FullName, Gender, Male, FeMale, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note, Case, Count };
        let isDup = false;
        if (Count > 1) {
            dup.sheet01.push(child);
            if (obj.isDupInDb) {
                isDup = true;
                helpers.file.write(`dup.in.database-${Count}`, `${child.FullName}-${child.Address}`);
            } else {
                helpers.file.write(`dup.in.file-${Count}`, `${child.FullName}-${child.Address}`);
            };
        };
        if (!isDup) list.sheet01.push(child);
    };
    console.log("sheet01", list.sheet01.length, dup.sheet01.length);//11,712 dup 4435
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    // for (let [i, item] of dup.sheet01.entries()) {
    //     item.STT = i + 1;
    //     ee.dup.sheet01.rows.push(item);
    // };
    // for (let [i, item] of list.sheet01.entries()) {
    //     item.STT = i + 1;
    //     let { AddressType, FullName, Gender, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note } = item;
    //     let text = "insert into temp(AddressType,FullName,Gender,YearOfBirth,IDCard,[Address],SubAddress,PhoneNumber,Job,FinancialSupport,Note) values(";
    //     text += `N'${AddressType}'`;
    //     text += `,N'${FullName}'`;
    //     text += `,${Gender}`;
    //     if (YearOfBirth) text += `,${YearOfBirth}`; else text += ",1956";
    //     if (IDCard) text += `,N'${IDCard}'`; else text += ",null";
    //     if (Address) text += `,N'${Address.replace(/'/gi, "''")}'`; else text += ",null";
    //     if (SubAddress) text += `,N'${SubAddress.replace(/'/gu, "''")}'`; else text += ",null";
    //     if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += ",null";
    //     if (Job) text += `,N'${Job}'`; else text += ",null";
    //     text += `,${FinancialSupport}`;
    //     if (Note) text += `,N'${Note}'`; else text += ",null";
    //     text += ")";
    //     text = text
    //         .replace(/,N'null',/gi, ",null,")
    //         .replace(/,'null',/gi, ",null,")
    //         .replace(/\n/gi, "");
    //     // helpers.file.write(text);
    //     ee.data.sheet01.rows.push(item);
    // };

    /*********|File-02|*********/
    // console.log("**** Sheet2 là danh sách cập nhật địa chỉ 473r");
    // for (let item of data.sheet02) {
    //     let { AutoID, FullName, Address, Note } = item;
    //     if (!Note) continue;
    //     Address = replace.Address(Address);
    //     let text = `update temp set [Address]=N'${Address}',SubAddress=N'${Address}',Note=N'${Note}' where AutoID=${AutoID}--${FullName}`;
    //     helpers.file.write(text);
    // };

    /*********|export-excel|*********/
    // let sheet00 = {
    //     columns: df.export.sheet01.columns,
    //     config: df.export.config,
    //     rows: ee.dup.sheet01.rows,
    // };
    // let fExport = {
    //     fileName: "dup.xlsx",
    //     data: [sheet00],
    // };
    // let res_export = await helpers.excel.export(fExport);
    // console.log("res_export", res_export);
};

const test05 = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-09-30";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Nam", key: "Male" },
                        { header: "Nữ", key: "FeMale" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "Địa chỉ", key: "Address", width: 40 },
                        { header: "Nhà trọ", key: "SubAddress", width: 40 },
                        { header: "Nghề nghiệp", key: "Job", width: 30 },
                        { header: "Số tiền hỗ trợ", key: "FinancialSupport", width: 15, style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Tình trạng", key: "Case", width: 30 },
                        { header: "Trùng", key: "Count", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "NQ68-19" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            Address: {},
            SubAddress: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "NQ68-đợt-19-trùng",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "NQ68-đợt-19",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length);

    /*********|get data|*********/
    let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    data.ss = res_ss.data;
    for (let item of data.ss) {
        let { Address, SubAddress } = item;
        let key = {
            Address: `${Address}`.toLowerCase(),
            SubAddress: `${SubAddress}`.toLowerCase(),
        };
        let obj = {
            Address: temp.ss[key.Address] || { items: [] },
            SubAddress: temp.ss[key.SubAddress] || { items: [] },
        };
        obj.Address.items.push(item);
        obj.SubAddress.items.push(item);
        if (key.Address !== "null") temp.ss[key.Address] = obj.Address;
        if (key.SubAddress !== "null") temp.ss[key.SubAddress] = obj.SubAddress;
    };
    let keys = {
        ss: Object.keys(temp.ss),
    };
    console.log("keys.ss", keys.ss.length);

    /*********|File-01|*********/
    console.log("**** Sheet1 là danh sách NQ68 đợt 190 18r");
    for (let item of data.sheet01) {
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (
            item.SubAddress ||
            reg.tamtru.test(item.Address01) ||
            reg.tamtru.test(item.Address02) ||
            reg.otro.test(item.Address01) ||
            reg.otro.test(item.Address02)
        ) isSubAddress = true;
        if (item.Address01 && reg.note.test(item.Address01)) item.Note += ` ${item.Address01.match(reg.note)[0].trim()}`;
        if (item.Address02 && reg.note.test(item.Address02)) item.Note += ` ${item.Address02.match(reg.note)[0].trim()}`;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.Address02) item.Address = replace.Address(item.Address02);
        else item.Address = replace.Address(item.Address01);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        item.SubAddress = replace.Address(item.SubAddress);
        let yob = replace.GenderAndYearOfBirth(item.Male, item.FeMale);
        item.Gender = yob.Gender;
        if (yob.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(yob.YearOfBirth).format("YYYY");
        else item.YearOfBirth = null;
        if (yob.Gender === 1) item.Male = item.YearOfBirth;
        else item.FeMale = item.YearOfBirth;
        if (isSubAddress) {
            item.AddressType = "Tạm trú";
            if (!item.Address) item.Address = item.SubAddress;
        } else {
            item.AddressType = "Thường trú";
        };
        let FullName = item.FullName.toLowerCase();
        let { Address, SubAddress } = item;
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (!isAddressInP16Q4 && !isSubAddressInP16Q4) console.log("Address.error", FullName, Address);
        let key = {
            Address: `${Address}`.toLowerCase(),
            SubAddress: `${SubAddress}`.toLowerCase(),
        };
        let obj = {
            Address: temp.ss[key.Address] || null,
            SubAddress: temp.ss[key.SubAddress] || null,
            sheet01: {
                Address: temp.sheet01.Address[key.Address] || { isDupInDb: false, items: [] },
                SubAddress: temp.sheet01.SubAddress[key.SubAddress] || { isDupInDb: false, items: [] },
            },
        };
        if (obj.Address) {
            obj.sheet01.Address.isDupInDb = true;
            obj.sheet01.Address.items = [...obj.Address.items, ...obj.sheet01.Address.items];
        };
        if (obj.SubAddress) {
            obj.sheet01.Address.isDupInDb = true;
            obj.sheet01.Address.items = [...obj.SubAddress.items, ...obj.sheet01.Address.items];
        };
        if (obj.sheet01.SubAddress) {
            obj.sheet01.Address.items = [...obj.sheet01.SubAddress.items, ...obj.sheet01.Address.items];
        };
        obj.sheet01.Address.items.push(item);
        obj.sheet01.SubAddress.items.push(Object.assign(item, { Case: "Trùng tạm trú" }));
        if (key.Address !== "null") temp.sheet01.Address[key.Address] = obj.sheet01.Address;
        if (key.SubAddress !== "null") temp.sheet01.SubAddress[key.SubAddress] = obj.sheet01.SubAddress;
    };
    for (let key of Object.keys(temp.sheet01.Address)) {
        let obj = temp.sheet01.Address[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let { AddressType, FullName, Gender, Male, FeMale, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note, Case } = item;
        let child = { AddressType, FullName, Gender, Male, FeMale, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note, Case, Count };
        let isDup = false;
        if (Count > 1) {
            dup.sheet01.push(child);
            if (obj.isDupInDb) {
                isDup = true;
                helpers.file.write(`dup.in.database-${Count}`, `${child.FullName}-${child.Address}-${child.SubAddress}`);
            } else {
                helpers.file.write(`dup.in.file-${Count}`, `${child.FullName}-${child.Address}-${child.SubAddress}`);
            };
        };
        if (!isDup) {
            // helpers.file.write(`${key}-${child.FullName}`, child.Address);
            list.sheet01.push(child);
        };
    };
    console.log("sheet01", list.sheet01.length, dup.sheet01.length);//1340 dup 111
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let [i, item] of dup.sheet01.entries()) {
        item.STT = i + 1;
        ee.dup.sheet01.rows.push(item);
    };
    for (let [i, item] of list.sheet01.entries()) {
        item.STT = i + 1;
        let { AddressType, FullName, Gender, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note } = item;
        let text = "insert into temp(AddressType,FullName,Gender,YearOfBirth,IDCard,[Address],SubAddress,PhoneNumber,Job,FinancialSupport,Note) values(";
        text += `N'${AddressType}'`;
        text += `,N'${FullName}'`;
        text += `,${Gender}`;
        if (YearOfBirth) text += `,${YearOfBirth}`; else text += ",1956";
        if (IDCard) text += `,N'${IDCard}'`; else text += ",null";
        if (Address) text += `,N'${Address.replace(/'/gi, "''")}'`; else text += ",null";
        if (SubAddress) text += `,N'${SubAddress.replace(/'/gu, "''")}'`; else text += ",null";
        if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += ",null";
        if (Job) text += `,N'${Job}'`; else text += ",null";
        text += `,${FinancialSupport}`;
        if (Note) text += `,N'${Note}'`; else text += ",null";
        text += ")";
        text = text
            .replace(/,N'null',/gi, ",null,")
            .replace(/,'null',/gi, ",null,")
            .replace(/\n/gi, "");
        helpers.file.write(text);
        ee.data.sheet01.rows.push(item);
    };

    /*********|File-02|*********/
    // console.log("**** Sheet2 là danh sách cập nhật địa chỉ 41r");
    // for (let item of data.sheet02) {
    //     let { CitizenID, FullName, Gender01, Gender02, IDCard01, IDCard02, Address01, SubAddress01, Address02, PhoneNumber01, PhoneNumber02 } = item;
    //     if (!SubAddress01 || !Address02) continue;
    //     if (SubAddress01.toLowerCase() !== Address02.toLowerCase()) continue
    //     let text = `update tCitizens--${FullName}`;
    //     text += "\nset";
    //     text += `\n\t[Address]=N'${Address01}'`;
    //     text += `,\n\tSubAddress=N'${SubAddress01}'`;
    //     if (Gender01 !== Gender02) text += `,\n\tGender=${Gender01}`;
    //     if (IDCard01 && !IDCard02) text += `,\n\tIDCard=N'${IDCard01}'`;
    //     if (PhoneNumber01 && !PhoneNumber02) text += `,\n\tPhoneNumber=N'${PhoneNumber01}'`;
    //     text += `\nwhere CitizenID='${CitizenID}'\n`
    //     // helpers.file.write(text);
    // };

    /*********|export-excel|*********/
    let sheet00 = {
        columns: df.export.sheet01.columns,
        config: df.export.config,
        rows: ee.dup.sheet01.rows,
    };
    let fExport = {
        fileName: "dup.xlsx",
        data: [sheet00],
    };
    let res_export = await helpers.excel.export(fExport);
    console.log("res_export", res_export);
};

const testCitizen = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-09-26";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Giới tính", key: "Gender" },
                        { header: "Ngày sinh", key: "YearOfBirth" },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Mã BHXH", key: "SocicalIDCard", width: 20 },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "Địa chỉ", key: "Address", width: 40 },
                        { header: "Tạm trú", key: "SubAddress", width: 40 },
                        { header: "Chỗ ở hiện nay", key: "CurrentAddress", width: 40 },
                        { header: "Ghi chú", key: "Note", width: 40 },
                        { header: "Tệp", key: "File", width: 40 },
                        { header: "Trùng", key: "Count", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            Address: {},
            SubAddress: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "NQ68-đợt-13-trùng",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "NQ68-đợt-13",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    console.log("data", data.sheet01.length);

    /*********|File-01|*********/
    console.log("**** Sheet1 là danh sách chống trùng P16Q4 16,472r");
    for (let item of data.sheet01) {
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/Xóm Chiếu/, "gi"),
        };
        item.FullName = replace.FullName(item.FullName);
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        item.SubAddress = replace.Address(item.SubAddress);
        item.CurrentAddress = replace.Address(item.CurrentAddress);
        // item.Address = item.Address || item.CurrentAddress || item.SubAddress;
        if (item.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(item.YearOfBirth).format("DD/MM/YYYY");
        if (!item.FullName) console.log("STT", item.STT);
        let FullName = item.FullName.toLowerCase();
        // if (FullName === "nguyễn ngọc bảo hân") console.log("item", item);
        let { Address, SubAddress, CurrentAddress } = item;
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        let isCurrentAddressInP16Q4 = reg.dvb.test(CurrentAddress) || reg.ttt.test(CurrentAddress) || reg.xc.test(CurrentAddress);
        if (!isAddressInP16Q4 && !isSubAddressInP16Q4 && !isCurrentAddressInP16Q4) helpers.file.write("Address.error", `${item.STT}-${FullName}-${Address}-${SubAddress}-${CurrentAddress}`);
        let key = {
            Address: `${FullName}-${Address}`,
        };
        let obj = {
            sheet01: {
                Address: temp.sheet01.Address[key.Address] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.Address.items.push(item);
        temp.sheet01.Address[key.Address] = obj.sheet01.Address;
    };
    for (let key of Object.keys(temp.sheet01.Address)) {
        let obj = temp.sheet01.Address[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let { STT, FullName, Gender, YearOfBirth, IDCard, SocicalIDCard, PhoneNumber, Address, SubAddress, CurrentAddress, Note, File } = item;
        let child = { STT, FullName, Gender, YearOfBirth, IDCard, SocicalIDCard, PhoneNumber, Address, SubAddress, CurrentAddress, Note, File, Count };
        let isDup = false;
        if (Count > 1) dup.sheet01.push(child);
        if (!isDup) list.sheet01.push(child);
    };
    console.log("sheet01", list.sheet01.length, dup.sheet01.length);
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let [i, item] of dup.sheet01.entries()) {
        item.STT = i + 1;
        ee.dup.sheet01.rows.push(item);
    };
    for (let [i, item] of list.sheet01.entries()) {
        item.STT = i + 1;
        ee.data.sheet01.rows.push(item);
    };

    // /*********|export-excel|*********/
    let sheet00 = {
        columns: df.export.sheet01.columns,
        config: df.export.config,
        rows: ee.dup.sheet01.rows,
    };
    let fExport = {
        fileName: "dup.xlsx",
        data: [sheet00],
    };
    let res_export = await helpers.excel.export(fExport);
    console.log("res_export", res_export);
};

const testFilial = async () => {
    console.log("đọc file excel");

    /*********|First Config|*********/
    helpers.file.clear();
    let dir = "E:/Download/tiemchung/2021-09-26";
    const cafe = {
        default: {
            file: {
                excel: path.join(dir, "map.xlsx"),
            },
            export: {
                sheet01: {
                    columns: [
                        { header: "STT", key: "STT", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                        { header: "HCDCCode", key: "HCDCCode", width: 25 },
                        { header: "Họ tên", key: "FullName", width: 25 },
                        { header: "Năm sinh", key: "YearOfBirth" },
                        { header: "Giới tính", key: "Gender" },
                        { header: "Số điện thoại", key: "PhoneNumber", width: 25 },
                        { header: "CMND", key: "IDCard", width: 20 },
                        { header: "Địa chỉ", key: "Address", width: 40 },
                        { header: "Tình trạng", key: "Status", width: 40 },
                        { header: "CT", key: "CTValue", width: 30 },
                        { header: "Bệnh viện nhận", key: "Hospital", width: 30 },
                        { header: "Ngày bắt đầu", key: "DateStart", width: 40 },
                        { header: "Ngày kết thúc", key: "DateEnd", width: 40 },
                        { header: "Trùng", key: "Count", style: { alignment: { horizontal: "right" }, numFmt: "#,##0" } },
                    ],
                },
                config: {
                    worksheet: {
                        name: "sheetName",
                        views: [{
                            state: "frozen",
                            xSplit: 1,
                            ySplit: 1,
                            activeCell: "A1"
                        }],
                    },
                },
            },
        },
    };
    const df = cafe.default;
    let data = {
        ss: [],
        sheet01: await helpers.excel.read({ fileName: df.file.excel, sheetName: "Sheet1" }),
    };
    let list = {
        sheet01: [],
    };
    let temp = {
        ss: {},
        sheet01: {
            Address: {},
            SubAddress: {},
        },
    };
    let dup = {
        sheet01: [],
    };
    let ee = {
        dup: {
            sheet01: {
                sheetName: "NQ68-đợt-13-trùng",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
        data: {
            sheet01: {
                sheetName: "NQ68-đợt-13",
                columns: df.export.sheet01.columns,
                config: df.export.config,
                rows: [],
            },
        },
    };
    let filter = { SessionID: api.net.config.SessionID.p16q4 };
    console.log("data", data.sheet01.length);

    // /*********|get data|*********/
    // let res_ss = await api.net.data.filial.load.list({ spName: stored.GetSocialSecurities, filter });
    // if (!res_ss || !res_ss.isSuccess) return console.log("GetSocialSecurities.error", res_ss);
    // data.ss = res_ss.data;
    // for (let item of data.ss) {
    //     let { Address, YearOfBirth, SubAddress } = item;
    //     let FullName = item.FullName.toLowerCase();
    //     let key = {
    //         Address: `${FullName}-${Address}`,
    //         SubAddress: `${FullName}-${YearOfBirth}-${SubAddress}`,
    //     };
    //     let obj = {
    //         Address: temp.ss[key.Address] || { items: [] },
    //         SubAddress: temp.ss[key.SubAddress] || { items: [] },
    //     };
    //     obj.Address.items.push(item);
    //     obj.SubAddress.items.push(item);
    //     temp.ss[key.Address] = obj.Address;
    //     temp.ss[key.SubAddress] = obj.SubAddress;
    // };
    // let keys = {
    //     ss: Object.keys(temp.ss),
    // };
    // console.log("keys.ss", keys.ss.length);

    /*********|File-01|*********/
    console.log("**** Sheet1 là danh sách NQ68 đợt 13 89r");
    for (let item of data.sheet01) {
        item.FullName = replace.FullName(item.FullName);
        item.Note = item.Note || "";
        let arr = item.FullName.split("-");
        if (arr.length > 1) {
            item.FullName = arr.shift();
            item.Note += arr.join(" ");
        };
        let reg = {
            note: new RegExp(/\(.*?\)/, "gi"),
            tamtru: new RegExp(/tạm trú/, "gi"),
            otro: new RegExp(/ở trọ/, "gi"),
            dvb: new RegExp(/Đoàn Văn Bơ/, "gi"),
            ttt: new RegExp(/Tôn Thất Thuyết/, "gi"),
            xc: new RegExp(/Xóm Chiếu/, "gi"),
        };
        let isSubAddress = false;
        if (item.SubAddress) isSubAddress = true;
        if (item.SubAddress && reg.note.test(item.SubAddress)) item.Note += ` ${item.SubAddress.match(reg.note)[0].trim()}`;
        item.Note = item.Note.trim();
        if (item.PhoneNumber) item.PhoneNumber = replace.PhoneNumber(item.PhoneNumber);
        item.Address = replace.Address(item.Address);
        item.SubAddress = replace.Address(item.SubAddress);
        if (item.YearOfBirth) item.YearOfBirth = replace.YearOfBirth(item.YearOfBirth).format("YYYY");
        if (isSubAddress) {
            item.AddressType = "Tạm trú";
            if (!item.Address) item.Address = item.SubAddress;
        } else {
            item.AddressType = "Thường trú";
        };
        let FullName = item.FullName.toLowerCase();
        let { Address, YearOfBirth, SubAddress } = item;
        let isAddressInP16Q4 = reg.dvb.test(Address) || reg.ttt.test(Address) || reg.xc.test(Address);
        let isSubAddressInP16Q4 = reg.dvb.test(SubAddress) || reg.ttt.test(SubAddress) || reg.xc.test(SubAddress);
        if (!isAddressInP16Q4 && !isSubAddressInP16Q4) console.log("Address.error", FullName, Address);
        let key = {
            Address: `${FullName}-${Address}`,
        };
        let obj = {
            sheet01: {
                Address: temp.sheet01.Address[key.Address] || { isDupInDb: false, items: [] },
            },
        };
        obj.sheet01.Address.items.push(item);
        temp.sheet01.Address[key.Address] = obj.sheet01.Address;
    };
    for (let key of Object.keys(temp.sheet01.Address)) {
        let obj = temp.sheet01.Address[key];
        let item = obj.items[0];
        let Count = obj.items.length;
        let { STT, HCDCCode, FullName, YearOfBirth, Gender, PhoneNumber, IDCard, Address, Status, CTValue, Hospital, DateStart, DateEnd } = item;
        let child = { STT, HCDCCode, FullName, YearOfBirth, Gender, PhoneNumber, IDCard, Address, Status, CTValue, Hospital, DateStart, DateEnd, Count };
        let isDup = false;
        if (Count > 1) dup.sheet01.push(child);
        if (!isDup) list.sheet01.push(child);
    };
    console.log("sheet01", list.sheet01.length, dup.sheet01.length);//1340 dup 111
    ee.dup.sheet01.sheetName += `-${dup.sheet01.length}`;
    ee.data.sheet01.sheetName += `-${list.sheet01.length}`;
    for (let [i, item] of dup.sheet01.entries()) {
        item.STT = i + 1;
        ee.dup.sheet01.rows.push(item);
    };
    for (let [i, item] of list.sheet01.entries()) {
        item.STT = i + 1;
        //     let { AddressType, FullName, Gender, YearOfBirth, IDCard, Address, SubAddress, PhoneNumber, Job, FinancialSupport, Note } = item;
        //     let text = "insert into temp2(AddressType,FullName,Gender,YearOfBirth,IDCard,[Address],SubAddress,PhoneNumber,Job,FinancialSupport,Note) values(";
        //     text += `N'${AddressType}'`;
        //     text += `,N'${FullName}'`;
        //     text += `,${Gender}`;
        //     if (YearOfBirth) text += `,${YearOfBirth}`; else text += ",1956";
        //     if (IDCard) text += `,N'${IDCard}'`; else text += ",null";
        //     if (Address) text += `,N'${Address.replace(/'/gi, "''")}'`; else text += ",null";
        //     if (SubAddress) text += `,N'${SubAddress.replace(/'/gu, "''")}'`; else text += ",null";
        //     if (PhoneNumber) text += `,N'${PhoneNumber}'`; else text += ",null";
        //     if (Job) text += `,N'${Job}'`; else text += ",null";
        //     text += `,${FinancialSupport}`;
        //     if (Note) text += `,N'${Note}'`; else text += ",null";
        //     text += ")";
        //     text = text
        //         .replace(/,N'null',/gi, ",null,")
        //         .replace(/,'null',/gi, ",null,")
        //         .replace(/\n/gi, "");
        //     helpers.file.write(text);
        ee.data.sheet01.rows.push(item);
    };

    /*********|export-excel|*********/
    let sheet00 = {
        columns: df.export.sheet01.columns,
        config: df.export.config,
        rows: ee.data.sheet01.rows,
    };
    let fExport = {
        fileName: "dup.xlsx",
        data: [sheet00],
    };
    let res_export = await helpers.excel.export(fExport);
    console.log("res_export", res_export);
};

const replace = {
    FullName: (FullName) => {
        if (!FullName) return null;
        FullName = FullName.trim();
        FullName = FullName.replace(/\n/gi, " ");
        FullName = FullName.replace(/'/gi, "''");
        while ((new RegExp("\r", "gi")).test(FullName)) FullName = FullName.replace(/\r/gi, "");
        while ((new RegExp("\n", "gi")).test(FullName)) FullName = FullName.replace(/\n/gi, "");
        FullName = FullName.replace(/  /gi, " ");
        FullName = tuflow(FullName);
        return FullName;
    },
    Gender: (Gender) => {
        if (!Gender) return 1;
        if (Gender.toLowerCase() === "nữ") return 2;
        return 1;
    },
    GenderAndYearOfBirth: (Male, FeMale) => {
        let obj = { YearOfBirth: null, Gender: 1 };
        if (isNaN(Male) && isNaN(FeMale)) return obj;
        if (FeMale) {
            obj.YearOfBirth = FeMale;
            obj.Gender = 2;
        };
        if (Male) {
            obj.YearOfBirth = Male;
            obj.Gender = 1;
        };

        return obj;
    },
    Address: (Address) => {
        if (!Address) return null;
        if (typeof (Address) !== "string") Address = JSON.stringify(Address);
        Address = Address.trim();
        while ((new RegExp("\r", "gi")).test(Address)) Address = Address.replace(/\r/gi, "");
        while ((new RegExp("\n", "gi")).test(Address)) Address = Address.replace(/\n/gi, "");
        Address = Address.replace(/TTT/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thắt Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thất Thuyến/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thất Thuyểt/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thát Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tốn Thất Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tồn Thất Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thât Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/Tôn Thất Thuyêt/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/ Ôn Thất Thuyết/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/ TÔN THẤT THYẾT/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/ TÔN THẤT TUYẾT/gi, "Tôn Thất Thuyết");

        Address = Address.replace(/ĐVB/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/BVĐ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/đbv/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/DVB/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đvc/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đoàn Vă Bơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đoan Văn Bơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đoàn Van Bơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đooàn Văn Bơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Boàn Văn Bơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đoàn Văn Vơ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/Đoán Văn Bơ/gi, "Đoàn Văn Bơ");

        Address = Address.replace(/XC/gi, "Xóm Chiếu");
        Address = Address.replace(/Xóm Chiêu/gi, "Xóm Chiếu");
        Address = Address.replace(/XÓM CHIÉU/gi, "Xóm Chiếu");

        Address = Address.replace(/NTT/gi, "Nguyễn Tất Thành");
        Address = Address.replace(/TĐ/gi, "Tôn Đản");

        Address = Address.replace(/Nguyễn Đình Triễu/gi, "Nguyễn Đình Chiểu");

        Address = Address.replace(/khu phố 1/gi, "");
        Address = Address.replace(/khu phố 2/gi, "");
        Address = Address.replace(/khu phố 3/gi, "");
        Address = Address.replace(/khu phố 4/gi, "");
        Address = Address.replace(/ \(.*?\)/gi, "");
        Address = Address.replace(/ ở trọ/gi, "");
        Address = Address.replace(/ nhà trọ/gi, "");
        Address = Address.replace(/ tạm trú/gi, "");
        Address = Address.replace(/ đường/gi, "");
        Address = Address.replace(/,/gi, "");

        Address = Address.replace(/ p16q4/gi, "");
        Address = Address.replace(/ p16\/q4/gi, "");
        Address = Address.replace(/ p16\/q5/gi, "");
        Address = Address.replace(/ q4/gi, "");
        Address = Address.replace(/ p16/gi, "");
        Address = Address.replace(/ -p16/gi, "");
        Address = Address.replace(/ quận 4/gi, "");
        Address = Address.replace(/ phường 16/gi, "");
        Address = Address.replace(/  /gi, " ");
        Address = Address.replace(/'/gi, "''");
        Address = tuflow(Address).trim();

        return Address;
    },
    Address2: (Address) => {
        if (!Address) return null;
        if (typeof (Address) !== "string") Address = JSON.stringify(Address);
        Address = Address.trim();
        while ((new RegExp("\r", "gi")).test(Address)) Address = Address.replace(/\r/gi, "");
        while ((new RegExp("\n", "gi")).test(Address)) Address = Address.replace(/\n/gi, "");
        Address = Address.replace(/TTT/gi, "Tôn Thất Thuyết");
        Address = Address.replace(/ĐVB/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/BVĐ/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/DVB/gi, "Đoàn Văn Bơ");
        Address = Address.replace(/XC/gi, "Xóm Chiếu");
        Address = Address.replace(/NTT/gi, "Nguyễn Tất Thành");
        Address = Address.replace(/TĐ/gi, "Tôn Đản");

        Address = Address.replace(/khu phố 1/gi, "");
        Address = Address.replace(/khu phố 2/gi, "");
        Address = Address.replace(/khu phố 3/gi, "");
        Address = Address.replace(/khu phố 4/gi, "");
        Address = Address.replace(/ \(.*?\)/gi, "");
        Address = Address.replace(/ ở trọ/gi, "");
        Address = Address.replace(/ nhà trọ/gi, "");
        Address = Address.replace(/ tạm trú/gi, "");
        Address = Address.replace(/ đường/gi, "");
        Address = Address.replace(/,/gi, "");

        Address = Address.replace(/ p16q4/gi, "");
        Address = Address.replace(/ p16\/q4/gi, "");
        Address = Address.replace(/ p16\/q5/gi, "");
        Address = Address.replace(/ q4/gi, "");
        Address = Address.replace(/ p16/gi, "");
        Address = Address.replace(/ -p16/gi, "");
        Address = Address.replace(/ quận 4/gi, "");
        Address = Address.replace(/ phường 16/gi, "");
        Address = Address.replace(/  /gi, " ");
        Address = tuflow(Address).trim();

        return Address;
    },
    YearOfBirth: (text, item) => {
        text = `${text}`.trim();
        if (text.length === 4) text = `${text}-01-01`;
        let arr = [];
        let newDate = new Date();
        if (text.includes("/")) arr = text.split("/");
        if (text.includes(".")) arr = text.split(".");
        if (text.includes("-")) arr = text.split("-");
        if (arr.length != 3) helpers.file.write("YearOfBirth", `---${text}---`);
        else {
            let year = "2021";
            let month = parseInt(arr[1], 10) - 1;
            let day = "1";
            if (arr[0].length === 4) {
                year = arr[0];
                day = arr[2];
            } else {
                year = arr[2];
                day = arr[0];
            };
            if (year.length > 4) helpers.file.write("year", `---${year}---`);
            if (year.length === 2) year = `20${year}`;
            year = parseInt(year, 10);
            day = parseInt(day, 10);
            // if (text === "15-10-19") console.log("date", year, month, day);
            newDate = new Date(year, month, day, 0, 0, 0, 0);
        };
        let mDate = moment(newDate);
        // if (text === "15-10-19") console.log("mDate", mDate.format("YYYY-MM-DD"));

        return mDate;
    },
    PhoneNumber: (text) => {
        if (!text) return null;
        if (text.length <= 9 && text.charAt(0) === "9") text = `0${text}`;
        text = text.replace(/'/gi, "");

        return text;
    },
};

start();
